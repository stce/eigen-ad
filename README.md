**Eigen is a C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms.**

For more information go to http://eigen.tuxfamily.org/.

***

**Eigen-AD is a fork of Eigen aimed at Algorithmic Differentiation (AD) by overloading tool developers and users.**

This repository is forked from [Eigen's official git project](https://gitlab.com/libeigen/eigen) ("*upstream*").

- Branch **release** contains the Eigen-AD version of the latest Eigen release (currently 3.3.9).
- Branch **develop** contains WIP development for Eigen-AD for the current release.
- Branch **develop_3.3** contains the WIP Eigen-AD version of a possible upcoming Eigen 3.3.10 release (upstream `3.3` branch).
- Branch **develop_3.4** contains the WIP Eigen-AD version of the Eigen 3.4 release (upstream `3.4` branch).  
- Branch **develop_3.5** contains the WIP Eigen-AD version of the ongoing Eigen development code scheduled for Eigen 3.5 (upstream `master` branch).  
- Branches with the **no_auto** suffix indicate a *light* Eigen-AD version without the auto return type deduction, i.e. without intrusive changes to the Eigen source

Also refer to the [tags page](https://gitlab.stce.rwth-aachen.de/stce/eigen-ad/-/tags) for older Eigen-AD versions.

The testsystem has been extended for supported AD by overloading tools. Use the EIGEN_AD_TOOL CMake variable to select a tool.

**Documentation:**
- A paper has been published in the proceedings of the ICCS 2020. Refer to the [Springer website](https://link.springer.com/chapter/10.1007/978-3-030-50371-0_51).
- See the extended [arXiv paper (v1)](https://arxiv.org/abs/1911.12604v1) for a general overview of Eigen-AD.
- AD tool authors are advised to read the [Eigen-AD Technical Guide](https://gitlab.stce.rwth-aachen.de/peltzer/eigen-ad/blob/release/unsupported/doc/AD/techguide.pdf) for details on the Eigen-AD API and required steps to implement a new AD tool module.
- dco/c++/eigen users may refer to the [dco/c++/eigen User Guide](https://gitlab.stce.rwth-aachen.de/peltzer/eigen-ad/blob/release/unsupported/doc/dco/userguide.pdf).

**Licensing:**  
Eigen-AD is is provided under the same licenses as Eigen; new source code introduced by Eigen-AD is licensed under MPL2. Eigen-AD does not include any standalone AD tools, but provides extension modules utilizing the APIs of AD tools.

**Citing:**  
Feel free to cite our ICCS 2020 paper when refering to Eigen-AD:
```
@InProceedings{10.1007/978-3-030-50371-0_51,
author="Peltzer, Patrick and Lotz, Johannes and Naumann, Uwe",
editor="Krzhizhanovskaya, Valeria V. and Z{\'a}vodszky, G{\'a}bor and Lees, Michael H. and Dongarra, Jack J. and Sloot, Peter M. A. and Brissos, S{\'e}rgio and Teixeira, Jo{\~a}o",
title="Eigen-AD: Algorithmic Differentiation of the Eigen Library",
booktitle="Computational Science -- ICCS 2020",
year="2020",
publisher="Springer International Publishing",
address="Cham",
pages="690--704",
isbn="978-3-030-50371-0"
}
```

