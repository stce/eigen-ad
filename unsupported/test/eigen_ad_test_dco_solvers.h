// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_TEST_DCO_SOLVERS_H
#define EIGEN_AD_TEST_DCO_SOLVERS_H

//#define EIGEN_AD_SOLVER_FALLBACK_TO_ALG
#define EIGEN_AD_NO_SYMBOLIC_PRODUCT

#define EIGEN_TEST_AD
#include "main.h"

#include "Eigen/LU"

#undef EIGEN_TEST_MAX_SIZE
#define EIGEN_TEST_MAX_SIZE 50

enum RequirementsType {
  None=0,
  Invertible=1,
  PosDefinite=2
};

// Hard coded 3x3 problem
template<typename Scalar> void createSystem(Matrix<Scalar, 3, 3>& A, Matrix<Scalar, 3, 1>& b)
{
    A << 1, 2, 4, 2, 3, 8, -1, -3, -1;
    b << 1, 2, 3;
}

// Dynamic matrix size and dynamic problem generation
template<typename Scalar> void createSystem(Matrix<Scalar, Dynamic, Dynamic>& A, Matrix<Scalar, Dynamic, 1>& b)
{
    int size = internal::random<int>(1,EIGEN_TEST_MAX_SIZE);

    A = Matrix<Scalar, Dynamic, Dynamic>::Random(size, size);
    b = Matrix<Scalar, Dynamic, Dynamic>::Random(size, 1);
}

template <int Requirements, typename MatrixType, typename VectorType> void setupSystem(MatrixType& A, VectorType& b) {
    createSystem(A, b);

    if(Requirements == Invertible) {
        FullPivLU<MatrixType> lu(A);
        lu.setThreshold(0.01);
        while(!lu.isInvertible()) {
          createSystem(A, b);
        }
    } else if(Requirements == PosDefinite) {
        MatrixType A1 = A * A.adjoint();
        A = A1;

        // let's make sure the matrix is not singular or near singular
        for (int k=0; k<3; ++k)
        {
          MatrixType A2 = MatrixType::Random(A.rows(),A.cols());
          A1 += A2 * A2.adjoint();
        }
        A = A1;
    }
}

// Actual solver code to be tested
template<typename Solver, typename MatrixType, typename VectorType>
unsigned int solve(Solver& decomp, const MatrixType& A, const VectorType& b, VectorType& x)
{
    x = decomp.solve(b);
    x -= decomp.solve(2*b);
    x = (decomp.transpose().solve(VectorType::Ones(b.rows(), b.cols()))).eval(); // eval() is necessary for the += operator with transpose()

    // Reuse object
    // compute with matrix
    decomp.compute((A*A.adjoint()).eval());
    x += decomp.solve(-b);

    // compute with expression
    decomp.compute(A*A.adjoint());
    x += decomp.solve(-b);

    return 7;
}

template<typename MatrixType>
struct callbackAmount { static unsigned int eval(const unsigned int& callbackAmount) { return callbackAmount; } };

template<typename MatrixType>
struct callbackAmount<Eigen_AD::internal::Matrix_AD_Wrapper<MatrixType> > { static unsigned int eval(const unsigned int&) { return 0; } };

template<typename Solver, typename MatrixType, typename VectorType, typename ... FArgs>
unsigned int solve(const MatrixType& A, const VectorType& b, VectorType& x, FArgs... fargs)
{
    unsigned int callback_amount = 0;

    // Const constructor
    {
        Solver decomp(A, fargs...);
        callback_amount += 1 + solve(decomp, A, b, x);
    }

    // Const constructor expression
    {
        Solver decomp(A.eval(), fargs...);
        callback_amount += 1 + solve(decomp, A, b, x);
    }
    return callbackAmount<MatrixType>::eval(callback_amount);
}

template<typename MatrixTypePassive, typename Solver> struct solve_helper;

template<typename MatrixTypePassive, template<typename, typename> class Solver> struct solve_helper<MatrixTypePassive, Solver<MatrixTypePassive, void> > {
  template<typename MatrixType, typename VectorType, typename ... FArgs>
  static unsigned int eval(const MatrixType& A, const VectorType& b, VectorType& x, FArgs... fargs) {
    return solve<Solver<MatrixType, void> >(A, b, x, fargs...);
  }
};

template<typename MatrixTypePassive, template<typename, int, typename> class Solver, int Arg> struct solve_helper<MatrixTypePassive, Solver<MatrixTypePassive, Arg, void> > {
  template<typename MatrixType, typename VectorType, typename ... FArgs>
  static unsigned int eval(const MatrixType& A, const VectorType& b, VectorType& x, FArgs... fargs) {
    return solve<Solver<MatrixType, Arg, void> >(A, b, x, fargs...);
  }
};

template<typename Solver, typename MatrixType, typename VectorType, typename MatrixTypePassive, typename VectorTypePassive, typename ... FArgs>
void active_solve_t1(const MatrixTypePassive& _A, MatrixTypePassive&,
                     const VectorTypePassive& _b, VectorTypePassive&,
                     VectorTypePassive& _x, VectorTypePassive& _x_t1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    VectorType b(_b.rows(), _b.cols()), x(_x.rows(), _x.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
            dco::derivative(A(i,j)) = 1.0;
        }
        dco::value(b(i,0)) = _b(i,0);
        dco::derivative(b(i,0)) = 1.0;
    }

    // Solve
    solve_helper<MatrixTypePassive, Solver>::eval(A, b, x, fargs...);

    // Save output
    for(int i=0; i<A.rows(); i++) {
        _x(i,0)    = dco::value(x(i,0));
        _x_t1(i,0) = dco::derivative(x(i,0));
    }
}

template<typename Solver, typename MatrixType, typename VectorType, typename MatrixTypePassive, typename VectorTypePassive, typename ... FArgs>
void active_solve_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1,
                     const VectorTypePassive& _b, VectorTypePassive& _b_a1,
                     VectorTypePassive& _x, const VectorTypePassive& _x_a1,
                     FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    VectorType b(_b.rows(), _b.cols()), x(_x.rows(), _x.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
        }
        dco::value(b(i,0)) = _b(i,0);
    }

    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
        }
        DCO_M::global_tape->register_variable(b(i,0));
    }

    // Solve
    unsigned int callback_amount = solve_helper<MatrixTypePassive, Solver>::eval(A, b, x, fargs...);

    // Seed
    for(int i=0; i<A.rows(); i++) {
        DCO_M::global_tape->register_output_variable(x(i,0));
        dco::derivative(x(i,0)) = _x_a1(i);
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Save output
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i,j) = dco::derivative(A(i,j));
        }
        _b_a1(i,0) = dco::derivative(b(i,0));
        _x(i,0)    = dco::value(x(i,0));
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Solver, typename MatrixType, typename VectorType, typename MatrixTypeVector, typename VectorTypeVector, typename MatrixTypePassive, typename VectorTypePassive, typename ... FArgs>
void active_solve_a1v(const MatrixTypePassive& _A, const VectorTypePassive& _b, FArgs... fargs)
{
    // Determine two random output adjoint vectors (either 0 or 1 per element)
    Matrix<typename MatrixTypePassive::Scalar, MatrixType::RowsAtCompileTime, 2> x_a1;
    do {
        x_a1 = ((Matrix<double, MatrixType::RowsAtCompileTime, 2>::Random(_A.rows(), 2).cwiseAbs() + MatrixXd::Constant(_A.rows(), 2, 0.5)).template cast<int>()).template cast<double>();
    } while(x_a1.col(0) == x_a1.col(1) || x_a1.col(0).isZero(0) || x_a1.col(1).isZero(0));

    // Declare and init active types
    MatrixTypeVector A(_A.rows(), _A.cols());
    VectorTypeVector b(_b.rows(), _b.cols()), x(_b.rows(), _b.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
        }
        dco::value(b(i,0)) = _b(i,0);
    }

    // Create tape
    typedef dco::mode<typename MatrixTypeVector::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
        }
        DCO_M::global_tape->register_variable(b(i,0));
    }

    // Solve
    unsigned int callback_amount = solve_helper<MatrixTypePassive, Solver>::eval(A, b, x, fargs...);

    // Seed
    for(int i=0; i<A.rows(); i++) {
      DCO_M::global_tape->register_output_variable(x(i));
    }
    for(int i=0; i<A.rows(); i++) {
      for(int j=0; j<2; j++) dco::derivative(x(i))[j] = x_a1(i,j);
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Harvest
    MatrixTypePassive A_vec_0(A.rows(), A.cols()), A_vec_1(A.rows(), A.cols());
    VectorTypePassive b_vec_0(b.rows()), b_vec_1(b.rows()), x_vec_0(x.rows());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            A_vec_0(i,j) = dco::derivative(A(i,j))[0];
            A_vec_1(i,j) = dco::derivative(A(i,j))[1];
        }
        b_vec_0(i) = dco::derivative(b(i))[0];
        b_vec_1(i) = dco::derivative(b(i))[1];
        x_vec_0(i) = dco::value(x(i));
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);

    // Compute reference results
    MatrixTypePassive A_sym_a1_0(_A.rows(), _A.cols()), A_sym_a1_1(_A.rows(), _A.cols());
    VectorTypePassive b_sym_a1_0(_b.rows(), _b.cols()), b_sym_a1_1(_b.rows(), _b.cols());
    VectorTypePassive x_sym_0(_b.rows(), _b.cols()), x_sym_1(_b.rows(), _b.cols()), x_sym_a1_0 = x_a1.col(0), x_sym_a1_1 = x_a1.col(1);
    active_solve_a1<Solver, MatrixType, VectorType>(_A, A_sym_a1_0, _b, b_sym_a1_0, x_sym_0, x_sym_a1_0, fargs...);
    active_solve_a1<Solver, MatrixType, VectorType>(_A, A_sym_a1_1, _b, b_sym_a1_1, x_sym_1, x_sym_a1_1, fargs...);

    // Check results
    VERIFY_IS_APPROX(x_sym_0, x_vec_0);
    VERIFY_IS_APPROX(A_sym_a1_0, A_vec_0);
    VERIFY_IS_APPROX(b_sym_a1_0, b_vec_0);
    VERIFY_IS_APPROX(A_sym_a1_1, A_vec_1);
    VERIFY_IS_APPROX(b_sym_a1_1, b_vec_1);
}

template<typename  Solver, typename MatrixType, typename VectorType, typename MatrixTypePassive, typename VectorTypePassive, typename ... FArgs>
void active_solve_t2_t1(const MatrixTypePassive& _A, const VectorTypePassive& _b,
                              VectorTypePassive& _x, VectorTypePassive& _x_t1, VectorTypePassive& _x_t2, VectorTypePassive& _x_t2_t1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    VectorType b(_b.rows(), _b.cols()), x(_x.rows(), _x.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
            dco::derivative(dco::value(A(i,j))) = 1.0;
            dco::value(dco::derivative(A(i,j))) = 1.0;
            dco::derivative(dco::derivative(A(i,j))) = 1.0;
        }
        dco::value(dco::value(b(i,0))) = _b(i,0);
        dco::derivative(dco::value(b(i,0))) = 1.0;
        dco::value(dco::derivative(b(i,0))) = 1.0;
        dco::derivative(dco::derivative(b(i,0))) = 1.0;
    }

    // Solve
    solve_helper<MatrixTypePassive, Solver>::eval(A, b, x, fargs...);

    // Save output
    for(int i=0; i<A.rows(); i++) {
        _x(i,0)    = dco::value(dco::value(x(i,0)));
        _x_t1(i,0) = dco::derivative(dco::value(x(i,0)));
        _x_t2(i,0) = dco::value(dco::derivative(x(i,0)));
        _x_t2_t1(i,0) = dco::derivative(dco::derivative(x(i,0)));
    }
}

template<typename  Solver, typename MatrixType, typename VectorType, typename MatrixTypePassive, typename VectorTypePassive, typename ... FArgs>
void active_solve_t2_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1, MatrixTypePassive& _A_t2_a1,
                        const VectorTypePassive& _b, VectorTypePassive& _b_a1, VectorTypePassive& _b_t2_a1,
                              VectorTypePassive& _x, VectorTypePassive& _x_t2, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    VectorType b(_b.rows(), _b.cols()), x(_x.rows(), _x.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
        }
        dco::value(dco::value(b(i,0))) = _b(i,0);
    }

    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
            dco::derivative(dco::value(A(i,j))) = 1.0;
        }
        DCO_M::global_tape->register_variable(b(i,0));
        dco::derivative(dco::value(b(i,0))) = 1.0;
    }

    // Solve
    unsigned int callback_amount = solve_helper<MatrixTypePassive, Solver>::eval(A, b, x, fargs...);

    // Seed
    for(int i=0; i<A.rows(); i++) {
      DCO_M::global_tape->register_output_variable(x(i,0));
      dco::derivative(x(i,0)) = 1.0;
      dco::derivative(dco::derivative(x(i,0))) = 1.0;
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i, j) = dco::value(dco::derivative(A(i,j)));
            _A_t2_a1(i, j) = dco::derivative(dco::derivative(A(i,j)));
        }
        _b_a1(i, 0) = dco::value(dco::derivative(b(i,0)));
        _b_t2_a1(i, 0) = dco::derivative(dco::derivative(b(i,0)));
        _x(i, 0) = dco::value(dco::value(x(i,0)));
        _x_t2(i, 0) = dco::derivative(dco::value(x(i,0)));
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Solver, typename MatrixType, typename VectorType, typename MatrixTypePassive, typename VectorTypePassive, typename ... FArgs>
void active_solve_a2_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1, MatrixTypePassive& _A_a2,
                        const VectorTypePassive& _b, VectorTypePassive& _b_a1, VectorTypePassive& _b_a2,
                              VectorTypePassive& _x, VectorTypePassive& _x_a2_a1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    VectorType b(_b.rows(), _b.cols()), x(_x.rows(), _x.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
        }
        dco::value(dco::value(b(i,0))) = _b(i,0);
    }

    // Create tapes
    typedef dco::mode<typename MatrixType::Scalar::VALUE_TYPE> DCO_BASE_M;
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;

    DCO_BASE_M::global_tape = DCO_BASE_M::tape_t::create();
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_BASE_M::global_tape->register_variable(dco::value(A(i,j)));
            DCO_M::global_tape->register_variable(A(i,j));
        }
        DCO_BASE_M::global_tape->register_variable(dco::value(b(i,0)));
        DCO_M::global_tape->register_variable(b(i,0));
    }

    // Solve
    unsigned int callback_amount = solve_helper<MatrixTypePassive, Solver>::eval(A, b, x, fargs...);

    // Seed
    for(int i=0; i<A.rows(); i++) {
        dco::derivative(x(i,0)) = 1.0;
        DCO_BASE_M::global_tape->register_variable(dco::derivative(x(i,0)));
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Seed and save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i, j) = dco::value(dco::derivative(A(i,j)));
            dco::derivative(dco::derivative(A(i,j))) = 1.0;
        }
        _b_a1(i, 0) = dco::value(dco::derivative(b(i,0)));
        dco::derivative(dco::derivative(b(i,0))) = 1.0;
        dco::derivative(dco::value(x(i,0))) = 1.0;
    }

    // Calculate derivatives
    DCO_BASE_M::global_tape->interpret_adjoint();

    // Save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a2(i, j) = dco::derivative(dco::value(A(i,j)));
        }
        _b_a2(i, 0) = dco::derivative(dco::value(b(i,0)));
        _x(i, 0) = dco::value(dco::value(x(i,0)));
        _x_a2_a1(i, 0) = dco::derivative(dco::derivative(x(i,0)));
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tapes
    DCO_BASE_M::tape_t::remove(DCO_BASE_M::global_tape);
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Solver, int Requirements = None, typename ... FArgs> void symbolic_solvers_test_1st_order(FArgs... fargs)
{
    typedef typename Solver::Scalar Scalar;
    static const unsigned int Rows = Solver::RowsAtCompileTime;
    static const unsigned int Cols = Solver::ColsAtCompileTime;

    typedef Matrix<Scalar, Rows, Cols>                                               MatrixType;
    typedef Matrix<Scalar, Rows, 1>                                                  VectorType;
    typedef Matrix<typename dco::gt1s<Scalar>::type, Rows, Cols>                     MatrixTypeTangent;
    typedef Matrix<typename dco::gt1s<Scalar>::type, Rows, 1>                        VectorTypeTangent;
    typedef Matrix<typename dco::ga1s<Scalar>::type, Rows, Cols>                     MatrixTypeSymbolic;
    typedef Matrix<typename dco::ga1s<Scalar>::type, Rows, 1>                        VectorTypeSymbolic;
    typedef Matrix<typename dco::ga1v<Scalar, 2>::type, Rows, Cols>                  MatrixTypeSymbolicVector;
    typedef Matrix<typename dco::ga1v<Scalar, 2>::type, Rows, 1>                     VectorTypeSymbolicVector;

    MatrixType A;
    VectorType b;

    setupSystem<Requirements>(A, b);

    MatrixType A_tan(A.rows(), A.cols()), A_tan_t1(A.rows(), A.cols()), A_sym(A.rows(), A.cols()), A_sym_a1(A.rows(), A.cols());
    VectorType b_tan(b.rows(), b.cols()), b_tan_t1(b.rows(), b.cols()), b_sym(b.rows(), b.cols()), b_sym_a1(b.rows(), b.cols());
    VectorType x(b.rows(), b.cols()), x_tan(b.rows(), b.cols()), x_tan_t1(b.rows(), b.cols()), x_sym(b.rows(), b.cols()), x_sym_a1 = VectorType::Ones(A.rows());

    A_tan   = A; b_tan   = b;
    A_sym   = A; b_sym   = b;

    solve          <Solver, MatrixType,         VectorType>        (A, b, x, fargs...);
    active_solve_t1<Solver, MatrixTypeTangent,  VectorTypeTangent> (A_tan, A_tan_t1, b_tan, b_tan_t1, x_tan, x_tan_t1, fargs...);
    active_solve_a1<Solver, MatrixTypeSymbolic, VectorTypeSymbolic>(A_sym, A_sym_a1, b_sym, b_sym_a1, x_sym, x_sym_a1, fargs...);

    // Check results
    VERIFY_IS_APPROX(x_sym, x);
    VERIFY_IS_APPROX(x_tan, x);
    VERIFY_IS_APPROX(x_tan_t1.sum(), A_sym_a1.sum()+b_sym_a1.sum());

    // Check vector mode
    if(A.rows() > 1) {
        active_solve_a1v<Solver, MatrixTypeSymbolic, VectorTypeSymbolic, MatrixTypeSymbolicVector, VectorTypeSymbolicVector>(A_sym, b_sym, fargs...);
    }

    // Verify against plain AAD
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    typedef Eigen_AD::internal::Matrix_AD_Wrapper<Matrix<typename dco::ga1s<Scalar>::type, Rows, Cols> > MatrixTypeAdjoint;
    typedef Eigen_AD::internal::Matrix_AD_Wrapper<Matrix<typename dco::ga1s<Scalar>::type, Rows, 1> >    VectorTypeAdjoint;

    MatrixType A_adj(A.rows(), A.cols()), A_adj_a1(A.rows(), A.cols());
    VectorType b_adj(b.rows(), b.cols()), b_adj_a1(b.rows(), b.cols());
    VectorType x_adj(b.rows(), b.cols());

    A_adj = A;
    b_adj = b;

    active_solve_a1<Solver, MatrixTypeAdjoint, VectorTypeAdjoint>(A_adj, A_adj_a1, b_adj, b_adj_a1, x_adj, x_sym_a1, fargs...);

    VERIFY_IS_APPROX(x_sym, x_adj);
    VERIFY_IS_APPROX(A_sym_a1, A_adj_a1);
    VERIFY_IS_APPROX(b_sym_a1, b_adj_a1);
#endif
}

template<typename Solver, int Requirements = None, typename ... FArgs> void symbolic_solvers_test_2nd_order(FArgs... fargs)
{
    typedef typename Solver::Scalar Scalar;
    static const unsigned int Rows = Solver::RowsAtCompileTime;
    static const unsigned int Cols = Solver::ColsAtCompileTime;

    typedef Matrix<Scalar, Rows, Cols> MatrixType;
    typedef Matrix<Scalar, Rows, 1>    VectorType;
    typedef Matrix<typename dco::gt1s<typename dco::gt1s<Scalar>::type>::type, Rows, Cols>                     MatrixTypeTangentTangent;
    typedef Matrix<typename dco::gt1s<typename dco::gt1s<Scalar>::type>::type, Rows, 1>                        VectorTypeTangentTangent;
    typedef Matrix<typename dco::ga1s<typename dco::gt1s<Scalar>::type>::type, Rows, Cols>                     MatrixTypeTangentSymbolic;
    typedef Matrix<typename dco::ga1s<typename dco::gt1s<Scalar>::type>::type, Rows, 1>                        VectorTypeTangentSymbolic;
    typedef Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, Cols>                     MatrixTypeSymbolicSymbolic;
    typedef Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, 1>                        VectorTypeSymbolicSymbolic;

    MatrixType A;
    VectorType b;

    setupSystem<Requirements>(A, b);

    MatrixType A_tantan(A.rows(), A.cols()), A_tansym(A.rows(), A.cols()), A_tansym_a1(A.rows(), A.cols()), A_tansym_t2_a1(A.rows(), A.cols()), A_symsym(A.rows(), A.cols()), A_symsym_a1(A.rows(), A.cols()), A_symsym_a2(A.rows(), A.cols());
    VectorType b_tantan(b.rows(), b.cols()), b_tansym(b.rows(), b.cols()), b_tansym_a1(b.rows(), b.cols()), b_tansym_t2_a1(b.rows(), b.cols()), b_symsym(b.rows(), b.cols()), b_symsym_a1(b.rows(), b.cols()), b_symsym_a2(b.rows(), b.cols());
    VectorType x(b.rows(), b.cols()), x_tantan(b.rows(), b.cols()), x_tantan_t1(b.rows(), b.cols()), x_tantan_t2(b.rows(), b.cols()), x_tantan_t2_t1(b.rows(), b.cols()), x_tansym(b.rows(), b.cols()), x_tansym_t2(b.rows(), b.cols()), x_symsym(b.rows(), b.cols()), x_symsym_a2_a1(b.rows(), b.cols());

    A_tantan = A; b_tantan = b;
    A_tansym = A; b_tansym = b;
    A_symsym = A; b_symsym = b;

    solve             <Solver, MatrixType,                   VectorType>                (A, b, x, fargs...);
    active_solve_t2_t1<Solver, MatrixTypeTangentTangent,     VectorTypeTangentTangent>  (A_tantan, b_tantan, x_tantan, x_tantan_t1, x_tantan_t2, x_tantan_t2_t1, fargs...);
    active_solve_t2_a1<Solver, MatrixTypeTangentSymbolic,    VectorTypeTangentSymbolic> (A_tansym, A_tansym_a1, A_tansym_t2_a1, b_tansym, b_tansym_a1, b_tansym_t2_a1, x_tansym, x_tansym_t2, fargs...);
    active_solve_a2_a1<Solver, MatrixTypeSymbolicSymbolic,   VectorTypeSymbolicSymbolic>(A_symsym, A_symsym_a1, A_symsym_a2, b_symsym, b_symsym_a1, b_symsym_a2, x_symsym, x_symsym_a2_a1, fargs...);

    // Check primal
    VERIFY_IS_APPROX(x_tantan, x);
    VERIFY_IS_APPROX(x_tansym, x);
    VERIFY_IS_APPROX(x_symsym, x);

    // Check ToA 1st orders
    VERIFY_IS_APPROX(x_tantan_t1, x_tantan_t2);
    VERIFY_IS_APPROX(x_tansym_t2, x_tantan_t2);
    VERIFY_IS_APPROX(A_tansym_a1.sum() + b_tansym_a1.sum(), x_tantan_t2.sum());

    // Check AoA 1st orders
    VERIFY_IS_APPROX(A_symsym_a1, A_tansym_a1);
    VERIFY_IS_APPROX(b_symsym_a1, b_tansym_a1);
    VERIFY_IS_APPROX(x_symsym_a2_a1, x_tansym_t2);

    // Check ToA 2nd orders
    VERIFY_IS_APPROX(A_tansym_t2_a1.sum() + b_tansym_t2_a1.sum(), x_tantan_t2_t1.sum());

    // Check AoA 2nd orders
    VERIFY_IS_APPROX(A_symsym_a2.sum() + b_symsym_a2.sum(), x_tantan_t2_t1.sum());

    // Verify against plain AAD
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    typedef Eigen_AD::internal::Matrix_AD_Wrapper<Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, Cols> > MatrixTypeAdjointAdjoint;
    typedef Eigen_AD::internal::Matrix_AD_Wrapper<Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, 1>    > VectorTypeAdjointAdjoint;

    MatrixType A_adjadj(A.rows(), A.cols()), A_adjadj_a1(A.rows(), A.cols()), A_adjadj_a2(A.rows(), A.cols());
    VectorType b_adjadj(b.rows(), b.cols()), b_adjadj_a1(b.rows(), b.cols()), b_adjadj_a2(b.rows(), b.cols());
    VectorType x_adjadj(b.rows(), b.cols()), x_adjadj_a2_a1(b.rows(), b.cols());

    A_adjadj = A;
    b_adjadj = b;

    active_solve_a2_a1<Solver, MatrixTypeAdjointAdjoint, VectorTypeAdjointAdjoint>(A_adjadj, A_adjadj_a1, A_adjadj_a2, b_adjadj, b_adjadj_a1, b_adjadj_a2, x_adjadj, x_adjadj_a2_a1, fargs...);

    VERIFY_IS_APPROX(x_symsym, x_adjadj);
    VERIFY_IS_APPROX(A_symsym_a1, A_adjadj_a1);
    VERIFY_IS_APPROX(A_symsym_a2, A_adjadj_a2);
    VERIFY_IS_APPROX(b_symsym_a1, b_adjadj_a1);
    VERIFY_IS_APPROX(b_symsym_a2, b_adjadj_a2);
    VERIFY_IS_APPROX(x_symsym_a2_a1, x_adjadj_a2_a1);
#endif
}

#endif // EIGEN_AD_TEST_DCO_SOLVERS_H
