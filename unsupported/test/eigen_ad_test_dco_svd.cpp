// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "eigen_ad_test_dco_solvers.h"
#include <Eigen/SVD>

template<typename SvdType, typename MatrixType> void svd_verify_assert(const MatrixType& m)
{
    typedef typename MatrixType::Scalar Scalar;
    typedef typename MatrixType::Index Index;
    Index rows = m.rows();
    Index cols = m.cols();

    enum {
      RowsAtCompileTime = MatrixType::RowsAtCompileTime,
      ColsAtCompileTime = MatrixType::ColsAtCompileTime
    };

    typedef Matrix<Scalar, RowsAtCompileTime, 1> RhsType;
    RhsType rhs(rows);
    SvdType svd;
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    VERIFY_RAISES_ASSERT(svd.matrixU())
    VERIFY_RAISES_ASSERT(svd.singularValues())
    VERIFY_RAISES_ASSERT(svd.matrixV())
#endif
    VERIFY_RAISES_ASSERT(svd.solve(rhs))
    MatrixType a = MatrixType::Zero(rows, cols);
    a.setZero();
    svd.compute(a, 0);
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    VERIFY_RAISES_ASSERT(svd.matrixU())
    VERIFY_RAISES_ASSERT(svd.matrixV())
    VERIFY_RAISES_ASSERT(svd.singularValues())
#endif
    VERIFY_RAISES_ASSERT(svd.solve(rhs))

    if (ColsAtCompileTime == Dynamic)
    {
      svd.compute(a, ComputeThinU);
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
      svd.matrixU();
      VERIFY_RAISES_ASSERT(svd.matrixV())
#endif
      VERIFY_RAISES_ASSERT(svd.solve(rhs))
      svd.compute(a, ComputeThinV);
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
      svd.matrixV();
      VERIFY_RAISES_ASSERT(svd.matrixU())
#endif
      VERIFY_RAISES_ASSERT(svd.solve(rhs))
    }
    else
    {
      VERIFY_RAISES_ASSERT(svd.compute(a, ComputeThinU))
      VERIFY_RAISES_ASSERT(svd.compute(a, ComputeThinV))
    }
}

template<typename MatrixType> void jacobisvd_verify_assert()
{
  // Create tape
  typedef dco::mode<typename MatrixType::Scalar> DCO_M;
  DCO_M::global_tape = DCO_M::tape_t::create();

  MatrixType tmp;

  svd_verify_assert<JacobiSVD<MatrixType> >(tmp);
  typedef typename MatrixType::Index Index;
  Index rows = tmp.rows();
  Index cols = tmp.cols();

  enum {
    ColsAtCompileTime = MatrixType::ColsAtCompileTime
  };

  MatrixType a = MatrixType::Zero(rows, cols);
  a.setZero();

  if (ColsAtCompileTime == Dynamic)
  {
    JacobiSVD<MatrixType, FullPivHouseholderQRPreconditioner> svd_fullqr;
    VERIFY_RAISES_ASSERT(svd_fullqr.compute(a, ComputeFullU|ComputeThinV))
    VERIFY_RAISES_ASSERT(svd_fullqr.compute(a, ComputeThinU|ComputeThinV))
    VERIFY_RAISES_ASSERT(svd_fullqr.compute(a, ComputeThinU|ComputeFullV))
  }

  // Remove tape
  DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename MatrixType> void bdcsvd_verify_assert()
{
  // Create tape
  typedef dco::mode<typename MatrixType::Scalar> DCO_M;
  DCO_M::global_tape = DCO_M::tape_t::create();

  MatrixType tmp;

  svd_verify_assert<BDCSVD<MatrixType> >(tmp);

  // Remove tape
  DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename MatrixType> void svd_verify_wrapping()
{
    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Passive Matrix
    typedef Matrix<typename DCO_M::passive_t, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> PassiveMatrixType;
    typedef Matrix<typename DCO_M::passive_t, MatrixType::RowsAtCompileTime, 1> PassiveVectorType;
    PassiveMatrixType B;
    PassiveVectorType rhs;
    setupSystem<PosDefinite>(B, rhs);
    MatrixType A = B;

    // JacobiSVD tests
    JacobiSVD<MatrixType> jsvd(A, ComputeFullU|ComputeFullV);
    JacobiSVD<PassiveMatrixType> jsvd_passive(B, ComputeFullU|ComputeFullV);
    VERIFY_IS_EQUAL(jsvd.nonzeroSingularValues(), jsvd_passive.nonzeroSingularValues());
    VERIFY_IS_EQUAL(jsvd.rank(), jsvd_passive.rank());
    VERIFY_IS_EQUAL(jsvd.threshold(), jsvd_passive.threshold());
    VERIFY_IS_EQUAL(jsvd.computeU(), jsvd_passive.computeU());
    VERIFY_IS_EQUAL(jsvd.computeV(), jsvd_passive.computeV());
    VERIFY_IS_EQUAL(jsvd.rows(), jsvd_passive.rows());
    VERIFY_IS_EQUAL(jsvd.cols(), jsvd_passive.cols());

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    VERIFY_IS_EQUAL(jsvd.matrixU(), jsvd_passive.matrixU());
    VERIFY_IS_EQUAL(jsvd.matrixV(), jsvd_passive.matrixV());
    VERIFY_IS_EQUAL(jsvd.singularValues(), jsvd_passive.singularValues());
#endif

    // BDCSVD tests
    BDCSVD<MatrixType> bdcsvd(A, ComputeFullU|ComputeFullV);
    BDCSVD<PassiveMatrixType> bdcsvd_passive(B, ComputeFullU|ComputeFullV);
    VERIFY_IS_EQUAL(bdcsvd.nonzeroSingularValues(), bdcsvd_passive.nonzeroSingularValues());
    VERIFY_IS_EQUAL(bdcsvd.rank(), bdcsvd_passive.rank());
    VERIFY_IS_EQUAL(bdcsvd.threshold(), bdcsvd_passive.threshold());
    VERIFY_IS_EQUAL(bdcsvd.computeU(), bdcsvd_passive.computeU());
    VERIFY_IS_EQUAL(bdcsvd.computeV(), bdcsvd_passive.computeV());
    VERIFY_IS_EQUAL(bdcsvd.rows(), bdcsvd_passive.rows());
    VERIFY_IS_EQUAL(bdcsvd.cols(), bdcsvd_passive.cols());

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    VERIFY_IS_EQUAL(bdcsvd.matrixU(), bdcsvd_passive.matrixU());
    VERIFY_IS_EQUAL(bdcsvd.matrixV(), bdcsvd_passive.matrixV());
    VERIFY_IS_EQUAL(bdcsvd.singularValues(), bdcsvd_passive.singularValues());
#endif

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

void test_eigen_ad_test_dco_svd()
{
  CALL_SUBTEST_1(( symbolic_solvers_test_1st_order<JacobiSVD<Matrix<double, 3, 3> > >(ComputeFullU|ComputeFullV) ));
  CALL_SUBTEST_2(( symbolic_solvers_test_1st_order<JacobiSVD<Matrix<double, Dynamic, Dynamic> > >(ComputeFullU|ComputeFullV) ));
  CALL_SUBTEST_3(( symbolic_solvers_test_1st_order<JacobiSVD<Matrix<double, 3, 3>, NoQRPreconditioner>, None>(ComputeFullU|ComputeFullV) ));             // TODO: Only relevant for non-square matrices
  CALL_SUBTEST_4(( symbolic_solvers_test_1st_order<JacobiSVD<Matrix<double, Dynamic, Dynamic>, NoQRPreconditioner>, None>(ComputeFullU|ComputeFullV) )); // TODO: Only relevant for non-square matrices
  CALL_SUBTEST_5(( symbolic_solvers_test_2nd_order<JacobiSVD<Matrix<double, 3, 3> > >(ComputeFullU|ComputeFullV) ));
  CALL_SUBTEST_6(( symbolic_solvers_test_2nd_order<JacobiSVD<Matrix<double, Dynamic, Dynamic> > >(ComputeFullU|ComputeFullV) ));
  CALL_SUBTEST_7(( symbolic_solvers_test_2nd_order<JacobiSVD<Matrix<double, 3, 3>, NoQRPreconditioner>, None>(ComputeFullU|ComputeFullV) ));             // TODO: Only relevant for non-square matrices
  CALL_SUBTEST_8(( symbolic_solvers_test_2nd_order<JacobiSVD<Matrix<double, Dynamic, Dynamic>, NoQRPreconditioner>, None>(ComputeFullU|ComputeFullV) )); // TODO: Only relevant for non-square matrices
  CALL_SUBTEST_9(( symbolic_solvers_test_1st_order<BDCSVD<Matrix<double, 3, 3> > >(ComputeFullU|ComputeFullV) ));
  CALL_SUBTEST_10(( symbolic_solvers_test_1st_order<BDCSVD<Matrix<double, Dynamic, Dynamic> > >(ComputeFullU|ComputeFullV) )); // TODO: Wrong tangent results!
  CALL_SUBTEST_11(( symbolic_solvers_test_2nd_order<BDCSVD<Matrix<double, 3, 3> > >(ComputeFullU|ComputeFullV) ));
  CALL_SUBTEST_12(( symbolic_solvers_test_2nd_order<BDCSVD<Matrix<double, Dynamic, Dynamic> > >(ComputeFullU|ComputeFullV) )); // TODO: Wrong tangent results!
  CALL_SUBTEST_13(( jacobisvd_verify_assert<Matrix3dco_a1d>() ));
  CALL_SUBTEST_14(( svd_verify_wrapping<Matrix3dco_a1d>() ));
}
