// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "eigen_ad_test_dco_solvers.h"
#include <Eigen/Cholesky>

template<typename MatrixType> void cholesky_verify_assert()
{
  MatrixType tmp;

  // LLT tests
  LLT<MatrixType, Upper> llt;
  VERIFY_RAISES_ASSERT(llt.info())
  VERIFY_RAISES_ASSERT(llt.rcond());
  VERIFY_RAISES_ASSERT(llt.solve(tmp))

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_RAISES_ASSERT(llt.matrixL())
  VERIFY_RAISES_ASSERT(llt.matrixLLT())
  VERIFY_RAISES_ASSERT(llt.matrixU())
#endif

  // LDLT tests
  LDLT<MatrixType> ldlt;
  VERIFY_RAISES_ASSERT(ldlt.info())
  VERIFY_RAISES_ASSERT(ldlt.rcond())
  VERIFY_RAISES_ASSERT(ldlt.setZero())
  VERIFY_RAISES_ASSERT(ldlt.transpositionsP())
  VERIFY_RAISES_ASSERT(ldlt.isPositive())
  VERIFY_RAISES_ASSERT(ldlt.isNegative())
  VERIFY_RAISES_ASSERT(ldlt.solve(tmp))

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_RAISES_ASSERT(ldlt.matrixL())
  VERIFY_RAISES_ASSERT(ldlt.matrixLDLT())
  VERIFY_RAISES_ASSERT(ldlt.matrixU())
  VERIFY_RAISES_ASSERT(ldlt.vectorD())
#endif
}

template<typename MatrixType> void cholesky_verify_wrapping()
{
  // Create tape
  typedef dco::mode<typename MatrixType::Scalar> DCO_M;
  DCO_M::global_tape = DCO_M::tape_t::create();

  // Passive Matrix
  typedef Matrix<typename DCO_M::passive_t, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> PassiveMatrixType;
  typedef Matrix<typename DCO_M::passive_t, MatrixType::RowsAtCompileTime, 1> PassiveVectorType;
  PassiveMatrixType B;
  PassiveVectorType rhs;
  setupSystem<PosDefinite>(B, rhs);
  MatrixType A = B;

  // LLT tests
  LLT<MatrixType> llt(A);
  LLT<PassiveMatrixType> llt_passive(B);
  VERIFY_IS_EQUAL(llt.cols(), llt_passive.cols());
  VERIFY_IS_EQUAL(llt.info(), llt_passive.info());
  VERIFY_IS_EQUAL(llt.rcond(), llt_passive.rcond());
  VERIFY_IS_EQUAL(llt.rows(), llt_passive.rows());

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_IS_EQUAL(llt.matrixL(), llt_passive.matrixL());
  VERIFY_IS_EQUAL(llt.matrixLLT(), llt_passive.matrixLLT());
  VERIFY_IS_EQUAL(llt.matrixU(), llt_passive.matrixU());
  // TODO: rankUpdate
#endif

  // LDLT tests
  LDLT<MatrixType> ldlt(A);
  LDLT<PassiveMatrixType> ldlt_passive(B);
  VERIFY_IS_EQUAL(ldlt.cols(), ldlt_passive.cols());
  VERIFY_IS_EQUAL(ldlt.info(), ldlt_passive.info());
  VERIFY_IS_EQUAL(ldlt.isPositive(), ldlt_passive.isPositive());
  VERIFY_IS_EQUAL(ldlt.isNegative(), ldlt_passive.isNegative());
  VERIFY_IS_EQUAL(ldlt.rcond(), ldlt_passive.rcond());
  VERIFY_IS_EQUAL(ldlt.rows(), ldlt_passive.rows());
  VERIFY_IS_EQUAL(ldlt.transpositionsP().indices(), ldlt_passive.transpositionsP().indices());

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_IS_EQUAL(ldlt.matrixL(), ldlt_passive.matrixL());
  VERIFY_IS_EQUAL(ldlt.matrixLLT(), ldlt_passive.matrixLLT());
  VERIFY_IS_EQUAL(ldlt.matrixU(), ldlt_passive.matrixU());
  VERIFY_IS_EQUAL(ldlt.vectorD(), ldlt_passive.vectorD());
  // TODO: rankUpdate
#endif
}

void test_eigen_ad_test_dco_cholesky()
{
  CALL_SUBTEST_1(( symbolic_solvers_test_1st_order<LLT<Matrix<double, 3, 3> >, PosDefinite>() ));
  CALL_SUBTEST_2(( symbolic_solvers_test_1st_order<LLT<Matrix<double, Dynamic, Dynamic> >, PosDefinite>() ));
  CALL_SUBTEST_3(( symbolic_solvers_test_1st_order<LLT<Matrix<double, 3, 3>, Upper>, PosDefinite>() ));
  CALL_SUBTEST_4(( symbolic_solvers_test_1st_order<LLT<Matrix<double, Dynamic, Dynamic>, Upper>, PosDefinite>() ));
  CALL_SUBTEST_5(( symbolic_solvers_test_2nd_order<LLT<Matrix<double, 3, 3> >, PosDefinite>() ));
  CALL_SUBTEST_6(( symbolic_solvers_test_2nd_order<LLT<Matrix<double, Dynamic, Dynamic> >, PosDefinite>() ));
  CALL_SUBTEST_7(( symbolic_solvers_test_2nd_order<LLT<Matrix<double, 3, 3>, Upper>, PosDefinite>() ));
  CALL_SUBTEST_8(( symbolic_solvers_test_2nd_order<LLT<Matrix<double, Dynamic, Dynamic>, Upper>, PosDefinite>() ));
  CALL_SUBTEST_9(( symbolic_solvers_test_1st_order<LDLT<Matrix<double, 3, 3> >, PosDefinite>() ));
  CALL_SUBTEST_10(( symbolic_solvers_test_1st_order<LDLT<Matrix<double, Dynamic, Dynamic> >, PosDefinite>() ));
  CALL_SUBTEST_11(( symbolic_solvers_test_1st_order<LDLT<Matrix<double, 3, 3>, Upper>, PosDefinite>() ));
  CALL_SUBTEST_12(( symbolic_solvers_test_1st_order<LDLT<Matrix<double, Dynamic, Dynamic>, Upper>, PosDefinite>() ));
  CALL_SUBTEST_13(( symbolic_solvers_test_2nd_order<LDLT<Matrix<double, 3, 3> >, PosDefinite>() ));
  CALL_SUBTEST_14(( symbolic_solvers_test_2nd_order<LDLT<Matrix<double, Dynamic, Dynamic> >, PosDefinite>() ));
  CALL_SUBTEST_15(( symbolic_solvers_test_2nd_order<LDLT<Matrix<double, 3, 3>, Upper>, PosDefinite>() ));
  CALL_SUBTEST_16(( symbolic_solvers_test_2nd_order<LDLT<Matrix<double, Dynamic, Dynamic>, Upper>, PosDefinite>() ));
  CALL_SUBTEST_17(( cholesky_verify_assert<Matrix3dco_a1d>() ));
  CALL_SUBTEST_18(( cholesky_verify_wrapping<Matrix3dco_a1d>() ));
}
