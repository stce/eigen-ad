// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "eigen_ad_test_dco_solvers.h"
#include "eigen_ad_test_dco_decompositions.h"
#include "eigen_ad_test_dco_inverse.h"
#include <Eigen/QR>

template<typename MatrixType> void qr_verify_assert()
{
  MatrixType tmp;

  HouseholderQR<MatrixType> qr;
  VERIFY_RAISES_ASSERT(qr.solve(tmp))
  VERIFY_RAISES_ASSERT(qr.logAbsDeterminant())

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
      VERIFY_RAISES_ASSERT(qr.householderQ())
      VERIFY_RAISES_ASSERT(qr.matrixQR())
      VERIFY_RAISES_ASSERT(qr.absDeterminant())
#endif

  FullPivHouseholderQR<MatrixType> qrfull;
  VERIFY_RAISES_ASSERT(qrfull.solve(tmp))
  VERIFY_RAISES_ASSERT(qrfull.dimensionOfKernel())
  VERIFY_RAISES_ASSERT(qrfull.isInjective())
  VERIFY_RAISES_ASSERT(qrfull.isSurjective())
  VERIFY_RAISES_ASSERT(qrfull.isInvertible())
  VERIFY_RAISES_ASSERT(qrfull.inverse())
  VERIFY_RAISES_ASSERT(qrfull.logAbsDeterminant())

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
      VERIFY_RAISES_ASSERT(qrfull.matrixQR())
      VERIFY_RAISES_ASSERT(qrfull.matrixQ())
      VERIFY_RAISES_ASSERT(qrfull.absDeterminant())
#endif

  ColPivHouseholderQR<MatrixType> qrcol;
  VERIFY_RAISES_ASSERT(qrcol.solve(tmp))
  VERIFY_RAISES_ASSERT(qrcol.dimensionOfKernel())
  VERIFY_RAISES_ASSERT(qrcol.isInjective())
  VERIFY_RAISES_ASSERT(qrcol.isSurjective())
  VERIFY_RAISES_ASSERT(qrcol.isInvertible())
  VERIFY_RAISES_ASSERT(qrcol.inverse())
  VERIFY_RAISES_ASSERT(qrcol.logAbsDeterminant())

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
      VERIFY_RAISES_ASSERT(qrcol.matrixQR())
      VERIFY_RAISES_ASSERT(qrcol.householderQ())
      VERIFY_RAISES_ASSERT(qrcol.absDeterminant())
#endif

  CompleteOrthogonalDecomposition<MatrixType> qrcod;
  VERIFY_RAISES_ASSERT(qrcod.solve(tmp))
  VERIFY_RAISES_ASSERT(qrcod.dimensionOfKernel())
  VERIFY_RAISES_ASSERT(qrcod.isInjective())
  VERIFY_RAISES_ASSERT(qrcod.isSurjective())
  VERIFY_RAISES_ASSERT(qrcod.isInvertible())
  VERIFY_RAISES_ASSERT(qrcod.logAbsDeterminant())

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
      VERIFY_RAISES_ASSERT(qrcod.matrixQTZ())
      VERIFY_RAISES_ASSERT(qrcod.householderQ())
      VERIFY_RAISES_ASSERT(qrcod.absDeterminant())
#endif
}

template<typename MatrixType> void qr_verify_wrapping()
{
  // Create tape
  typedef dco::mode<typename MatrixType::Scalar> DCO_M;
  DCO_M::global_tape = DCO_M::tape_t::create();

  // Passive Matrix
  typedef Matrix<typename DCO_M::passive_t, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> PassiveMatrixType;
  PassiveMatrixType B = PassiveMatrixType::Random();
  MatrixType A = B;

  // HouseholderQR tests
  HouseholderQR<MatrixType> qr(A);
  HouseholderQR<PassiveMatrixType> qr_passive(B);

  VERIFY_IS_EQUAL(qr.rows(), qr_passive.rows());
  VERIFY_IS_EQUAL(qr.cols(), qr_passive.cols());
  VERIFY_IS_EQUAL(qr.logAbsDeterminant(), qr_passive.logAbsDeterminant());

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_IS_EQUAL(qr.householderQ(), qr_passive.householderQ());
  VERIFY_IS_EQUAL(qr.matrixQR(), qr_passive.matrixQR());
  VERIFY_IS_EQUAL(qr.absDeterminant(), qr_passive.absDeterminant());
  VERIFY_IS_EQUAL(qr.hCoeffs(), qr_passive.hCoeffs());
#endif

  // FullPivHouseholderQR tests
  FullPivHouseholderQR<MatrixType> qrfull(A);
  FullPivHouseholderQR<PassiveMatrixType> qrfull_passive(B);
  VERIFY_IS_EQUAL(qrfull.cols(), qrfull_passive.cols());
  VERIFY_IS_EQUAL(qrfull.colsPermutation().indices(), qrfull_passive.colsPermutation().indices());
  VERIFY_IS_EQUAL(qrfull.dimensionOfKernel(), qrfull_passive.dimensionOfKernel());
  VERIFY_IS_EQUAL(qrfull.isInjective(), qrfull_passive.isInjective());
  VERIFY_IS_EQUAL(qrfull.isInvertible(), qrfull_passive.isInvertible());
  VERIFY_IS_EQUAL(qrfull.isSurjective(), qrfull_passive.isSurjective());
  VERIFY_IS_EQUAL(qrfull.logAbsDeterminant(), qrfull_passive.logAbsDeterminant());
  VERIFY_IS_EQUAL(qrfull.maxPivot(), qrfull_passive.maxPivot());
  VERIFY_IS_EQUAL(qrfull.nonzeroPivots(), qrfull_passive.nonzeroPivots());
  VERIFY_IS_EQUAL(qrfull.rank(), qrfull_passive.rank());
  VERIFY_IS_EQUAL(qrfull.rows(), qrfull_passive.rows());
  VERIFY_IS_EQUAL(qrfull.threshold(), qrfull_passive.threshold());
  MatrixType qr_full_inv = qrfull.inverse();
  PassiveMatrixType qr_full_passive_inv = qrfull_passive.inverse();
  for(unsigned int i=0; i<qr_full_inv.rows(); i++)
    for(unsigned int j=0; j<qr_full_inv.cols(); j++)
      VERIFY_IS_EQUAL(dco::value(qr_full_inv(i,j)), qr_full_passive_inv(i,j));

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_IS_EQUAL(qrfull.matrixQ(), qrfull_passive.matrixQ());
  VERIFY_IS_EQUAL(qrfull.matrixQR(), qrfull_passive.matrixQR());
  VERIFY_IS_EQUAL(qrfull.absDeterminant(), qrfull_passive.absDeterminant());
  VERIFY_IS_EQUAL(qrfull.hCoeffs(), qrfull_passive.hCoeffs());
#endif

  // ColPivHouseholderQR tests
  ColPivHouseholderQR<MatrixType> qrcol(A);
  ColPivHouseholderQR<PassiveMatrixType> qrcol_passive(B);
  VERIFY_IS_EQUAL(qrcol.cols(), qrcol_passive.cols());
  VERIFY_IS_EQUAL(qrcol.colsPermutation().indices(), qrcol_passive.colsPermutation().indices());
  VERIFY_IS_EQUAL(qrcol.dimensionOfKernel(), qrcol_passive.dimensionOfKernel());
  VERIFY_IS_EQUAL(qrcol.info(), qrcol_passive.info());
  VERIFY_IS_EQUAL(qrcol.isInjective(), qrcol_passive.isInjective());
  VERIFY_IS_EQUAL(qrcol.isInvertible(), qrcol_passive.isInvertible());
  VERIFY_IS_EQUAL(qrcol.isSurjective(), qrcol_passive.isSurjective());
  VERIFY_IS_EQUAL(qrcol.logAbsDeterminant(), qrcol_passive.logAbsDeterminant());
  VERIFY_IS_EQUAL(qrcol.maxPivot(), qrcol_passive.maxPivot());
  VERIFY_IS_EQUAL(qrcol.nonzeroPivots(), qrcol_passive.nonzeroPivots());
  VERIFY_IS_EQUAL(qrcol.rank(), qrcol_passive.rank());
  VERIFY_IS_EQUAL(qrcol.rows(), qrcol_passive.rows());
  VERIFY_IS_EQUAL(qrcol.threshold(), qrcol_passive.threshold());
  MatrixType qrcol_inv = qrcol.inverse();
  PassiveMatrixType qrcol_passive_inv = qrcol_passive.inverse();
  for(unsigned int i=0; i<qrcol_inv.rows(); i++)
    for(unsigned int j=0; j<qrcol_inv.cols(); j++)
      VERIFY_IS_EQUAL(dco::value(qrcol_inv(i,j)), qrcol_passive_inv(i,j));

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_IS_EQUAL(qrcol.matrixQR(), qrcol_passive.matrixQR());
  VERIFY_IS_EQUAL(qrcol.matrixR(), qrcol_passive.matrixR());
  VERIFY_IS_EQUAL(qrcol.householderQ(), qrcol_passive.householderQ());
  VERIFY_IS_EQUAL(qrcol.absDeterminant(), qrcol_passive.absDeterminant());
  VERIFY_IS_EQUAL(qrcol.hCoeffs(), qrcol_passive.hCoeffs());
#endif

  // CompleteOrthogonalDecomposition tests
  CompleteOrthogonalDecomposition<MatrixType> qrcod(A);
  CompleteOrthogonalDecomposition<PassiveMatrixType> qrcod_passive(B);
  VERIFY_IS_EQUAL(qrcod.cols(), qrcod_passive.cols());
  VERIFY_IS_EQUAL(qrcod.colsPermutation().indices(), qrcod_passive.colsPermutation().indices());
  VERIFY_IS_EQUAL(qrcod.dimensionOfKernel(), qrcod_passive.dimensionOfKernel());
  VERIFY_IS_EQUAL(qrcod.info(), qrcod_passive.info());
  VERIFY_IS_EQUAL(qrcod.isInjective(), qrcod_passive.isInjective());
  VERIFY_IS_EQUAL(qrcod.isInvertible(), qrcod_passive.isInvertible());
  VERIFY_IS_EQUAL(qrcod.isSurjective(), qrcod_passive.isSurjective());
  VERIFY_IS_EQUAL(qrcod.logAbsDeterminant(), qrcod_passive.logAbsDeterminant());
  VERIFY_IS_EQUAL(qrcod.maxPivot(), qrcod_passive.maxPivot());
  VERIFY_IS_EQUAL(qrcod.nonzeroPivots(), qrcod_passive.nonzeroPivots());
  VERIFY_IS_EQUAL(qrcod.rank(), qrcod_passive.rank());
  VERIFY_IS_EQUAL(qrcod.rows(), qrcod_passive.rows());
  VERIFY_IS_EQUAL(qrcod.threshold(), qrcod_passive.threshold());

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_IS_EQUAL(qrcod.householderQ(), qrcod_passive.householderQ());
  VERIFY_IS_EQUAL(qrcod.matrixQTZ(), qrcod_passive.matrixQTZ());
  VERIFY_IS_EQUAL(qrcod.matrixT(), qrcod_passive.matrixT());
  VERIFY_IS_EQUAL(qrcod.matrixZ(), qrcod_passive.matrixZ());
  VERIFY_IS_EQUAL(qrcod.absDeterminant(), qrcod_passive.absDeterminant());
  VERIFY_IS_EQUAL(qrcod.hCoeffs(), qrcod_passive.hCoeffs());
  VERIFY_IS_EQUAL(qrcod.zCoeffs(), qrcod_passive.zCoeffs());
#endif

  // Remove tape
  DCO_M::tape_t::remove(DCO_M::global_tape);
}

void test_eigen_ad_test_dco_qr()
{
  CALL_SUBTEST_1(( symbolic_solvers_test_1st_order<HouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_2(( symbolic_solvers_test_1st_order<HouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_3(( symbolic_solvers_test_2nd_order<HouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_4(( symbolic_solvers_test_2nd_order<HouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_5(( symbolic_solvers_test_1st_order<FullPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_6(( symbolic_solvers_test_1st_order<FullPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_7(( symbolic_solvers_test_2nd_order<FullPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_8(( symbolic_solvers_test_2nd_order<FullPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_9(( symbolic_solvers_test_1st_order<ColPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_10(( symbolic_solvers_test_1st_order<ColPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_11(( symbolic_solvers_test_2nd_order<ColPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_12(( symbolic_solvers_test_2nd_order<ColPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_13(( symbolic_solvers_test_1st_order<CompleteOrthogonalDecomposition<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_14(( symbolic_solvers_test_1st_order<CompleteOrthogonalDecomposition<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_15(( symbolic_solvers_test_2nd_order<CompleteOrthogonalDecomposition<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_16(( symbolic_solvers_test_2nd_order<CompleteOrthogonalDecomposition<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_17(( qr_verify_assert<Matrix3dco_a1d>() ));
  CALL_SUBTEST_18(( qr_verify_wrapping<Matrix3dco_a1d>() ));

  // Symbolic decomposition tests
  //CALL_SUBTEST_101(( symbolic_decomp_test_1st_order<HouseholderQR<Matrix<double, 3, 3> > >() ));
  //CALL_SUBTEST_102(( symbolic_decomp_test_1st_order<HouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  //CALL_SUBTEST_103(( symbolic_decomp_test_2nd_order<HouseholderQR<Matrix<double, 3, 3> > >() ));
  //CALL_SUBTEST_104(( symbolic_decomp_test_2nd_order<HouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_105(( symbolic_decomp_test_1st_order<FullPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_106(( symbolic_decomp_test_1st_order<FullPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_107(( symbolic_decomp_test_2nd_order<FullPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_108(( symbolic_decomp_test_2nd_order<FullPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_109(( symbolic_decomp_test_1st_order<ColPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_110(( symbolic_decomp_test_1st_order<ColPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_111(( symbolic_decomp_test_2nd_order<ColPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_112(( symbolic_decomp_test_2nd_order<ColPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_113(( symbolic_decomp_test_1st_order<CompleteOrthogonalDecomposition<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_114(( symbolic_decomp_test_1st_order<CompleteOrthogonalDecomposition<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_115(( symbolic_decomp_test_2nd_order<CompleteOrthogonalDecomposition<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_116(( symbolic_decomp_test_2nd_order<CompleteOrthogonalDecomposition<Matrix<double, Dynamic, Dynamic> > >() ));

  // Symbolic inverse tests
  CALL_SUBTEST_205(( symbolic_inverse_test_1st_order<FullPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_206(( symbolic_inverse_test_1st_order<FullPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_207(( symbolic_inverse_test_2nd_order<FullPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_208(( symbolic_inverse_test_2nd_order<FullPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_209(( symbolic_inverse_test_1st_order<ColPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_210(( symbolic_inverse_test_1st_order<ColPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_211(( symbolic_inverse_test_2nd_order<ColPivHouseholderQR<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_212(( symbolic_inverse_test_2nd_order<ColPivHouseholderQR<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_213(( symbolic_inverse_test_1st_order<CompleteOrthogonalDecomposition<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_214(( symbolic_inverse_test_1st_order<CompleteOrthogonalDecomposition<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_215(( symbolic_inverse_test_2nd_order<CompleteOrthogonalDecomposition<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_216(( symbolic_inverse_test_2nd_order<CompleteOrthogonalDecomposition<Matrix<double, Dynamic, Dynamic> > >() ));

}
