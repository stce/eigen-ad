// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_TEST_DCO_INVERSE_H
#define EIGEN_AD_TEST_DCO_INVERSE_H

#include "Eigen/LU"

#undef EIGEN_TEST_MAX_SIZE
#define EIGEN_TEST_MAX_SIZE 50


// Hard coded 3x3 problem
template<typename Scalar> void createSystem_inverse(Matrix<Scalar, 3, 3>& A)
{
    A << 1, 2, 4, 2, 3, 8, -1, -3, -1;
}

// Dynamic matrix size and dynamic problem generation
template<typename Scalar> void createSystem_inverse(Matrix<Scalar, Dynamic, Dynamic>& A)
{
    int size = internal::random<int>(1,EIGEN_TEST_MAX_SIZE);

    A = Matrix<Scalar, Dynamic, Dynamic>::Random(size, size);
}

template <typename MatrixType> void setupSystem_inverse(MatrixType& A) {
    createSystem_inverse(A);

    FullPivLU<MatrixType> lu(A);
    lu.setThreshold(0.01);
    while(!lu.isInvertible()) {
      createSystem_inverse(A);
    }
}

// Actual solver code to be tested
template<typename Solver, typename MatrixType>
unsigned int inverse(Solver& decomp, const MatrixType& A, MatrixType& C)
{
    Eigen_AD::dco_eigen::get_inverse(C, decomp);

    // Reuse object
    // compute with matrix
    decomp.compute((A*A.adjoint()).eval());
    MatrixType tmp(C.rows(), C.cols());
    Eigen_AD::dco_eigen::get_inverse(tmp, decomp);
    C += tmp;

    return 3;
}

template<typename Solver, typename MatrixType, typename... FArgs>
unsigned int inverse(const MatrixType& A, MatrixType& C, FArgs... fargs)
{
    unsigned int callback_amount = 0;

    Solver decomp(A, fargs...);
    callback_amount = inverse(decomp, A, C);

    return callbackAmount<MatrixType>::eval(callback_amount+1);
}

template<typename MatrixTypePassive, typename Solver> struct inverse_helper;

template<typename MatrixTypePassive, template<typename, typename> class Solver> struct inverse_helper<MatrixTypePassive, Solver<MatrixTypePassive, void> > {
  template<typename MatrixType, typename ... FArgs>
  static unsigned int eval(const MatrixType& A, MatrixType& C, FArgs... fargs) {
    return inverse<Solver<MatrixType, void> >(A, C, fargs...);
  }
};

template<typename MatrixTypePassive, template<typename, int, typename> class Solver, int Arg> struct inverse_helper<MatrixTypePassive, Solver<MatrixTypePassive, Arg, void> > {
  template<typename MatrixType, typename ... FArgs>
  static unsigned int eval(const MatrixType& A, MatrixType& C, FArgs... fargs) {
    return inverse<Solver<MatrixType, Arg, void> >(A, C, fargs...);
  }
};

template<typename Solver, typename MatrixType, typename MatrixTypePassive, typename... FArgs>
void active_inverse_t1(const MatrixTypePassive& _A, MatrixTypePassive&,
                     MatrixTypePassive& _C, MatrixTypePassive& _C_t1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols()), C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
            dco::derivative(A(i,j)) = 1.0;
        }
    }

    // Inverse
    inverse_helper<MatrixTypePassive, Solver>::eval(A, C, fargs...);

    // Save output
    for(int i=0; i<C.rows(); i++) {
        for(int j=0; j<C.cols(); j++) {
            _C(i,j)    = dco::value(C(i,j));
            _C_t1(i,j) = dco::derivative(C(i,j));
        }
    }
}

template<typename Solver, typename MatrixType, typename MatrixTypePassive, typename... FArgs>
void active_inverse_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1,
                     MatrixTypePassive& _C, MatrixTypePassive&, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols()), C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
        }
    }

    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
        }
    }

    // Inverse
    unsigned int callback_amount = inverse_helper<MatrixTypePassive, Solver>::eval(A, C, fargs...);

    // Seed
    for(int i=0; i<C.rows(); i++) {
        for(int j=0; j<C.rows(); j++) {
            DCO_M::global_tape->register_output_variable(C(i,j));
            dco::derivative(C(i,j)) = 1.0;
        }
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Save output
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i,j) = dco::derivative(A(i,j));
            _C(i,j)    = dco::value(C(i,j));
        }
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Solver, typename MatrixType, typename MatrixTypePassive, typename... FArgs>
void active_inverse_t2_t1(const MatrixTypePassive& _A,
                          MatrixTypePassive& _C, MatrixTypePassive& _C_t1, MatrixTypePassive& _C_t2, MatrixTypePassive& _C_t2_t1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols()), C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
            dco::derivative(dco::value(A(i,j))) = 1.0;
            dco::value(dco::derivative(A(i,j))) = 1.0;
            dco::derivative(dco::derivative(A(i,j))) = 1.0;
        }
    }

    // Inverse
    inverse_helper<MatrixTypePassive, Solver>::eval(A, C, fargs...);

    // Save output
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _C(i,j)    = dco::value(dco::value(C(i,j)));
            _C_t1(i,j) = dco::derivative(dco::value(C(i,j)));
            _C_t2(i,j) = dco::value(dco::derivative(C(i,j)));
            _C_t2_t1(i,j) = dco::derivative(dco::derivative(C(i,j)));
        }
    }
}

template<typename Solver, typename MatrixType, typename MatrixTypePassive, typename... FArgs>
void active_inverse_t2_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1, MatrixTypePassive& _A_t2_a1,
                          MatrixTypePassive& _C, MatrixTypePassive& _C_t2, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols()), C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
        }
    }

    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
            dco::derivative(dco::value(A(i,j))) = 1.0;
        }
    }

    // Inverse
    unsigned int callback_amount = inverse_helper<MatrixTypePassive, Solver>::eval(A, C, fargs...);

    // Seed
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_output_variable(C(i,j));
            dco::derivative(C(i,j)) = 1.0;
            dco::derivative(dco::derivative(C(i,j))) = 1.0;
        }
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i, j) = dco::value(dco::derivative(A(i,j)));
            _A_t2_a1(i, j) = dco::derivative(dco::derivative(A(i,j)));
            _C(i, j) = dco::value(dco::value(C(i,j)));
            _C_t2(i, j) = dco::derivative(dco::value(C(i,j)));
        }
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Solver, typename MatrixType, typename MatrixTypePassive, typename... FArgs>
void active_inverse_a2_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1, MatrixTypePassive& _A_a2,
                          MatrixTypePassive& _C, MatrixTypePassive& _C_a2_a1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols()), C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
        }
    }

    // Create tapes
    typedef dco::mode<typename MatrixType::Scalar::VALUE_TYPE> DCO_BASE_M;
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;

    DCO_BASE_M::global_tape = DCO_BASE_M::tape_t::create();
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_BASE_M::global_tape->register_variable(dco::value(A(i,j)));
            DCO_M::global_tape->register_variable(A(i,j));
        }
    }

    // Inverse
    unsigned int callback_amount = inverse_helper<MatrixTypePassive, Solver>::eval(A, C, fargs...);

    // Seed
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::derivative(C(i,j)) = 1.0;
            DCO_BASE_M::global_tape->register_variable(dco::derivative(C(i,j)));
        }
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Seed and save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i, j) = dco::value(dco::derivative(A(i,j)));
            dco::derivative(dco::derivative(A(i,j))) = 1.0;
            dco::derivative(dco::value(C(i,j))) = 1.0;
        }
    }

    // Calculate derivatives
    DCO_BASE_M::global_tape->interpret_adjoint();

    // Save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a2(i, j) = dco::derivative(dco::value(A(i,j)));
            _C(i, j) = dco::value(dco::value(C(i,j)));
            _C_a2_a1(i, j) = dco::derivative(dco::derivative(C(i,j)));
        }
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tapes
    DCO_BASE_M::tape_t::remove(DCO_BASE_M::global_tape);
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Solver, int Requirements = None, typename ... FArgs> void symbolic_inverse_test_1st_order(FArgs... fargs)
{
    typedef typename Solver::Scalar Scalar;
    static const unsigned int Rows = Solver::MaxRowsAtCompileTime;
    static const unsigned int Cols = Solver::MaxColsAtCompileTime;

    typedef Matrix<Scalar, Rows, Cols> MatrixType;
    typedef Matrix<typename dco::gt1s<Scalar>::type, Rows, Cols> MatrixTypeTangent;
    typedef Matrix<typename dco::ga1s<Scalar>::type, Rows, Cols> MatrixTypeSymbolic;

    MatrixType A;

    setupSystem_inverse(A);

    MatrixType A_tan(A.rows(), A.cols()), A_tan_t1(A.rows(), A.cols()), A_sym(A.rows(), A.cols()), A_sym_a1(A.rows(), A.cols());
    MatrixType C(A.rows(), A.cols()), C_tan(A.rows(), A.cols()), C_tan_t1(A.rows(), A.cols()), C_sym(A.rows(), A.cols()), C_sym_a1(A.rows(), A.cols());

    A_sym = A;
    A_tan = A;

    inverse          <Solver, MatrixType>        (A, C, fargs...);
    active_inverse_t1<Solver, MatrixTypeTangent> (A_tan, A_tan_t1, C_tan, C_tan_t1, fargs...);
    active_inverse_a1<Solver, MatrixTypeSymbolic>(A_sym, A_sym_a1, C_sym, C_sym_a1, fargs...);

    // Check results
    VERIFY_IS_APPROX(C_sym, C);
    VERIFY_IS_APPROX(C_tan, C);
    VERIFY_IS_APPROX(C_tan_t1.sum(), A_sym_a1.sum());

    // Verify against plain AAD
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    typedef Eigen_AD::internal::Matrix_AD_Wrapper<Matrix<typename dco::ga1s<Scalar>::type, Rows, Cols> > MatrixTypeAdjoint;

    MatrixType A_adj(A.rows(), A.cols()), A_adj_a1(A.rows(), A.cols());
    MatrixType C_adj(A.rows(), A.cols()), C_adj_a1(A.rows(), A.cols());

    A_adj = A;

    active_inverse_a1<Solver, MatrixTypeAdjoint>(A_adj, A_adj_a1, C_adj, C_adj_a1, fargs...);

    VERIFY_IS_APPROX(C_sym, C_adj);
    VERIFY_IS_APPROX(A_sym_a1, A_adj_a1);
#endif
}


template<typename Solver, int Requirements = None, typename ... FArgs> void symbolic_inverse_test_2nd_order(FArgs... fargs)
{
    typedef typename Solver::Scalar Scalar;
    static const unsigned int Rows = Solver::MaxRowsAtCompileTime;
    static const unsigned int Cols = Solver::MaxColsAtCompileTime;

    typedef Matrix<Scalar, Rows, Cols> MatrixType;
    typedef Matrix<typename dco::gt1s<typename dco::gt1s<Scalar>::type>::type, Rows, Cols> MatrixTypeTangentTangent;
    typedef Matrix<typename dco::ga1s<typename dco::gt1s<Scalar>::type>::type, Rows, Cols> MatrixTypeTangentSymbolic;
    typedef Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, Cols> MatrixTypeSymbolicSymbolic;

    MatrixType A;

    setupSystem_inverse(A);

    MatrixType A_tantan(A.rows(), A.cols()), A_tansym(A.rows(), A.cols()), A_tansym_a1(A.rows(), A.cols()), A_tansym_t2_a1(A.rows(), A.cols()), A_symsym(A.rows(), A.cols()), A_symsym_a1(A.rows(), A.cols()), A_symsym_a2(A.rows(), A.cols());
    MatrixType C(A.rows(), A.cols()), C_tantan(A.rows(), A.cols()), C_tantan_t1(A.rows(), A.cols()), C_tantan_t2(A.rows(), A.cols()), C_tantan_t2_t1(A.rows(), A.cols()), C_tansym(A.rows(), A.cols()), C_tansym_t2(A.rows(), A.cols()), C_symsym(A.rows(), A.cols()), C_symsym_a2_a1(A.rows(), A.cols());

    A_tantan = A;
    A_tansym = A;
    A_symsym = A;

    inverse             <Solver, MatrixType>                (A, C, fargs...);
    active_inverse_t2_t1<Solver, MatrixTypeTangentTangent>  (A_tantan, C_tantan, C_tantan_t1, C_tantan_t2, C_tantan_t2_t1, fargs...);
    active_inverse_t2_a1<Solver, MatrixTypeTangentSymbolic> (A_tansym, A_tansym_a1, A_tansym_t2_a1, C_tansym, C_tansym_t2, fargs...);
    active_inverse_a2_a1<Solver, MatrixTypeSymbolicSymbolic>(A_symsym, A_symsym_a1, A_symsym_a2, C_symsym, C_symsym_a2_a1, fargs...);

    // Check primal
    VERIFY_IS_APPROX(C_tantan, C);
    VERIFY_IS_APPROX(C_tansym, C);
    VERIFY_IS_APPROX(C_symsym, C);

    // Check ToA 1st orders
    VERIFY_IS_APPROX(C_tantan_t1, C_tantan_t2);
    VERIFY_IS_APPROX(C_tansym_t2, C_tantan_t2);
    VERIFY_IS_APPROX(A_tansym_a1.sum(), C_tantan_t2.sum());

    // Check AoA 1st orders
    VERIFY_IS_APPROX(A_symsym_a1, A_tansym_a1);
    VERIFY_IS_APPROX(C_symsym_a2_a1, C_tansym_t2);

    // Check ToA 2nd orders
    VERIFY_IS_APPROX(A_tansym_t2_a1.sum(), C_tantan_t2_t1.sum());

    // Check AoA 2nd orders
    VERIFY_IS_APPROX(A_symsym_a2.sum(), C_tantan_t2_t1.sum());

    // Verify against plain AAD
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    typedef Eigen_AD::internal::Matrix_AD_Wrapper<Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, Cols> > MatrixTypeAdjointAdjoint;

    MatrixType A_adjadj(A.rows(), A.cols()), A_adjadj_a1(A.rows(), A.cols()), A_adjadj_a2(A.rows(), A.cols());
    MatrixType C_adjadj(A.rows(), A.cols()), C_adjadj_a2_a1(A.rows(), A.cols());

    A_adjadj = A;

    active_inverse_a2_a1<Solver, MatrixTypeAdjointAdjoint>(A_adjadj, A_adjadj_a1, A_adjadj_a2, C_adjadj, C_adjadj_a2_a1, fargs...);

    VERIFY_IS_APPROX(C_symsym, C_adjadj);
    VERIFY_IS_APPROX(A_symsym_a1, A_adjadj_a1);
    VERIFY_IS_APPROX(A_symsym_a2, A_adjadj_a2);
    VERIFY_IS_APPROX(C_symsym_a2_a1, C_adjadj_a2_a1);
#endif
}

#endif // EIGEN_AD_TEST_DCO_INVERSE_H
