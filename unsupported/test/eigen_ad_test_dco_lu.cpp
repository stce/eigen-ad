// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "eigen_ad_test_dco_solvers.h"
#include "eigen_ad_test_dco_inverse.h"
#include <Eigen/LU>

template<typename MatrixType> void lu_verify_assert()
{
  MatrixType tmp;

  FullPivLU<MatrixType> lu;
  VERIFY_RAISES_ASSERT(lu.permutationP())
  VERIFY_RAISES_ASSERT(lu.permutationQ())
  VERIFY_RAISES_ASSERT(lu.solve(tmp))
  VERIFY_RAISES_ASSERT(lu.rank())
  VERIFY_RAISES_ASSERT(lu.dimensionOfKernel())
  VERIFY_RAISES_ASSERT(lu.isInjective())
  VERIFY_RAISES_ASSERT(lu.isSurjective())
  VERIFY_RAISES_ASSERT(lu.isInvertible())
  VERIFY_RAISES_ASSERT(lu.inverse())

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_RAISES_ASSERT(lu.matrixLU())
  VERIFY_RAISES_ASSERT(lu.determinant())
  VERIFY_RAISES_ASSERT(lu.kernel())
  VERIFY_RAISES_ASSERT(lu.image(tmp))
#endif

  PartialPivLU<MatrixType> plu;
  VERIFY_RAISES_ASSERT(plu.permutationP())
  VERIFY_RAISES_ASSERT(plu.solve(tmp))
  VERIFY_RAISES_ASSERT(plu.inverse())

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_RAISES_ASSERT(plu.matrixLU())
  VERIFY_RAISES_ASSERT(plu.determinant())
#endif
}

template<typename MatrixType> void lu_verify_wrapping()
{
  // Create tape
  typedef dco::mode<typename MatrixType::Scalar> DCO_M;
  DCO_M::global_tape = DCO_M::tape_t::create();

  // Passive Matrix
  typedef Matrix<typename DCO_M::passive_t, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> PassiveMatrixType;
  PassiveMatrixType B = PassiveMatrixType::Random();
  MatrixType A = B;

  // FullPivLU tests
  FullPivLU<MatrixType> lu(A);
  FullPivLU<PassiveMatrixType> lu_passive(B);
  VERIFY_IS_EQUAL(lu.nonzeroPivots(), lu_passive.nonzeroPivots());
  VERIFY_IS_EQUAL(lu.maxPivot(), lu_passive.maxPivot());
  VERIFY_IS_EQUAL(lu.permutationP().indices(), lu_passive.permutationP().indices());
  VERIFY_IS_EQUAL(lu.permutationQ().indices(), lu_passive.permutationQ().indices());
  VERIFY_IS_EQUAL(lu.rcond(), lu_passive.rcond());
  VERIFY_IS_EQUAL(lu.threshold(), lu_passive.threshold());
  VERIFY_IS_EQUAL(lu.rank(), lu_passive.rank());
  VERIFY_IS_EQUAL(lu.dimensionOfKernel(), lu_passive.dimensionOfKernel());
  VERIFY_IS_EQUAL(lu.isInjective(), lu_passive.isInjective());
  VERIFY_IS_EQUAL(lu.isSurjective(), lu_passive.isSurjective());
  VERIFY_IS_EQUAL(lu.isInvertible(), lu_passive.isInvertible());
  VERIFY_IS_EQUAL(lu.rows(), lu_passive.rows());
  VERIFY_IS_EQUAL(lu.cols(), lu_passive.cols());
  MatrixType lu_inv = lu.inverse();
  PassiveMatrixType lu_passive_inv = lu_passive.inverse();
  for(unsigned int i=0; i<lu_inv.rows(); i++)
    for(unsigned int j=0; j<lu_inv.cols(); j++)
      VERIFY_IS_EQUAL(dco::value(lu_inv(i,j)), lu_passive_inv(i,j));

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_IS_EQUAL(lu.matrixLU(), lu_passive.matrixLU());
  VERIFY_IS_EQUAL(lu.determinant(), lu_passive.determinant());
  VERIFY_IS_EQUAL(lu.kernel(), lu_passive.kernel());
  VERIFY_IS_EQUAL(lu.image(A), lu_passive.image(B));
#endif

  // PartPivLU tests
  PartialPivLU<MatrixType> plu(A);
  PartialPivLU<PassiveMatrixType> plu_passive(B);
  VERIFY_IS_EQUAL(plu.permutationP().indices(), plu_passive.permutationP().indices());
  VERIFY_IS_EQUAL(plu.rcond(), plu_passive.rcond());
  VERIFY_IS_EQUAL(plu.rows(), plu_passive.rows());
  VERIFY_IS_EQUAL(plu.cols(), plu_passive.cols());
  MatrixType plu_inv = plu.inverse();
  PassiveMatrixType plu_passive_inv = plu_passive.inverse();
  for(unsigned int i=0; i<plu_inv.rows(); i++)
    for(unsigned int j=0; j<plu_inv.cols(); j++)
      VERIFY_IS_EQUAL(dco::value(plu_inv(i,j)), plu_passive_inv(i,j));

#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  VERIFY_IS_EQUAL(plu.matrixLU(), plu_passive.matrixLU());
  VERIFY_IS_EQUAL(plu.determinant(), plu_passive.determinant());
#endif

  // Remove tape
  DCO_M::tape_t::remove(DCO_M::global_tape);
}

void test_eigen_ad_test_dco_lu()
{
  CALL_SUBTEST_1(( symbolic_solvers_test_1st_order<FullPivLU<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_2(( symbolic_solvers_test_1st_order<FullPivLU<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_3(( symbolic_solvers_test_2nd_order<FullPivLU<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_4(( symbolic_solvers_test_2nd_order<FullPivLU<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_5(( symbolic_solvers_test_1st_order<PartialPivLU<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_6(( symbolic_solvers_test_1st_order<PartialPivLU<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_7(( symbolic_solvers_test_2nd_order<PartialPivLU<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_8(( symbolic_solvers_test_2nd_order<PartialPivLU<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_9(( lu_verify_assert<Matrix3dco_a1d>() ));
  CALL_SUBTEST_10(( lu_verify_wrapping<Matrix3dco_a1d>() ));

  // Symbolic inverse tests
  CALL_SUBTEST_201(( symbolic_inverse_test_1st_order<FullPivLU<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_202(( symbolic_inverse_test_1st_order<FullPivLU<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_203(( symbolic_inverse_test_2nd_order<FullPivLU<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_204(( symbolic_inverse_test_2nd_order<FullPivLU<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_205(( symbolic_inverse_test_1st_order<PartialPivLU<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_206(( symbolic_inverse_test_1st_order<PartialPivLU<Matrix<double, Dynamic, Dynamic> > >() ));
  CALL_SUBTEST_207(( symbolic_inverse_test_2nd_order<PartialPivLU<Matrix<double, 3, 3> > >() ));
  CALL_SUBTEST_208(( symbolic_inverse_test_2nd_order<PartialPivLU<Matrix<double, Dynamic, Dynamic> > >() ));
}
