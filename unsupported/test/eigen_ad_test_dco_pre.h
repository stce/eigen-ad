// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_TEST_DCO_PRE_H
#define EIGEN_AD_TEST_DCO_PRE_H

#ifndef DCO_AUTO_SUPPORT
#define DCO_AUTO_SUPPORT
#endif
#ifndef DCO_STD_COMPATIBILITY
#define DCO_STD_COMPATIBILITY
#endif

#include "dco.hpp"

// Wrapper to return the inner type if we have a complex type
template<typename T> struct get_inner_type_base { typedef T type; enum { value = false }; };
template<typename T> struct get_inner_type_base<std::complex<T> > { typedef T type; enum { value = true }; };
template<typename T> struct get_inner_type { typedef typename get_inner_type_base<T>::type type;
                                             enum { value = get_inner_type_base<T>::value}; };

namespace Eigen {
  template<class T, class U, typename enable_if_dco_type = typename std::enable_if<(dco::mode<typename get_inner_type<T>::type>::is_dco_type && dco::mode<typename get_inner_type<U>::type>::is_dco_type)>::type>
  inline bool test_isApprox(const T& a, const U& b);
}


#endif // EIGEN_AD_TEST_DCO_PRE_H
