// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_TEST_DCO_DECOMPOSITIONS_H
#define EIGEN_AD_TEST_DCO_DECOMPOSITIONS_H

#include "Eigen/LU"

#undef EIGEN_TEST_MAX_SIZE
#define EIGEN_TEST_MAX_SIZE 50

// Hard coded 3x3 problem
template<typename Scalar> void createSystem_decomp(Matrix<Scalar, 3, 3>& A)
{
    A << 1, 2, 4, 2, 3, 8, -1, -3, -1;
}

// Dynamic matrix size and dynamic problem generation
template<typename Scalar> void createSystem_decomp(Matrix<Scalar, Dynamic, Dynamic>& A)
{
    int size = internal::random<int>(1,EIGEN_TEST_MAX_SIZE);

    A = Matrix<Scalar, Dynamic, Dynamic>::Random(size, size);
}

template <int Requirements, typename MatrixType> void setupSystem_decomp(MatrixType& A) {
    createSystem_decomp(A);

    if(Requirements == Invertible) {
        FullPivLU<MatrixType> lu(A);
        lu.setThreshold(0.01);
        while(!lu.isInvertible()) {
          createSystem_decomp(A);
        }
    } else if(Requirements == PosDefinite) {
        MatrixType A1 = A * A.adjoint();
        A = A1;

        // let's make sure the matrix is not singular or near singular
        for (int k=0; k<3; ++k)
        {
          MatrixType A2 = MatrixType::Random(A.rows(),A.cols());
          A1 += A2 * A2.adjoint();
        }
        A = A1;
    }
}

// Actual solver code to be tested
template<typename Solver, typename MatrixType, typename Scalar>
unsigned int logAbsDeterminant(Solver& decomp, const MatrixType& A, Scalar& x)
{
    x = decomp.logAbsDeterminant();

    // Reuse object
    // compute with matrix
    decomp.compute((A*A.adjoint()).eval());
    x += decomp.logAbsDeterminant();

    return 3;
}

template<typename Solver, typename MatrixType, typename Scalar, typename... FArgs>
unsigned int decomp(const MatrixType& A, Scalar& x, FArgs... fargs)
{
    unsigned int callback_amount = 0;

    Solver decomp(A, fargs...);
    callback_amount = logAbsDeterminant(decomp, A, x);

    return callbackAmount<MatrixType>::eval(callback_amount+1);
}

template<typename MatrixTypePassive, typename Solver> struct decomp_helper;

template<typename MatrixTypePassive, template<typename, typename> class Solver> struct decomp_helper<MatrixTypePassive, Solver<MatrixTypePassive, void> > {
  template<typename MatrixType, typename Scalar, typename ... FArgs>
  static unsigned int eval(const MatrixType& A, Scalar& x, FArgs... fargs) {
    return decomp<Solver<MatrixType, void> >(A, x, fargs...);
  }
};

template<typename MatrixTypePassive, template<typename, int, typename> class Solver, int Arg> struct decomp_helper<MatrixTypePassive, Solver<MatrixTypePassive, Arg, void> > {
  template<typename MatrixType, typename Scalar, typename ... FArgs>
  static unsigned int eval(const MatrixType& A, Scalar& x, FArgs... fargs) {
    return decomp<Solver<MatrixType, Arg, void> >(A, x, fargs...);
  }
};

template<typename Solver, typename MatrixType, typename Scalar, typename MatrixTypePassive, typename ScalarPassive, typename... FArgs>
void active_decomp_t1(const MatrixTypePassive& _A, MatrixTypePassive&,
                     ScalarPassive& _x, ScalarPassive& _x_t1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
            dco::derivative(A(i,j)) = 1.0;
        }
    }
    Scalar x = _x;

    // decomp
    decomp_helper<MatrixTypePassive, Solver>::eval(A, x, fargs...);

    // Save output
    _x    = dco::value(x);
    _x_t1 = dco::derivative(x);
}

template<typename Solver, typename MatrixType, typename Scalar, typename MatrixTypePassive, typename ScalarPassive, typename... FArgs>
void active_decomp_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1,
                     ScalarPassive& _x, ScalarPassive&, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
        }
    }
    Scalar x = _x;

    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
        }
    }

    // decomp
    unsigned int callback_amount = decomp_helper<MatrixTypePassive, Solver>::eval(A, x, fargs...);

    // Seed
    DCO_M::global_tape->register_output_variable(x);
    dco::derivative(x) = 1.0;

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Save output
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i,j) = dco::derivative(A(i,j));
        }
    }
    _x = dco::value(x);

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}


template<typename Solver, typename MatrixType, typename Scalar, typename MatrixTypePassive, typename ScalarPassive, typename... FArgs>
void active_decomp_t2_t1(const MatrixTypePassive& _A,
                         ScalarPassive& _x, ScalarPassive& _x_t1, ScalarPassive& _x_t2, ScalarPassive& _x_t2_t1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
            dco::derivative(dco::value(A(i,j))) = 1.0;
            dco::value(dco::derivative(A(i,j))) = 1.0;
            dco::derivative(dco::derivative(A(i,j))) = 1.0;
        }
    }
    Scalar x = _x;

    // decomp
    decomp_helper<MatrixTypePassive, Solver>::eval(A, x, fargs...);

    // Save output
    _x    = dco::value(dco::value(x));
    _x_t1 = dco::derivative(dco::value(x));
    _x_t2 = dco::value(dco::derivative(x));
    _x_t2_t1 = dco::derivative(dco::derivative(x));
}

template<typename Solver, typename MatrixType, typename Scalar, typename MatrixTypePassive, typename ScalarPassive, typename... FArgs>
void active_decomp_t2_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1, MatrixTypePassive& _A_t2_a1,
                         ScalarPassive& _x, ScalarPassive& _x_t2, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
        }
    }
    Scalar x = _x;

    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
            dco::derivative(dco::value(A(i,j))) = 1.0;
        }
    }

    // decomp
    unsigned int callback_amount = decomp_helper<MatrixTypePassive, Solver>::eval(A, x, fargs...);

    // Seed
    DCO_M::global_tape->register_output_variable(x);
    dco::derivative(x) = 1.0;
    dco::derivative(dco::derivative(x)) = 1.0;

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i, j) = dco::value(dco::derivative(A(i,j)));
            _A_t2_a1(i, j) = dco::derivative(dco::derivative(A(i,j)));
        }
    }
    _x = dco::value(dco::value(x));
    _x_t2 = dco::derivative(dco::value(x));

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Solver, typename MatrixType, typename Scalar, typename MatrixTypePassive, typename ScalarPassive, typename... FArgs>
void active_decomp_a2_a1(const MatrixTypePassive& _A, MatrixTypePassive& _A_a1, MatrixTypePassive& _A_a2,
                         ScalarPassive& _x, ScalarPassive& _x_a2_a1, FArgs... fargs)
{
    // Declare and init active types
    MatrixType A(_A.rows(), _A.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
        }
    }
    Scalar x = _x;

    // Create tapes
    typedef dco::mode<typename MatrixType::Scalar::VALUE_TYPE> DCO_BASE_M;
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;

    DCO_BASE_M::global_tape = DCO_BASE_M::tape_t::create();
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_BASE_M::global_tape->register_variable(dco::value(A(i,j)));
            DCO_M::global_tape->register_variable(A(i,j));
        }
    }

    // decomp
    unsigned int callback_amount = decomp_helper<MatrixTypePassive, Solver>::eval(A, x, fargs...);

    // Seed
    dco::derivative(x) = 1.0;
    DCO_BASE_M::global_tape->register_variable(dco::derivative(x));

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Seed and save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i, j) = dco::value(dco::derivative(A(i,j)));
            dco::derivative(dco::derivative(A(i,j))) = 1.0;
        }
    }
    dco::derivative(dco::value(x)) = 1.0;

    // Calculate derivatives
    DCO_BASE_M::global_tape->interpret_adjoint();

    // Save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a2(i, j) = dco::derivative(dco::value(A(i,j)));
        }
    }
    _x = dco::value(dco::value(x));
    _x_a2_a1 = dco::derivative(dco::derivative(x));

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tapes
    DCO_BASE_M::tape_t::remove(DCO_BASE_M::global_tape);
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Solver, int Requirements = None, typename ... FArgs> void symbolic_decomp_test_1st_order(FArgs... fargs)
{
    typedef typename Solver::Scalar Scalar;
    static const unsigned int Rows = Solver::MaxRowsAtCompileTime;
    static const unsigned int Cols = Solver::MaxColsAtCompileTime;

    typedef Matrix<Scalar, Rows, Cols> MatrixType;
    typedef typename dco::gt1s<Scalar>::type ScalarTangent;
    typedef Matrix<ScalarTangent, Rows, Cols> MatrixTypeTangent;
    typedef typename dco::ga1s<Scalar>::type ScalarSymbolic;
    typedef Matrix<ScalarSymbolic, Rows, Cols> MatrixTypeSymbolic;

    MatrixType A;

    setupSystem_decomp<Requirements>(A);

    MatrixType A_tan(A.rows(), A.cols()), A_tan_t1(A.rows(), A.cols()), A_sym(A.rows(), A.cols()), A_sym_a1(A.rows(), A.cols());
    Scalar     x, x_tan, x_tan_t1, x_sym, x_sym_a1;

    A_sym = A;
    A_tan = A;

    decomp          <Solver, MatrixType, Scalar>                (A, x, fargs...);
    active_decomp_t1<Solver, MatrixTypeTangent, ScalarTangent>  (A_tan, A_tan_t1, x_tan, x_tan_t1, fargs...);
    active_decomp_a1<Solver, MatrixTypeSymbolic, ScalarSymbolic>(A_sym, A_sym_a1, x_sym, x_sym_a1, fargs...);

    // Check results
    VERIFY_IS_APPROX(x_sym, x);
    VERIFY_IS_APPROX(x_tan, x);
    VERIFY_IS_APPROX(x_tan_t1, A_sym_a1.sum());

    // Verify against plain AAD
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    typedef Eigen_AD::internal::Matrix_AD_Wrapper<Matrix<typename dco::ga1s<Scalar>::type, Rows, Cols> > MatrixTypeAdjoint;

    MatrixType A_adj(A.rows(), A.cols()), A_adj_a1(A.rows(), A.cols());
    Scalar     x_ad, x_adj_a1;

    A_adj = A;

    active_decomp_a1<Solver, MatrixTypeAdjoint,  ScalarSymbolic> (A_adj, A_adj_a1, x_adj, x_adj_a1, fargs...);

    VERIFY_IS_APPROX(x_sym, x_adj);
    VERIFY_IS_APPROX(A_sym_a1, A_adj_a1);
#endif
}

template<typename Solver, int Requirements = None, typename ... FArgs> void symbolic_decomp_test_2nd_order(FArgs... fargs)
{
    typedef typename Solver::Scalar Scalar;
    static const unsigned int Rows = Solver::MaxRowsAtCompileTime;
    static const unsigned int Cols = Solver::MaxColsAtCompileTime;

    typedef Matrix<Scalar, Rows, Cols> MatrixType;
    typedef typename dco::gt1s<typename dco::gt1s<Scalar>::type>::type ScalarTangentTangent;
    typedef Matrix<ScalarTangentTangent, Rows, Cols>                   MatrixTypeTangentTangent;
    typedef typename dco::ga1s<typename dco::gt1s<Scalar>::type>::type ScalarTangentSymbolic;
    typedef Matrix<ScalarTangentSymbolic, Rows, Cols>                  MatrixTypeTangentSymbolic;
    typedef typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type ScalarSymbolicSymbolic;
    typedef Matrix<ScalarSymbolicSymbolic, Rows, Cols>                 MatrixTypeSymbolicSymbolic;

    MatrixType A;

    setupSystem_decomp<Requirements>(A);

    MatrixType A_tantan(A.rows(), A.cols()), A_tansym(A.rows(), A.cols()), A_tansym_a1(A.rows(), A.cols()), A_tansym_t2_a1(A.rows(), A.cols()), A_symsym(A.rows(), A.cols()), A_symsym_a1(A.rows(), A.cols()), A_symsym_a2(A.rows(), A.cols());
    Scalar     x, x_tantan, x_tantan_t1, x_tantan_t2, x_tantan_t2_t1, x_tansym, x_tansym_t2, x_symsym, x_symsym_a2_a1;

    A_tantan = A;
    A_tansym = A;
    A_symsym = A;

    decomp             <Solver, MatrixType, Scalar>                                (A, x, fargs...);
    active_decomp_t2_t1<Solver, MatrixTypeTangentTangent, ScalarTangentTangent>    (A_tantan, x_tantan, x_tantan_t1, x_tantan_t2, x_tantan_t2_t1, fargs...);
    active_decomp_t2_a1<Solver, MatrixTypeTangentSymbolic, ScalarTangentSymbolic>  (A_tansym, A_tansym_a1, A_tansym_t2_a1, x_tansym, x_tansym_t2, fargs...);
    active_decomp_a2_a1<Solver, MatrixTypeSymbolicSymbolic, ScalarSymbolicSymbolic>(A_symsym, A_symsym_a1, A_symsym_a2, x_symsym, x_symsym_a2_a1, fargs...);

    // Check primal
    VERIFY_IS_APPROX(x_tantan, x);
    VERIFY_IS_APPROX(x_tansym, x);
    VERIFY_IS_APPROX(x_symsym, x);

    // Check ToA 1st orders
    VERIFY_IS_APPROX(x_tantan_t1, x_tantan_t2);
    VERIFY_IS_APPROX(x_tansym_t2, x_tantan_t2);
    VERIFY_IS_APPROX(A_tansym_a1.sum(), x_tantan_t2);

    // Check AoA 1st orders
    VERIFY_IS_APPROX(A_symsym_a1, A_tansym_a1);
    VERIFY_IS_APPROX(x_symsym_a2_a1, x_tansym_t2);

    // Check ToA 2nd orders
    VERIFY_IS_APPROX(A_tansym_t2_a1.sum(), x_tantan_t2_t1);

    // Check AoA 2nd orders
    VERIFY_IS_APPROX(A_symsym_a2.sum(), x_tantan_t2_t1);

    // Verify against plain AAD
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
    typedef Eigen_AD::internal::Matrix_AD_Wrapper<Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, Cols> > MatrixTypeAdjointAdjoint;

    MatrixType A_adjadj(A.rows(), A.cols()), A_adjadj_a1(A.rows(), A.cols()), A_adjadj_a2(A.rows(), A.cols());
    Scalar     x_adjadj, x_adjadj_a2_a1;

    A_adjadj = A;

    active_decomp_a2_a1<Solver, MatrixTypeAdjointAdjoint, ScalarSymbolicSymbolic>(A_adjadj, A_adjadj_a1, A_adjadj_a2, x_adjadj, x_adjadj_a2_a1, fargs...);

    VERIFY_IS_APPROX(x_symsym, x_adjadj);
    VERIFY_IS_APPROX(A_symsym_a1, A_adjadj_a1);
    VERIFY_IS_APPROX(A_symsym_a2, A_adjadj_a2);
    VERIFY_IS_APPROX(x_symsym_a2_a1, x_adjadj_a2_a1);
#endif
}
#endif // EIGEN_AD_TEST_DCO_DECOMPOSITIONS_H
