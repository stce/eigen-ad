// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_TEST_DCO_OPERATORS_COMPARE_H
#define EIGEN_AD_TEST_DCO_OPERATORS_COMPARE_H

#include <fstream>

#define Q(x) #x
#define QUOTE(x) Q(x)

#ifndef DCO_EIGEN_H // Include this for the vanilla Eigen tests
namespace Eigen {
    // NumTraits for the dco active type
    template<class DCO_TAPE_REAL, class DATA_HANDLER> struct NumTraits<typename dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER> >
     : NumTraits<typename dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER>::VALUE_TYPE> // permits to get the epsilon, dummy_precision, lowest, highest functions
    {
      typedef dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER> Real;
      typedef dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER> NonInteger;
      typedef dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER> Literal;
      typedef dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER> Nested;
      enum {
        IsComplex = 0,
        IsInteger = 0,
        ReadCost = 1,
        AddCost = 3,
        MulCost = 3,
        IsSigned = 1,
        RequireInitialization = 1
      };
    };
}
#endif

std::ofstream outfile;

// Equal, lower, ..., than the VANILLA version
#define EQ 0
#define LT 1

#ifdef DCO_TEST_CREATE_DOT
  #define CREATE_DOT(x) DCO_M::global_tape->write_to_dot(x)
#else
  #define CREATE_DOT(x)
#endif

typedef dco::ga1s<double>::type dco_a1d;

template <typename T>
void write_comparison_to_file(int compare, T value) {
    if(!outfile.is_open()) {
        outfile.open(std::string(QUOTE(EIGEN_TEST_FUNC)) + "." + QUOTE(EIGEN_AD_TEST_COMPARE) + ".out" );
    }
    if(outfile) {
        outfile << compare << " " << value << std::endl;
    }
}

#endif // EIGEN_AD_TEST_DCO_OPERATORS_COMPARE_H
