// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#define EIGEN_TEST_AD
#include "main.h"
#include "eigen_ad_test_dco_operators_compare.h"

// Binary operators being tested
#define SIMPLE_ADDITION 0
#define ADDITION 1
#define SIMPLE_SUBTRACTION 2
#define SUBTRACTION 3
#define SIMPLE_PRODUCT 4
#define PRODUCT 5
#define SIMPLE_SCHURPRODUCT 6
#define SCHURPRODUCT 7
#define SIMPLE_DIVISION 8
#define DIVISION 9
#define SIMPLE_CWISEDIVISION 10
#define CWISEDIVISION 11
#define SIMPLE_POW 12
#define POW 13
#define SIMPLE_MIN 14
#define MIN 15
#define SIMPLE_MAX 16
#define MAX 17
#define ALL_AA 18
#define ALL_MIXED 19
#define LARGE 20

template<typename MatrixType, typename pMatrixType> void binary_op_matrix_calculation(const MatrixType& A, MatrixType& B, const pMatrixType& M, const int OP)
{
    switch(OP) {
        default:
        case SIMPLE_ADDITION: B += A ; break;
        case ADDITION: B = A + A + A; break;
        case SIMPLE_SUBTRACTION: B -=  A; break;
        case SUBTRACTION: B = A - A - A; break;
        case SIMPLE_PRODUCT: B *= A; break;
        case PRODUCT: B = A * A; break;
        case SIMPLE_SCHURPRODUCT: B = A.cwiseProduct(A); break;
        case SCHURPRODUCT: B = A.cwiseProduct(A.cwiseProduct(A)); break;
        case SIMPLE_CWISEDIVISION: B = A.cwiseQuotient(A); break;
        case CWISEDIVISION: B = A.cwiseQuotient(A.cwiseQuotient(A)); break;
        case SIMPLE_MIN: B = A.cwiseMin(A); break;
        case MIN: B = A.cwiseMin(-A.cwiseMin(A)); break;
        case SIMPLE_MAX: B = A.cwiseMax(A); break;
        case MAX: B = A.cwiseMax(-A.cwiseMax(A)); break;
        case ALL_AA: B = A + A * A - (A + A); break;
        case ALL_MIXED: B = A + M * A - (A + M); break;
        case LARGE: B = A * M * A + (M - A) * A + M; break;
    }
}

template<typename ArrayType, typename pArrayType> void binary_op_array_calculation(const ArrayType& A, ArrayType& B, const pArrayType& M, const int OP)
{
    (void) M;
    switch(OP) {
        default:
        case SIMPLE_ADDITION: B += A ; break;
        case ADDITION: B = A + A + A; break;
        case SIMPLE_SUBTRACTION: B -=  A; break;
        case SUBTRACTION: B = A - A - A; break;
        case SIMPLE_PRODUCT: B *= A; break;
        case PRODUCT: B = A * A; break;
        case SIMPLE_DIVISION: B /= A; break;
        case DIVISION: B = A / A / A; break;
        case SIMPLE_POW: B = A.pow(A); break;
        case POW: B = A.pow(A.pow()); break;
    }
}

template<typename MatrixType> void binary_op_matrix(const MatrixType& m, const int OP, const int TAPE_CMP, const bool no_dot = false)
{
    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Declare and init active Matrices
    MatrixType A = MatrixType::Random(m.rows(), m.cols());
    MatrixType B = MatrixType::Ones();

    // Declare and init passive Matrices
    typedef Matrix<typename MatrixType::Scalar, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> pMatrixType;
    pMatrixType pA, pB;
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            pA(i, j) = dco::value(A(i, j));
            pB(i, j) = dco::value(B(i, j));
        }
    }

    // Register active Matrices on tape
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i, j));
        }
    }

    // Do the calculation
    binary_op_matrix_calculation(A, B, pA, OP);
    binary_op_matrix_calculation(pA, pB, pA, OP);

    // Calculate derivatives
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            dco::derivative(B(i, j)) = 1.0;
        }
    }
    DCO_M::global_tape->interpret_adjoint();

    if(!no_dot) CREATE_DOT(std::string("dot/") + QUOTE(EIGEN_TEST_FUNC) + ".matrix." + std::to_string(OP) + "." QUOTE(EIGEN_AD_TEST_COMPARE) + ".dot" );

    // Check against passive result
    VERIFY_IS_EQUAL(B, pB);

    // Compare derivatives with vanilla Eigen version
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            write_comparison_to_file(EQ, dco::derivative(B(i,j)));
        }
     }
    write_comparison_to_file(TAPE_CMP, dco::size_of(DCO_M::global_tape));

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename ArrayType> void binary_op_array(const ArrayType& m, const int OP, const int TAPE_CMP, const bool no_dot = false)
{
    // Create tape
    typedef dco::mode<typename ArrayType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Declare and init active Arrays
    ArrayType A = ArrayType::Random(m.rows(), m.cols());
    ArrayType B = ArrayType::Ones();

    // Declare and init passive Arrays
    typedef Array<typename ArrayType::Scalar, ArrayType::RowsAtCompileTime, ArrayType::ColsAtCompileTime> pArrayType;
    pArrayType pA, pB;
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            pA(i, j) = dco::value(A(i, j));
            pB(i, j) = dco::value(B(i, j));
        }
    }

    // Register active Arrays on tape
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i, j));
        }
    }

    // Do the calculation
    binary_op_matrix_calculation(A, B, pA, OP);
    binary_op_matrix_calculation(pA, pB, pA, OP);

    // Calculate derivatives
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            dco::derivative(B(i, j)) = 1.0;
        }
    }
    DCO_M::global_tape->interpret_adjoint();

    if(!no_dot) CREATE_DOT(std::string("dot/") + QUOTE(EIGEN_TEST_FUNC) + ".array." + std::to_string(OP) + "." QUOTE(EIGEN_AD_TEST_COMPARE) + ".dot" );

    // Check against passive result
    VERIFY_IS_APPROX(B, pB);

    // Compare derivatives with vanilla Eigen version
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            write_comparison_to_file(EQ, dco::derivative(B(i,j)));
        }
     }
    write_comparison_to_file(TAPE_CMP, dco::size_of(DCO_M::global_tape));

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

void test_eigen_ad_test_dco_compare_op_binary()
{
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_ADDITION, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), ADDITION, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_SUBTRACTION, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SUBTRACTION, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_PRODUCT, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), PRODUCT, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_SCHURPRODUCT, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SCHURPRODUCT, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_CWISEDIVISION, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), CWISEDIVISION, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_PRODUCT, LT ) ); // TODO: Why LT and not EQ?!
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), PRODUCT, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_DIVISION, LT ) ); // TODO: Why LT and not EQ?!
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), DIVISION, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), POW, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_MIN, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), MIN, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_MAX, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), MAX, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), ALL_AA, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), ALL_MIXED, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 50, 50>(), LARGE, LT, true ) );
}
