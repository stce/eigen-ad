// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_TEST_DCO_H
#define EIGEN_AD_TEST_DCO_H

#include <unsupported/Eigen/dco>

#ifdef EIGEN_AD_TEST_ADJOINT
  #define DCO_SCALAR ga1s
#else
  #define DCO_SCALAR gt1s
#endif

typedef dco::DCO_SCALAR<float>::type                 EIGEN_AD_TEST_TYPE_FLOAT;
typedef dco::DCO_SCALAR<double>::type                EIGEN_AD_TEST_TYPE_DOUBLE;
typedef dco::DCO_SCALAR<long double>::type           EIGEN_AD_TEST_TYPE_LDOUBLE;
typedef std::complex<dco::DCO_SCALAR<float>::type>   EIGEN_AD_TEST_TYPE_CFLOAT;
typedef std::complex<dco::DCO_SCALAR<double>::type>  EIGEN_AD_TEST_TYPE_CDOUBLE;

namespace Eigen {
namespace Eigen_AD {
namespace test {
  //
  // Specializations of Eigen-AD test structs/functions
  //
  template<typename T>
  struct value<T, typename std::enable_if<Eigen_AD::is_ad_type<T>::value>::type> {
    template<typename U>
    static typename dco::mode<T>::value_t eval(const U& arg) { return dco::value(arg); }
  };

  template<typename T>
  struct passive_type<T, typename std::enable_if<Eigen_AD::is_ad_type<T>::value>::type> {
    typedef typename dco::mode<T>::passive_t type;
  };

} // end namespace test
} // end namespace Eigen_AD

  //
  // Specializations/overloads of Eigen test structs/functions
  //

  // dco types
  template<> inline EIGEN_AD_TEST_TYPE_FLOAT   test_precision<EIGEN_AD_TEST_TYPE_FLOAT>()   { return test_precision<typename dco::mode<EIGEN_AD_TEST_TYPE_FLOAT>::value_t>();   }
  template<> inline EIGEN_AD_TEST_TYPE_DOUBLE  test_precision<EIGEN_AD_TEST_TYPE_DOUBLE>()  { return test_precision<typename dco::mode<EIGEN_AD_TEST_TYPE_DOUBLE>::value_t>();  }
  template<> inline EIGEN_AD_TEST_TYPE_LDOUBLE test_precision<EIGEN_AD_TEST_TYPE_LDOUBLE>() { return test_precision<typename dco::mode<EIGEN_AD_TEST_TYPE_LDOUBLE>::value_t>(); }
  //template<> inline EIGEN_AD_TEST_TYPE_CFLOAT  test_precision<EIGEN_AD_TEST_TYPE_CFLOAT>()  { return test_precision<typename dco::mode<EIGEN_AD_TEST_TYPE_CFLOAT>::value_t>();  }
  //template<> inline EIGEN_AD_TEST_TYPE_CDOUBLE test_precision<EIGEN_AD_TEST_TYPE_CDOUBLE>() { return test_precision<typename dco::mode<EIGEN_AD_TEST_TYPE_CDOUBLE>::value_t>(); }

  template<class T, class U, typename enable_if_dco_type>
  inline bool test_isApprox(const T& a, const U& b)
  {
      typedef typename internal::conditional<(dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::order > dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::order),
                                             typename dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::type, typename dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::type>::type DCO_T;
      return internal::isApprox(dco::value(a), dco::value(b), test_precision<typename dco::mode<DCO_T>::value_t>());
  }

  template<class T, class U, typename enable_if_dco_type = typename internal::enable_if<(dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::is_dco_type && dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::is_dco_type)>::type>
  inline bool test_isMuchSmallerThan(const T& a, const U& b)
  {
    typedef typename internal::conditional<(dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::order > dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::order),
                                           typename dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::type, typename dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::type>::type DCO_T;
    return internal::isMuchSmallerThan(dco::value(a), dco::value(b), test_precision<typename dco::mode<DCO_T>::value_t>());
  }

  template<class T, class U, typename enable_if_dco_type = typename internal::enable_if<(dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::is_dco_type && dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::is_dco_type)>::type>
  inline bool test_isApproxOrLessThan(const T& a, const U& b)
  {
    typedef typename internal::conditional<(dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::order > dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::order),
                                           typename dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::type, typename dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::type>::type DCO_T;
    return internal::isApproxOrLessThan(dco::value(a), dco::value(b), test_precision<typename dco::mode<DCO_T>::value_t>());
  }

  template<typename T, typename U, typename TRef, typename enable_if_dco_type = typename internal::enable_if<(dco::mode<typename Eigen_AD::internal::get_inner_type<T>::type>::is_dco_type && dco::mode<typename Eigen_AD::internal::get_inner_type<U>::type>::is_dco_type)>::type>
  inline bool test_isApproxWithRef(const T& a, const U& b, const TRef& ref)
  {
    //typedef typename internal::conditional<(dco::mode<U>::order > dco::mode<T>::order), typename dco::mode<U>::type, typename dco::mode<T>::type>::type DCO_T;
    return test_isApprox(a+ref, b+ref);
  }

  template<class DCO_TAPE_REAL, class DATA_HANDLER, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
  void createRandomPIMatrixOfRank(Index desired_rank, Index rows, Index cols, Matrix<dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER>, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& m)
  {
    typedef Matrix<dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER>, _Rows, _Cols, _Options, _MaxRows, _MaxCols> MatrixType;
    typedef typename internal::traits<MatrixType>::Scalar Scalar;
    enum { Rows = MatrixType::RowsAtCompileTime, Cols = MatrixType::ColsAtCompileTime };

    typedef Matrix<Scalar, Dynamic, 1> VectorType;
    typedef Matrix<Scalar, Rows, Rows> MatrixAType;
    typedef Matrix<Scalar, Cols, Cols> MatrixBType;

    if(desired_rank == 0)
    {
      m.setZero(rows,cols);
      return;
    }

    if(desired_rank == 1)
    {
      // here we normalize the vectors to get a partial isometry
      m = VectorType::Random(rows).normalized() * VectorType::Random(cols).normalized().transpose();
      return;
    }

    MatrixAType a = MatrixAType::Random(rows,rows);
    MatrixType d = MatrixType::Identity(rows,cols);
    MatrixBType  b = MatrixBType::Random(cols,cols);

    // set the diagonal such that only desired_rank non-zero entries reamain
    const Index diag_size = (std::min)(d.rows(),d.cols());
    if(diag_size != desired_rank)
      d.diagonal().segment(desired_rank, diag_size-desired_rank) = VectorType::Zero(diag_size-desired_rank);

    Ref<MatrixAType> a_ref(a);
    Ref<MatrixBType> b_ref(b);
    HouseholderQR<Ref<MatrixAType> > qra(a_ref);
    HouseholderQR<Ref<MatrixBType> > qrb(b_ref);
    m = qra.householderQ() * d * qrb.householderQ();
  }

}

#endif // EIGEN_AD_TEST_DCO_H
