// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#define EIGEN_TEST_AD
#include "main.h"
#include "eigen_ad_test_dco_operators_compare.h"

// Binary operators being tested
#define SIMPLE_NEG 0
#define NEG 1
#define CONST 2
#define SIMPLE_ABS 3
#define ABS 4
#define SIMPLE_ABS2 5
#define ABS2 6
#define SIMPLE_EXP 7
#define EXP 8
#define SIMPLE_LOG 9
#define LOG 10
#define SIMPLE_LOG1P 11
#define LOG1P 12
#define SIMPLE_LOG10 13
#define LOG10 14
#define SIMPLE_SQRT 15
#define SQRT 16
#define SIMPLE_RSQRT 17
#define RSQRT 18
#define SIMPLE_SQUARE 19
#define SQUARE 20
#define SIMPLE_CUBE 21
#define CUBE 22
#define SIMPLE_SIN 23
#define SIN 24
#define SIMPLE_COS 25
#define COS 26
#define SIMPLE_TAN 27
#define TAN 28
#define SIMPLE_ASIN 29
#define ASIN 30
#define SIMPLE_ACOS 31
#define ACOS 32
#define SIMPLE_ATAN 33
#define ATAN 34
#define SIMPLE_SINH 35
#define SINH 36
#define SIMPLE_COSH 37
#define COSH 38
#define SIMPLE_TANH 39
#define TANH 40
#define SIMPLE_INVERSE 41
#define INVERSE 42
#define CEIL 43
#define FLOOR 44
#define ROUND 45

template<typename MatrixType, typename pMatrixType> void binary_op_matrix_calculation(const MatrixType& A, MatrixType& B, const pMatrixType& M, const int OP)
{
    switch(OP) {
        default:
        case SIMPLE_NEG: B = -A ; break;
        case NEG: B = - - A ; break;
        case CONST: { const MatrixType cA(A); B = cA; break; }
        case SIMPLE_ABS: B = A.cwiseAbs() ; break;
        case ABS: B = A.cwiseAbs().cwiseAbs() ; break;
        case SIMPLE_ABS2: B = A.cwiseAbs2() ; break;
        case ABS2: B = A.cwiseAbs2().cwiseAbs2() ; break;
        case SIMPLE_SQRT: { MatrixType tmp = A.cwiseAbs(); B = tmp.cwiseSqrt() ; break; }
        case SQRT: { MatrixType tmp = A.cwiseAbs(); B = tmp.cwiseSqrt().cwiseSqrt() ; break; }
        case SIMPLE_INVERSE: B = A.cwiseInverse() ; break;
        case INVERSE: B = A.cwiseInverse().cwiseInverse() ; break;
    }
}

template<typename ArrayType, typename pArrayType> void binary_op_array_calculation(const ArrayType& A, ArrayType& B, const pArrayType& M, const int OP)
{
    (void) M;
    switch(OP) {
        default:
        case SIMPLE_EXP: B = A.exp() ; break;
        case EXP: B = A.exp().exp() ; break;
        case SIMPLE_LOG: B = A.log() ; break;
        case LOG: B = A.log().log() ; break;
        case SIMPLE_LOG1P: B = A.log1p() ; break;
        case LOG1P: B = A.log1p().log1p() ; break;
        case SIMPLE_LOG10: B = A.log10() ; break;
        case LOG10: B = A.log1p().log10() ; break;
        case SIMPLE_RSQRT: { ArrayType tmp = A.abs(); B = tmp.rqsrt() ; break; }
        case RSQRT: { ArrayType tmp = A.abs(); B = tmp.rqsrt().rqsrt() ; break; }
        case SIMPLE_SQUARE: B = A.square() ; break;
        case SQUARE: B = A.square().square() ; break;
        case SIMPLE_CUBE: B = A.cube() ; break;
        case CUBE: B = A.cube().cube() ; break;
        case SIMPLE_SIN: B = A.sin() ; break;
        case SIN: B = A.sin().sin() ; break;
        case SIMPLE_COS: B = A.cos() ; break;
        case COS: B = A.cos().cos() ; break;
        case SIMPLE_TAN: B = A.tan() ; break;
        case TAN: B = A.tan().tan() ; break;
        case SIMPLE_ASIN: B = A.asin() ; break;
        case ASIN: B = A.asin().asin() ; break;
        case SIMPLE_ACOS: B = A.acos() ; break;
        case ACOS: B = A.acos().acos() ; break;
        case SIMPLE_ATAN: B = A.atan() ; break;
        case ATAN: B = A.atan().atan() ; break;
        case SIMPLE_SINH: B = A.sinh() ; break;
        case SINH: B = A.sinh().sinh() ; break;
        case SIMPLE_COSH: B = A.cosh() ; break;
        case COSH: B = A.cosh().cosh() ; break;
        case SIMPLE_TANH: B = A.tanh() ; break;
        case TANH: B = A.tanh().tanh() ; break;
        case CEIL: B = A.ceil() ; break;
        case FLOOR: B = A.floor() ; break;
        case ROUND: B = A.round() ; break;
    }
}

template<typename MatrixType> void binary_op_matrix(const MatrixType& m, const int OP, const int TAPE_CMP)
{
    // Create tape
    typedef dco::mode<typename MatrixType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Declare and init active Matrices
    MatrixType A = MatrixType::Random(m.rows(), m.cols());
    MatrixType B = MatrixType::Ones();

    // Declare and init passive Matrices
    typedef Matrix<typename MatrixType::Scalar, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> pMatrixType;
    pMatrixType pA, pB;
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            pA(i, j) = dco::value(A(i, j));
            pB(i, j) = dco::value(B(i, j));
        }
    }

    // Register active Matrices on tape
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i, j));
        }
    }

    // Do the calculation
    binary_op_matrix_calculation(A, B, pA, OP);
    binary_op_matrix_calculation(pA, pB, pA, OP);

    // Calculate derivatives
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            dco::derivative(B(i, j)) = 1.0;
        }
    }
    DCO_M::global_tape->interpret_adjoint();

    CREATE_DOT(std::string("dot/") + QUOTE(EIGEN_TEST_FUNC) + ".matrix." + std::to_string(OP) + "." QUOTE(EIGEN_AD_TEST_COMPARE) + ".dot" );

    // Check against passive result
    VERIFY_IS_EQUAL(B, B);

    // Compare derivatives with vanilla Eigen version
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            write_comparison_to_file(EQ, dco::derivative(B(i,j)));
        }
     }
    write_comparison_to_file(TAPE_CMP, dco::size_of(DCO_M::global_tape));

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename ArrayType> void binary_op_array(const ArrayType& m, const int OP, const int TAPE_CMP)
{
    // Create tape
    typedef dco::mode<typename ArrayType::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Declare and init active Arrays
    ArrayType A = ArrayType::Random(m.rows(), m.cols());
    ArrayType B = ArrayType::Ones();

    // Declare and init passive Arrays
    typedef Array<typename ArrayType::Scalar, ArrayType::RowsAtCompileTime, ArrayType::ColsAtCompileTime> pArrayType;
    pArrayType pA, pB;
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            pA(i, j) = dco::value(A(i, j));
            pB(i, j) = dco::value(B(i, j));
        }
    }

    // Register active Arrays on tape
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i, j));
        }
    }

    // Do the calculation
    binary_op_matrix_calculation(A, B, pA, OP);
    binary_op_matrix_calculation(pA, pB, pA, OP);

    // Calculate derivatives
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            dco::derivative(B(i, j)) = 1.0;
        }
    }
    DCO_M::global_tape->interpret_adjoint();

    CREATE_DOT(std::string("dot/") + QUOTE(EIGEN_TEST_FUNC) + ".array." + std::to_string(OP) + "." QUOTE(EIGEN_AD_TEST_COMPARE) + ".dot" );

    // Check against passive result
    VERIFY_IS_APPROX(B, pB);

    // Compare derivatives with vanilla Eigen version
    for(int i=0; i<m.rows(); i++) {
        for(int j=0; j<m.cols(); j++) {
            write_comparison_to_file(EQ, dco::derivative(B(i,j)));
        }
     }
    write_comparison_to_file(TAPE_CMP, dco::size_of(DCO_M::global_tape));

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

void test_eigen_ad_test_dco_compare_op_unary()
{
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_NEG, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), NEG, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), CONST, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_ABS, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), ABS, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_ABS2, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), ABS2, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_EXP, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), EXP, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_LOG, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), LOG, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_LOG1P, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), LOG1P, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_LOG10, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), LOG10, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_SQRT, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SQRT, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_RSQRT, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), RSQRT, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_SQUARE, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SQUARE, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_CUBE, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), CUBE, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_SIN, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIN, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_COS, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), COS, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_TAN, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), TAN, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_ASIN, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), ASIN, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_ACOS, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), ACOS, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), SIMPLE_ATAN, LT ) ); // dco TODO: Why LT?
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), ATAN, LT ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), SIMPLE_INVERSE, EQ ) );
  CALL_SUBTEST( binary_op_matrix(Matrix<dco_a1d, 3, 3>(), INVERSE, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), CEIL, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), FLOOR, LT ) );
  CALL_SUBTEST( binary_op_array(Array<dco_a1d, 3, 3>(), ROUND, LT ) );

}
