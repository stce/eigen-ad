// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <iostream>
#include <fstream>

// Equal, lower, ..., than the VANILLA version
#define EQ 0
#define LT 1

bool is_empty(std::ifstream& pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}

bool CMP_implementation(int CMP, double value1, double value2) {
    switch(CMP) {
        case EQ: return value1 == value2;
        case LT: return value1 < value2;
    }
    std::cerr << "Unknown compare tag!" << std::endl;
    return false;
}

int main(int argc, char *argv[])
{
    if(argc != 2) return 1;

    // Open out files
    std::ifstream infile_ad, infile_no_features;
    infile_ad.open(std::string(argv[1]) + ".ad.out");
    infile_no_features.open(std::string(argv[1]) + ".no_features.out");

    // Check for error
    if(!infile_ad || !infile_no_features || is_empty(infile_ad) || is_empty(infile_no_features)) {
        std::cerr << "Outfile(s) not found!" << std::endl;
        return 1;
    }

    int cmp_ad, cmp_vno_features, line = 0;
    double value_ad, value_no_features;
    while (infile_ad >> cmp_ad >> value_ad && infile_no_features >> cmp_vno_features >> value_no_features)
    {
        line++;
        if(cmp_ad != cmp_vno_features) {
            std::cerr << "CMP tag missmatch in Outfiles!" << std::endl;
            return 1;
        }
        if(!CMP_implementation(cmp_ad, value_ad, value_no_features)) {
            std::cerr << "Comparison failed for output line " << line << std::endl;
            std::cerr << "Comparison operator tag: " << cmp_ad << std::endl;
            std::cerr << "Value ad: " << value_ad << std::endl;
            std::cerr << "Value no_features: " << value_no_features << std::endl;
            return 1;
        }

    }

    // Close and delete out files
    infile_ad.close();
    infile_no_features.close();
    std::remove( (std::string(argv[1]) + ".ad.out").c_str() );
    std::remove( (std::string(argv[1]) + ".no_features.out").c_str() );

    return 0;
}
