// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#define EIGEN_TEST_AD
#include "main.h"

#undef EIGEN_TEST_MAX_SIZE
#define EIGEN_TEST_MAX_SIZE 100

// General computation
template<typename MatrixTypeA, typename MatrixTypeB, typename MatrixTypeC> unsigned int product_computation(const MatrixTypeA& A, const MatrixTypeB& B, MatrixTypeC& C)
{
    C = A * B;

    return 1;
}

// Dynamic case
template<typename Scalar> unsigned int product_computation(const Matrix<Scalar, Dynamic, Dynamic>& A, const Matrix<Scalar, Dynamic, Dynamic>& B, Matrix<Scalar, Dynamic, Dynamic>& C)
{
    C = A * B;

    return 1;
}

// Quadratic fixed-size matrices
template<typename Scalar, int size> unsigned int product_computation(const Matrix<Scalar, size, size>& A, const Matrix<Scalar, size, size>& B, Matrix<Scalar, size, size>& C)
{
    C = A * B.transpose() + A * (B*A).transpose() - A*A.transpose()*B*A;
    C -= A*A;
    C += B*B;
    return 8;
}

// Quadratic 16x16 matrices (used to test Map<> support)
template<typename Scalar> unsigned int product_computation(const Matrix<Scalar, 16, 16>& A, const Matrix<Scalar, 16, 16>& B, Matrix<Scalar, 16, 16>& C)
{
  typedef Matrix<Scalar, 8, 8> MatrixTypeMapped;

  const Map<const MatrixTypeMapped, 0, Stride<32, 2> > A_Map(A.data(), 8, 8), B_Map(B.data(), 8, 8);
  Map<MatrixTypeMapped, 0, Stride<32, 2> > C_Map(C.data(), 8, 8);

  C_Map.noalias() = A_Map * B_Map;

  return 1;
}

template<typename MatrixTypeA, typename MatrixTypeB, typename MatrixTypeC, typename MatrixTypePassiveA, typename MatrixTypePassiveB, typename MatrixTypePassiveC>
void active_product_t1(const MatrixTypePassiveA& _A, MatrixTypePassiveA&,
                       const MatrixTypePassiveB& _B, MatrixTypePassiveB&,
                       MatrixTypePassiveC& _C, MatrixTypePassiveC& _C_t1)
{
    // Declare and init active types
    MatrixTypeA A(_A.rows(), _A.cols());
    MatrixTypeB B(_B.rows(), _B.cols());
    MatrixTypeC C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
            dco::derivative(A(i,j)) = 1.0;
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            dco::value(B(i,j)) = _B(i,j);
            dco::derivative(B(i,j)) = 1.0;
        }
    }

    // Compute
    product_computation(A, B, C);

    // Save output
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
        _C(i,j)    = dco::value(C(i,j));
        _C_t1(i,j) = dco::derivative(C(i,j));
      }
    }
}

template<typename MatrixTypeA, typename MatrixTypeB, typename MatrixTypeC, typename MatrixTypePassiveA, typename MatrixTypePassiveB, typename MatrixTypePassiveC>
void active_product_a1(const MatrixTypePassiveA& _A, MatrixTypePassiveA& _A_a1,
                       const MatrixTypePassiveB& _B, MatrixTypePassiveB& _B_a1,
                       MatrixTypePassiveC& _C, const MatrixTypePassiveC& _C_a1)
{
    // Declare and init active types
    MatrixTypeA A(_A.rows(), _A.cols());
    MatrixTypeB B(_B.rows(), _B.cols());
    MatrixTypeC C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            dco::value(B(i,j)) = _B(i,j);
        }
    }

    // Create tape
    typedef dco::mode<typename MatrixTypeA::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            DCO_M::global_tape->register_variable(B(i,j));
        }
    }

    // Compute
    unsigned int callback_amount = product_computation(A, B, C);

    // Seed
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
          DCO_M::global_tape->register_output_variable(C(i,j));
          dco::derivative(C(i,j)) = _C_a1(i,j);
        }
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Save output
    for(int i=0; i<A.rows(); i++) {
      for(int j=0; j<A.cols(); j++) {
        _A_a1(i,j) = dco::derivative(A(i,j));
      }
    }
    for(int i=0; i<B.rows(); i++) {
      for(int j=0; j<B.cols(); j++) {
        _B_a1(i,j) = dco::derivative(B(i,j));
      }
    }
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
        _C(i,j) = dco::value(C(i,j));
      }
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename MatrixTypeA, typename MatrixTypeB, typename MatrixTypeC, typename MatrixTypeAVector, typename MatrixTypeBVector, typename MatrixTypeCVector, typename MatrixTypePassiveA, typename MatrixTypePassiveB, typename MatrixTypePassiveC>
void active_product_a1v(const MatrixTypePassiveA& _A, const MatrixTypePassiveB& _B, const MatrixTypePassiveC& _C)
{
    // Determine two random output adjoint matrices (either 0 or 1 per element)
    Matrix<typename MatrixTypePassiveC::Scalar, MatrixTypePassiveC::RowsAtCompileTime, MatrixTypePassiveC::ColsAtCompileTime> C_a1_0, C_a1_1;
    do {
        C_a1_0 = ((Matrix<double, MatrixTypePassiveC::RowsAtCompileTime, MatrixTypePassiveC::ColsAtCompileTime>::Random(_C.rows(), _C.cols()).cwiseAbs() + MatrixXd::Constant(_C.rows(), _C.cols(), 0.5)).template cast<int>()).template cast<double>();
        C_a1_1 = ((Matrix<double, MatrixTypePassiveC::RowsAtCompileTime, MatrixTypePassiveC::ColsAtCompileTime>::Random(_C.rows(), _C.cols()).cwiseAbs() + MatrixXd::Constant(_C.rows(), _C.cols(), 0.5)).template cast<int>()).template cast<double>();
    } while(C_a1_0 == C_a1_1 || C_a1_0.isZero(0) || C_a1_1.isZero(0));

    // Declare and init active types
    MatrixTypeAVector A(_A.rows(), _A.cols());
    MatrixTypeBVector B(_B.rows(), _B.cols());
    MatrixTypeCVector C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(A(i,j)) = _A(i,j);
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            dco::value(B(i,j)) = _B(i,j);
        }
    }

    // Create tape
    typedef dco::mode<typename MatrixTypeAVector::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            DCO_M::global_tape->register_variable(B(i,j));
        }
    }

    // Compute
    unsigned int callback_amount = product_computation(A, B, C);

    // Seed
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
          DCO_M::global_tape->register_output_variable(C(i,j));
        }
    }
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
          dco::derivative(C(i,j))[0] = C_a1_0(i,j);
          dco::derivative(C(i,j))[1] = C_a1_1(i,j);
        }
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Harvest
    MatrixTypePassiveA A_vec_0(A.rows(), A.cols()), A_vec_1(A.rows(), A.cols());
    MatrixTypePassiveB B_vec_0(B.rows(), B.cols()), B_vec_1(B.rows(), B.cols());
    MatrixTypePassiveC C_vec_0(C.rows(), C.cols());
    for(int i=0; i<A.rows(); i++) {
      for(int j=0; j<A.cols(); j++) {
        A_vec_0(i,j) = dco::derivative(A(i,j))[0];
        A_vec_1(i,j) = dco::derivative(A(i,j))[1];
      }
    }
    for(int i=0; i<B.rows(); i++) {
      for(int j=0; j<B.cols(); j++) {
          B_vec_0(i,j) = dco::derivative(B(i,j))[0];
          B_vec_1(i,j) = dco::derivative(B(i,j))[1];
        }
    }
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
          C_vec_0(i,j) = dco::value(C(i,j));
        }
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);

    // Compute reference results
    MatrixTypePassiveA A_sym_a1_0(_A.rows(), _A.cols()), A_sym_a1_1(_A.rows(), _A.cols());
    MatrixTypePassiveB B_sym_a1_0(_B.rows(), _B.cols()), B_sym_a1_1(_B.rows(), _B.cols());
    MatrixTypePassiveC C_sym_0(_C.rows(), _C.cols()), C_sym_1(_C.rows(), _C.cols());
    active_product_a1<MatrixTypeA, MatrixTypeB, MatrixTypeC>(_A, A_sym_a1_0, _B, B_sym_a1_0, C_sym_0, C_a1_0);
    active_product_a1<MatrixTypeA, MatrixTypeC, MatrixTypeC>(_A, A_sym_a1_1, _B, B_sym_a1_1, C_sym_1, C_a1_1);

    // Check results
    VERIFY_IS_APPROX(C_sym_0, C_vec_0);
    VERIFY_IS_APPROX(A_sym_a1_0, A_vec_0);
    VERIFY_IS_APPROX(B_sym_a1_0, B_vec_0);
    VERIFY_IS_APPROX(A_sym_a1_1, A_vec_1);
    VERIFY_IS_APPROX(B_sym_a1_1, B_vec_1);
}

template<typename MatrixTypeA, typename MatrixTypeB, typename MatrixTypeC, typename MatrixTypePassiveA, typename MatrixTypePassiveB, typename MatrixTypePassiveC>
void active_product_t2_t1(const MatrixTypePassiveA& _A, const MatrixTypePassiveB& _B,
                                MatrixTypePassiveC& _C, MatrixTypePassiveC& _C_t1, MatrixTypePassiveC& _C_t2, MatrixTypePassiveC& _C_t2_t1)
{
    // Declare and init active types
    MatrixTypeA A(_A.rows(), _A.cols());
    MatrixTypeB B(_B.rows(), _B.cols());
    MatrixTypeC C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
            dco::derivative(dco::value(A(i,j))) = 1.0;
            dco::value(dco::derivative(A(i,j))) = 1.0;
            dco::derivative(dco::derivative(A(i,j))) = 1.0;
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            dco::value(dco::value(B(i,j))) = _B(i,j);
            dco::derivative(dco::value(B(i,j))) = 1.0;
            dco::value(dco::derivative(B(i,j))) = 1.0;
            dco::derivative(dco::derivative(B(i,j))) = 1.0;
        }
    }

    // Compute
    product_computation(A, B, C);

    // Save output
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
        _C(i,j)    = dco::value(dco::value(C(i,j)));
        _C_t1(i,j) = dco::derivative(dco::value(C(i,j)));
        _C_t2(i,j) = dco::value(dco::derivative(C(i,j)));
        _C_t2_t1(i,j) = dco::derivative(dco::derivative(C(i,j)));
      }
    }
}

template<typename MatrixTypeA, typename MatrixTypeB, typename MatrixTypeC, typename MatrixTypePassiveA, typename MatrixTypePassiveB, typename MatrixTypePassiveC>
void active_product_t2_a1(const MatrixTypePassiveA& _A, MatrixTypePassiveA& _A_a1, MatrixTypePassiveA& _A_t2_a1,
                          const MatrixTypePassiveB& _B, MatrixTypePassiveB& _B_a1, MatrixTypePassiveB& _B_t2_a1,
                                MatrixTypePassiveC& _C, MatrixTypePassiveC& _C_t2)
{
    // Declare and init active types
    MatrixTypeA A(_A.rows(), _A.cols());
    MatrixTypeB B(_B.rows(), _B.cols());
    MatrixTypeC C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            dco::value(dco::value(B(i,j))) = _B(i,j);
        }
    }

    // Create tape
    typedef dco::mode<typename MatrixTypeA::Scalar> DCO_M;
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_M::global_tape->register_variable(A(i,j));
            dco::derivative(dco::value(A(i,j))) = 1.0;
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            DCO_M::global_tape->register_variable(B(i,j));
            dco::derivative(dco::value(B(i,j))) = 1.0;
        }
    }

    // Compute
    unsigned int callback_amount = product_computation(A, B, C);

    // Seed
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
        DCO_M::global_tape->register_output_variable(C(i,j));
        dco::derivative(C(i,j)) = 1.0;
        dco::derivative(dco::derivative(C(i,j))) = 1.0;
      }
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i, j) = dco::value(dco::derivative(A(i,j)));
            _A_t2_a1(i, j) = dco::derivative(dco::derivative(A(i,j)));
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            _B_a1(i, j) = dco::value(dco::derivative(B(i,j)));
            _B_t2_a1(i, j) = dco::derivative(dco::derivative(B(i,j)));
        }
    }
    for(int i=0; i<C.rows(); i++) {
        for(int j=0; j<C.cols(); j++) {
            _C(i, j) = dco::value(dco::value(C(i,j)));
            _C_t2(i, j) = dco::derivative(dco::value(C(i,j)));
        }
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tape
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename MatrixTypeA, typename MatrixTypeB, typename MatrixTypeC, typename MatrixTypePassiveA, typename MatrixTypePassiveB, typename MatrixTypePassiveC>
void active_product_a2_a1(const MatrixTypePassiveA& _A, MatrixTypePassiveA& _A_a1, MatrixTypePassiveA& _A_a2,
                          const MatrixTypePassiveB& _B, MatrixTypePassiveB& _B_a1, MatrixTypePassiveB& _B_a2,
                                MatrixTypePassiveC& _C, MatrixTypePassiveC& _C_a2_a1)
{
    // Declare and init active types
    MatrixTypeA A(_A.rows(), _A.cols());
    MatrixTypeB B(_B.rows(), _B.cols());
    MatrixTypeC C(_C.rows(), _C.cols());
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            dco::value(dco::value(A(i,j))) = _A(i,j);
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            dco::value(dco::value(B(i,j))) = _B(i,j);
        }
    }

    // Create tapes
    typedef dco::mode<typename MatrixTypeA::Scalar::VALUE_TYPE> DCO_BASE_M;
    typedef dco::mode<typename MatrixTypeA::Scalar> DCO_M;

    DCO_BASE_M::global_tape = DCO_BASE_M::tape_t::create();
    DCO_M::global_tape = DCO_M::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_BASE_M::global_tape->register_variable(dco::value(A(i,j)));
            DCO_M::global_tape->register_variable(A(i,j));
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            DCO_BASE_M::global_tape->register_variable(dco::value(B(i,j)));
            DCO_M::global_tape->register_variable(B(i,j));
        }
    }

    // Compute
    unsigned int callback_amount = product_computation(A, B, C);

    // Seed
    for(int i=0; i<C.rows(); i++) {
      for(int j=0; j<C.cols(); j++) {
        dco::derivative(C(i,j)) = 1.0;
        DCO_BASE_M::global_tape->register_variable(dco::derivative(C(i,j)));
      }
    }

    // Calculate derivatives
    DCO_M::global_tape->interpret_adjoint();

    // Seed and save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a1(i, j) = dco::value(dco::derivative(A(i,j)));
            dco::derivative(dco::derivative(A(i,j))) = 1.0;
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            _B_a1(i, j) = dco::value(dco::derivative(B(i,j)));
            dco::derivative(dco::derivative(B(i,j))) = 1.0;
        }
    }
    for(int i=0; i<C.rows(); i++) {
        for(int j=0; j<C.cols(); j++) {
            dco::derivative(dco::value(C(i,j))) = 1.0;
        }
    }

    // Calculate derivatives
    DCO_BASE_M::global_tape->interpret_adjoint();

    // Save results
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            _A_a2(i, j) = dco::derivative(dco::value(A(i,j)));
        }
    }
    for(int i=0; i<B.rows(); i++) {
        for(int j=0; j<B.cols(); j++) {
            _B_a2(i, j) = dco::derivative(dco::value(B(i,j)));
        }
    }
    for(int i=0; i<C.rows(); i++) {
        for(int j=0; j<C.cols(); j++) {
            _C(i, j) = dco::value(dco::value(C(i,j)));
            _C_a2_a1(i, j) = dco::derivative(dco::derivative(C(i,j)));
        }
    }

    // Check callback amount
    VERIFY_IS_EQUAL(DCO_BASE_M::global_tape->number_of_callbacks(), callback_amount*3);
    VERIFY_IS_EQUAL(DCO_M::global_tape->number_of_callbacks(), callback_amount);

    // Remove tapes
    DCO_BASE_M::tape_t::remove(DCO_BASE_M::global_tape);
    DCO_M::tape_t::remove(DCO_M::global_tape);
}

template<typename Scalar, int Rows, int Cols, int Cols2> void symbolic_product_test_1st_order()
{
    typedef Matrix<Scalar, Rows, Cols>  MatrixTypeA;
    typedef Matrix<Scalar, Cols, Cols2> MatrixTypeB;
    typedef Matrix<Scalar, Rows, Cols2> MatrixTypeC;
    typedef Matrix<typename dco::gt1s<Scalar>::type, Rows, Cols>  MatrixTypeTangentA;
    typedef Matrix<typename dco::gt1s<Scalar>::type, Cols, Cols2> MatrixTypeTangentB;
    typedef Matrix<typename dco::gt1s<Scalar>::type, Rows, Cols2> MatrixTypeTangentC;
    typedef Matrix<typename dco::ga1s<Scalar>::type, Rows, Cols>  MatrixTypeSymbolicA;
    typedef Matrix<typename dco::ga1s<Scalar>::type, Cols, Cols2> MatrixTypeSymbolicB;
    typedef Matrix<typename dco::ga1s<Scalar>::type, Rows, Cols2> MatrixTypeSymbolicC;
    typedef Matrix<typename dco::ga1v<Scalar, 2>::type, Rows, Cols>  MatrixTypeSymbolicAVector;
    typedef Matrix<typename dco::ga1v<Scalar, 2>::type, Cols, Cols2> MatrixTypeSymbolicBVector;
    typedef Matrix<typename dco::ga1v<Scalar, 2>::type, Rows, Cols2> MatrixTypeSymbolicCVector;

    MatrixTypeA A, A_tan, A_tan_t1, A_sym, A_sym_a1;
    MatrixTypeB B, B_tan, B_tan_t1, B_sym, B_sym_a1;
    MatrixTypeC C, C_tan, C_tan_t1, C_sym, C_sym_a1 = MatrixTypeC::Ones();

    A = MatrixTypeA::Random();
    B = MatrixTypeB::Random();
    A_sym = A; B_sym = B;
    A_tan = A; B_tan = B;

    product_computation(A, B, C);
    active_product_t1<MatrixTypeTangentA,  MatrixTypeTangentB,  MatrixTypeTangentC> (A_tan, A_tan_t1, B_tan, B_tan_t1, C_tan, C_tan_t1);
    active_product_a1<MatrixTypeSymbolicA, MatrixTypeSymbolicB, MatrixTypeSymbolicC>(A_sym, A_sym_a1, B_sym, B_sym_a1, C_sym, C_sym_a1);

    // Check results
    VERIFY_IS_APPROX(C_sym, C);
    VERIFY_IS_APPROX(C_tan, C);
    VERIFY_IS_APPROX(C_tan_t1.sum(), A_sym_a1.sum()+B_sym_a1.sum());

    // Check vector mode
    if(A.rows() > 1) {
        active_product_a1v<MatrixTypeSymbolicA, MatrixTypeSymbolicB, MatrixTypeSymbolicC, MatrixTypeSymbolicAVector, MatrixTypeSymbolicBVector, MatrixTypeSymbolicCVector>(A_sym, B_sym, C_sym);
    }
}

template<typename Scalar> void symbolic_product_test_1st_order_dyn()
{
    typedef Matrix<Scalar, Dynamic, Dynamic> MatrixType;
    typedef Matrix<typename dco::gt1s<Scalar>::type, Dynamic, Dynamic> MatrixTypeTangent;
    typedef Matrix<typename dco::ga1s<Scalar>::type, Dynamic, Dynamic> MatrixTypeSymbolic;
    typedef Matrix<typename dco::ga1v<Scalar, 2>::type, Dynamic, Dynamic> MatrixTypeSymbolicVector;

    MatrixType A = MatrixType::Random(internal::random<int>(1,EIGEN_TEST_MAX_SIZE), internal::random<int>(1,EIGEN_TEST_MAX_SIZE));
    MatrixType B = MatrixType::Random(A.cols(), internal::random<int>(1,EIGEN_TEST_MAX_SIZE));
    MatrixType A_tan(A), A_tan_t1(A.rows(), A.cols()), A_sym(A), A_sym_a1(A.rows(), A.cols());
    MatrixType B_tan(B), B_tan_t1(B.rows(), B.cols()), B_sym(B), B_sym_a1(B.rows(), B.cols());
    MatrixType C(A.rows(), B.cols()), C_tan(A.rows(), B.cols()), C_tan_t1(A.rows(), B.cols()), C_sym(A.rows(), B.cols()), C_sym_a1 = MatrixType::Ones(A.rows(), B.cols());

    product_computation(A, B, C);
    active_product_t1<MatrixTypeTangent, MatrixTypeTangent, MatrixTypeTangent>(A_tan, A_tan_t1, B_tan, B_tan_t1, C_tan, C_tan_t1);
    active_product_a1<MatrixTypeSymbolic, MatrixTypeSymbolic, MatrixTypeSymbolic>(A_sym, A_sym_a1, B_sym, B_sym_a1, C_sym, C_sym_a1);

    // Check results
    VERIFY_IS_APPROX(C_sym, C);
    VERIFY_IS_APPROX(C_tan, C);
    VERIFY_IS_APPROX(C_tan_t1.sum(), A_sym_a1.sum()+B_sym_a1.sum());

    // Check vector mode
    if(A.rows() > 1) {
        active_product_a1v<MatrixTypeSymbolic, MatrixTypeSymbolic, MatrixTypeSymbolic, MatrixTypeSymbolicVector, MatrixTypeSymbolicVector, MatrixTypeSymbolicVector>(A_sym, B_sym, C_sym);
    }
}

template<typename Scalar, int Rows, int Cols, int Cols2> void symbolic_product_test_2nd_order()
{
    typedef Matrix<Scalar, Rows, Cols>  MatrixTypeA;
    typedef Matrix<Scalar, Cols, Cols2> MatrixTypeB;
    typedef Matrix<Scalar, Rows, Cols2> MatrixTypeC;
    typedef Matrix<typename dco::gt1s<typename dco::gt1s<Scalar>::type>::type, Rows, Cols>  MatrixTypeTangentTangentA;
    typedef Matrix<typename dco::gt1s<typename dco::gt1s<Scalar>::type>::type, Cols, Cols2> MatrixTypeTangentTangentB;
    typedef Matrix<typename dco::gt1s<typename dco::gt1s<Scalar>::type>::type, Rows, Cols2> MatrixTypeTangentTangentC;
    typedef Matrix<typename dco::ga1s<typename dco::gt1s<Scalar>::type>::type, Rows, Cols>  MatrixTypeTangentSymbolicA;
    typedef Matrix<typename dco::ga1s<typename dco::gt1s<Scalar>::type>::type, Cols, Cols2> MatrixTypeTangentSymbolicB;
    typedef Matrix<typename dco::ga1s<typename dco::gt1s<Scalar>::type>::type, Rows, Cols2> MatrixTypeTangentSymbolicC;
    typedef Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, Cols>  MatrixTypeSymbolicSymbolicA;
    typedef Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Cols, Cols2> MatrixTypeSymbolicSymbolicB;
    typedef Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Rows, Cols2> MatrixTypeSymbolicSymbolicC;

    MatrixTypeA A, A_tantan, A_tansym, A_tansym_a1, A_tansym_t2_a1, A_symsym, A_symsym_a1, A_symsym_a2;
    MatrixTypeB B, B_tantan, B_tansym, B_tansym_a1, B_tansym_t2_a1, B_symsym, B_symsym_a1, B_symsym_a2;
    MatrixTypeC C, C_tantan, C_tantan_t1, C_tantan_t2, C_tantan_t2_t1, C_tansym, C_tansym_t2, C_symsym, C_symsym_a2_a1;

    A = MatrixTypeA::Random();
    B = MatrixTypeB::Random();
    A_tantan = A; B_tantan = B;
    A_tansym = A; B_tansym = B;
    A_symsym = A; B_symsym = B;

    product_computation(A, B, C);
    active_product_t2_t1<MatrixTypeTangentTangentA, MatrixTypeTangentTangentB, MatrixTypeTangentTangentC>      (A_tantan, B_tantan, C_tantan, C_tantan_t1, C_tantan_t2, C_tantan_t2_t1);
    active_product_t2_a1<MatrixTypeTangentSymbolicA, MatrixTypeTangentSymbolicB, MatrixTypeTangentSymbolicC>   (A_tansym, A_tansym_a1, A_tansym_t2_a1, B_tansym, B_tansym_a1, B_tansym_t2_a1, C_tansym, C_tansym_t2);
    active_product_a2_a1<MatrixTypeSymbolicSymbolicA, MatrixTypeSymbolicSymbolicB, MatrixTypeSymbolicSymbolicC>(A_symsym, A_symsym_a1, A_symsym_a2, B_symsym, B_symsym_a1, B_symsym_a2, C_symsym, C_symsym_a2_a1);

    // Check primal
    VERIFY_IS_APPROX(C_tantan, C);
    VERIFY_IS_APPROX(C_tansym, C);
    VERIFY_IS_APPROX(C_symsym, C);

    // Check ToA 1st orders
    VERIFY_IS_APPROX(C_tantan_t1, C_tantan_t2);
    VERIFY_IS_APPROX(C_tansym_t2, C_tantan_t2);
    VERIFY_IS_APPROX(A_tansym_a1.sum() + B_tansym_a1.sum(), C_tantan_t2.sum());

    // Check AoA 1st orders
    VERIFY_IS_APPROX(A_symsym_a1, A_tansym_a1);
    VERIFY_IS_APPROX(B_symsym_a1, B_tansym_a1);
    VERIFY_IS_APPROX(C_symsym_a2_a1, C_tansym_t2);

    // Check ToA 2nd orders
    VERIFY_IS_APPROX(A_tansym_t2_a1.sum() + B_tansym_t2_a1.sum(), C_tantan_t2_t1.sum());

    // Check AoA 2nd orders
    VERIFY_IS_APPROX(A_symsym_a2.sum() + B_symsym_a2.sum(), C_tantan_t2_t1.sum());
}

template<typename Scalar> void symbolic_product_test_2nd_order_dyn()
{
    typedef Matrix<Scalar, Dynamic, Dynamic>  MatrixType;
    typedef Matrix<typename dco::gt1s<typename dco::gt1s<Scalar>::type>::type, Dynamic, Dynamic> MatrixTypeTangentTangent;
    typedef Matrix<typename dco::ga1s<typename dco::gt1s<Scalar>::type>::type, Dynamic, Dynamic> MatrixTypeTangentSymbolic;
    typedef Matrix<typename dco::ga1s<typename dco::ga1s<Scalar>::type>::type, Dynamic, Dynamic> MatrixTypeSymbolicSymbolic;

    MatrixType A = MatrixType::Random(internal::random<int>(1,EIGEN_TEST_MAX_SIZE), internal::random<int>(1,EIGEN_TEST_MAX_SIZE));
    MatrixType B = MatrixType::Random(A.cols(), internal::random<int>(1,EIGEN_TEST_MAX_SIZE));
    MatrixType A_tantan(A), A_tansym(A), A_tansym_a1(A.rows(), A.cols()), A_tansym_t2_a1(A.rows(), A.cols()), A_symsym(A), A_symsym_a1(A.rows(), A.cols()), A_symsym_a2(A.rows(), A.cols());
    MatrixType B_tantan(B), B_tansym(B), B_tansym_a1(B.rows(), B.cols()), B_tansym_t2_a1(B.rows(), B.cols()), B_symsym(B), B_symsym_a1(B.rows(), B.cols()), B_symsym_a2(B.rows(), B.cols());
    MatrixType C(A.rows(), B.cols()), C_tantan(A.rows(), B.cols()), C_tantan_t1(A.rows(), B.cols()), C_tantan_t2(A.rows(), B.cols()), C_tantan_t2_t1(A.rows(), B.cols()), C_tansym(A.rows(), B.cols()), C_tansym_t2(A.rows(), B.cols()), C_symsym(A.rows(), B.cols()), C_symsym_a2_a1(A.rows(), B.cols());

    product_computation(A, B, C);
    active_product_t2_t1<MatrixTypeTangentTangent, MatrixTypeTangentTangent, MatrixTypeTangentTangent>      (A_tantan, B_tantan, C_tantan, C_tantan_t1, C_tantan_t2, C_tantan_t2_t1);
    active_product_t2_a1<MatrixTypeTangentSymbolic, MatrixTypeTangentSymbolic, MatrixTypeTangentSymbolic>   (A_tansym, A_tansym_a1, A_tansym_t2_a1, B_tansym, B_tansym_a1, B_tansym_t2_a1, C_tansym, C_tansym_t2);
    active_product_a2_a1<MatrixTypeSymbolicSymbolic, MatrixTypeSymbolicSymbolic, MatrixTypeSymbolicSymbolic>(A_symsym, A_symsym_a1, A_symsym_a2, B_symsym, B_symsym_a1, B_symsym_a2, C_symsym, C_symsym_a2_a1);

    // Check primal
    VERIFY_IS_APPROX(C_tantan, C);
    VERIFY_IS_APPROX(C_tansym, C);
    VERIFY_IS_APPROX(C_symsym, C);

    // Check ToA 1st orders
    VERIFY_IS_APPROX(C_tantan_t1, C_tantan_t2);
    VERIFY_IS_APPROX(C_tansym_t2, C_tantan_t2);
    VERIFY_IS_APPROX(A_tansym_a1.sum() + B_tansym_a1.sum(), C_tantan_t2.sum());

    // Check AoA 1st orders
    VERIFY_IS_APPROX(A_symsym_a1, A_tansym_a1);
    VERIFY_IS_APPROX(B_symsym_a1, B_tansym_a1);
    VERIFY_IS_APPROX(C_symsym_a2_a1, C_tansym_t2);

    // Check ToA 2nd orders
    VERIFY_IS_APPROX(A_tansym_t2_a1.sum() + B_tansym_t2_a1.sum(), C_tantan_t2_t1.sum());

    // Check AoA 2nd orders
    VERIFY_IS_APPROX(A_symsym_a2.sum() + B_symsym_a2.sum(), C_tantan_t2_t1.sum());
}

void test_eigen_ad_test_dco_product()
{
  CALL_SUBTEST_1(( symbolic_product_test_1st_order<double, 3, 3, 3>() ));
  CALL_SUBTEST_2(( symbolic_product_test_1st_order_dyn<double>() ));
  CALL_SUBTEST_3(( symbolic_product_test_2nd_order<double, 3, 3, 3>() ));
  CALL_SUBTEST_4(( symbolic_product_test_2nd_order_dyn<double>() ));
  CALL_SUBTEST_5(( symbolic_product_test_1st_order<double, 16, 16, 16>() ));
  CALL_SUBTEST_6(( symbolic_product_test_2nd_order<double, 16, 16, 16>() ));
}

