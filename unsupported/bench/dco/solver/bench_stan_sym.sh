#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -I../../../../../ -I../tools/stan/math/ -I../tools/stan/math/lib/boost_1.69.0 -I../tools/stan/math/lib/sundials_4.1.0/include -D_REENTRANT"
Filename="dco_eigen_bench_symbolic_solvers.cpp"

Solvers=( ColPivHouseholderQR )
SolversTemplateArgs=( "" )
SolversArgs=( "" )

# Compile
i=0
procs=0
if [ -z $1 ];
then
	for solver in "${Solvers[@]}" 
	do : 
	   mkdir $solver

	   if [ -z "${SolversTemplateArgs[$i]}" ]
	   then	   
		TemplateArgString=""
	   else
		TemplateArgString=" -DTEST_SOLVER_TEMPLATE_ARGS=,"${SolversTemplateArgs[$i]}
	   fi

	   if [ -z "${SolversArgs[$i]}" ]
	   then	   
		ArgString=""
	   else
		ArgString=" -DTEST_SOLVER_ARGS=,"${SolversArgs[$i]}
	   fi

	   # Symbolic
	   OutputName=$solver/$solver"_stan_sym.out"
	   CompileStringSolver=$CompileStringBase" -o "$OutputName" -DTEST_STAN -DTEST_SYMBOLIC -DTEST_SOLVER="$solver$ArgString$TemplateArgString
	   CompileString=$CompileStringSolver" "$Filename
	   echo $CompileString
	   $CompileString &
	   pids[$procs]=$!

	   i=$((i+1))
	   procs=$((procs+1))
	   if [ "$procs" -eq 3 ]
	   then
		   # wait for all compiles
		   for pid in ${pids[*]}; do
		   	wait $pid
		   done
		   procs=0
	   fi
	done
fi

for solver in "${Solvers[@]}"
do :
   cd $solver
        for i in {200..5000..400}
        do
                for j in {1..2}
                do
                        # Run
                        ./$solver"product_stan_sym.out" $i
                done
        done
   cd ..
done

