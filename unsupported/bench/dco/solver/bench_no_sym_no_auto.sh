#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DDCO_AUTO_SUPPORT -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src"
Filename="dco_eigen_bench_symbolic_solvers.cpp"

Solvers=( ColPivHouseholderQR )
SolversTemplateArgs=( "" )
SolversArgs=( "")

# Compile
i=0
procs=0
if [ -z $1 ];
then
	for solver in "${Solvers[@]}" 
	do : 
	   mkdir $solver

	   if [ -z "${SolversTemplateArgs[$i]}" ]
	   then	   
		TemplateArgString=""
	   else
		TemplateArgString=" -DTEST_SOLVER_TEMPLATE_ARGS=,"${SolversTemplateArgs[$i]}
	   fi

	   if [ -z "${SolversArgs[$i]}" ]
	   then	   
		ArgString=""
	   else
		ArgString=" -DTEST_SOLVER_ARGS=,"${SolversArgs[$i]}
	   fi

	   # No Symbolic
	   OutputName=$solver/$solver"_no_sym_no_auto.out"
	   CompileStringSolver=$CompileStringBase" -o "$OutputName" -DTEST_SOLVER="$solver$ArgString$TemplateArgString
	   CompileString=$CompileStringSolver" -DDCO_EIGEN_NO_SYMBOLIC_SOLVERS -DDCO_EIGEN_NO_AUTO_RETURN_TYPE "$Filename
	   echo $CompileString
	   $CompileString &
	   pids[$procs]=$!

	   i=$((i+1))
	   procs=$((procs+1))
	   if [ "$procs" -eq 3 ]
	   then
		   # wait for all compiles
		   for pid in ${pids[*]}; do
		   	wait $pid
		   done
		   procs=0
	   fi
	done
fi

# Run
for solver in "${Solvers[@]}"
do :
   cd $solver
        for i in {200..5000..400}
        do
                for j in {1..2}
                do
                        # Run
                        ./$solver"_no_sym_no_auto.out" $i
                done
        done
   cd ..
done
