#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DDCO_AUTO_SUPPORT -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src"
Filename="dco_eigen_bench_symbolic_solvers.cpp"

Solvers=( ColPivHouseholderQR )
SolversTemplateArgs=( "" )
SolversArgs=( "")

# Compile
i=0
if [ -z $1 ];
then
	for solver in "${Solvers[@]}" 
	do : 
	   mkdir $solver

	   if [ -z "${SolversTemplateArgs[$i]}" ]
	   then	   
		TemplateArgString=""
	   else
		TemplateArgString=" -DTEST_SOLVER_TEMPLATE_ARGS=,"${SolversTemplateArgs[$i]}
	   fi

	   if [ -z "${SolversArgs[$i]}" ]
	   then	   
		ArgString=""
	   else
		ArgString=" -DTEST_SOLVER_ARGS=,"${SolversArgs[$i]}
	   fi

	   # Symbolic
	   OutputName=$solver/$solver"_sym.out"
	   CompileStringSolver=$CompileStringBase" -o "$OutputName" -DTEST_SOLVER="$solver$ArgString$TemplateArgString
	   CompileString=$CompileStringSolver" "$Filename
	   echo $CompileString
	   $CompileString &
	   pids[0]=$!

	   # No Symbolic
	   OutputName=$solver/$solver"_no_sym.out"
	   CompileStringSolver=$CompileStringBase" -o "$OutputName" -DTEST_SOLVER="$solver$ArgString$TemplateArgString
	   CompileString=$CompileStringSolver" -DDCO_EIGEN_NO_SYMBOLIC_SOLVERS "$Filename
	   echo $CompileString
	   $CompileString &
	   pids[1]=$!

	   # Passive
	   OutputName=$solver/$solver"_passive.out"
	   CompileStringSolver=$CompileStringBase" -o "$OutputName" -DTEST_SOLVER="$solver$ArgString$TemplateArgString
	   CompileString=$CompileStringSolver" -DTEST_PASSIVE "$Filename
	   echo $CompileString
	   $CompileString &
	   pids[2]=$!

	   i=$((i+1))

	   # wait for all compiles
	   for pid in ${pids[*]}; do
	   	wait $pid
	   done
	done
fi

# Run
for solver in "${Solvers[@]}"
do :
   cd $solver
        for i in {200..5000..400}
        do
                for j in {1..2}
                do
                        # Run
                        ./$solver"_sym.out" $i
                        ./$solver"_no_sym.out" $i
                        ./$solver"_no_sym_no_auto.out" $i
                        ./$solver"_passive.out" $i
                done
        done
   cd ..
done
