#ifdef EIGEN_AD_NO_SYMBOLIC_SOLVERS
  #define EIGEN_AD_NO_SYMBOLIC_PRODUCT
#endif

#include <fstream>
using namespace std;

#include <chrono>

#ifdef TEST_STAN
#include "stan/math.hpp"
#else
#include <Eigen/Core>
#endif

#ifdef TEST_FADBAD
#include "badiff.h"
namespace std {
inline const fadbad::B<double>& conj(const fadbad::B<double>& x)  { return x; }
inline const fadbad::B<double>& real(const fadbad::B<double>& x)  { return x; }
inline fadbad::B<double> imag(const fadbad::B<double>&)    { return 0.; }
inline fadbad::B<double> abs(const fadbad::B<double>&  x)  { return (x<0) ? -x : x; }
inline fadbad::B<double> abs2(const fadbad::B<double>& x)  { return x*x; }
}
#endif

#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/QR>
#include <Eigen/Cholesky>
#include <Eigen/SVD>

using namespace Eigen;

#ifndef TEST_SOLVER
  #define TEST_SOLVER FullPivLU
#endif
#ifndef TEST_SOLVER_TEMPLATE_ARGS
  #define TEST_SOLVER_TEMPLATE_ARGS
#endif
#ifndef TEST_SOLVER_ARGS
  #define TEST_SOLVER_ARGS
#endif

#ifdef EIGEN_AD_NO_SYMBOLIC_SOLVERS
  #ifdef EIGEN_AD_NO_AUTO_RETURN_TYPE
    #define FILE_SUFFIX _no_sym_no_auto
  #else
    #define FILE_SUFFIX _no_sym
  #endif
    #define USE_DCO
#elif defined TEST_PASSIVE
    #define FILE_SUFFIX _passive
#elif defined TEST_ADOLC
    #define FILE_SUFFIX _adolc
    #include <adolc/adolc.h>
    #include <unsupported/Eigen/AdolcForward>

template <typename MatrixTypePassive, typename VectorTypePassive>
void adolcBench(ofstream& ofs, const MatrixTypePassive& Ap, const VectorTypePassive& bp) {
    typedef Matrix<adouble, Dynamic, Dynamic> MatrixType;
    typedef Matrix<adouble, Dynamic, 1> VectorType;
    MatrixType A(Ap.rows(), Ap.cols());
    VectorType b(bp.rows(), 1);
    VectorType x(bp.rows(), 1);
    VectorTypePassive xp(bp.rows(),1);

    auto tstart = chrono::high_resolution_clock::now();

    trace_on(1,1);

    for(unsigned int i=0; i<Ap.rows(); i++) {
        for(unsigned int j=0; j<Ap.cols(); j++) {
            A(i,j) <<= Ap(i,j);
        }
        b(i,0) <<= bp(i,0);
    }

    TEST_SOLVER<MatrixType TEST_SOLVER_TEMPLATE_ARGS> solver(A TEST_SOLVER_ARGS);
    x = solver.solve(b);
    for(unsigned int i=0; i<Ap.rows(); i++) {
        x(i,0) >>= xp(i,0);
    }
    trace_off();

    auto tstart_reverse = chrono::high_resolution_clock::now();
    int sizeInput  = Ap.size()+bp.rows();
    int sizeOutput = bp.rows();
    double* u = new double[sizeOutput];
    for(unsigned int i=0; i<sizeOutput; i++) {
      u[i] = 1.0;
    }
    double* Z = new double[sizeInput];

    fos_reverse(1,sizeOutput,sizeInput,u,Z);

    auto tend = chrono::high_resolution_clock::now();

    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;
}
#elif defined TEST_ADEPT
#define FILE_SUFFIX _adept
#include <adept.h>

namespace Eigen {

template<> struct NumTraits<adept::adouble>
    : NumTraits<double>
{
  typedef adept::adouble Real;
  typedef adept::adouble NonInteger;
  typedef adept::adouble Nested;
  typedef int Literal;

  enum {
    IsComplex = 0,
    IsInteger = 0,
    IsSigned = 1,
    RequireInitialization = 1,
    ReadCost = 1,
    AddCost = 1,
    MulCost = 1
  };
};

}

//template <typename Derived>
auto operator*(const adept::Active<double>& A, const Eigen::Block<Eigen::Block<Eigen::Matrix<adept::Active<double>, -1, -1, 0, -1, -1>, -1, 1, true>, -1, 1, false>& B) {
  return B.derived().operator*(A);
}

template <typename MatrixTypePassive, typename VectorTypePassive>
void adeptBench(ofstream& ofs, const MatrixTypePassive& Ap, const VectorTypePassive& bp) {
  adept::Stack s;

  typedef Matrix<adept::adouble, Dynamic, Dynamic> MatrixType;
  typedef Matrix<adept::adouble, Dynamic, 1> VectorType;
  MatrixType A(Ap.rows(), Ap.cols());
  VectorType b(bp.rows(), 1);
  VectorType x(bp.rows(), 1);
  VectorTypePassive xp(bp.rows(),1);

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
      b(i,0) = bp(i,0);
  }

  auto tstart = chrono::high_resolution_clock::now();

  s.new_recording();

  TEST_SOLVER<MatrixType TEST_SOLVER_TEMPLATE_ARGS> solver(A TEST_SOLVER_ARGS);
  x = solver.solve(b);

  for(unsigned int i=0; i<Ap.rows(); i++) {
      x(i,0).set_gradient(1.0);
  }

  auto tstart_reverse = chrono::high_resolution_clock::now();
  s.reverse();
  auto tend = chrono::high_resolution_clock::now();

  ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

  /*
  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).get_gradient() << std::endl;
      }
      std::cout << b(i,0).get_gradient() << std::endl;
  }
  */
}
#elif defined TEST_CODI
#define FILE_SUFFIX _codi
#include <codi.hpp>
template <typename MatrixTypePassive, typename VectorTypePassive>
void codiBench(ofstream& ofs, const MatrixTypePassive& Ap, const VectorTypePassive& bp) {
  typedef Matrix<codi::RealReverse, Dynamic, Dynamic> MatrixType;
  typedef Matrix<codi::RealReverse, Dynamic, 1> VectorType;
  MatrixType A(Ap.rows(), Ap.cols());
  VectorType b(bp.rows(), 1);
  VectorType x(bp.rows(), 1);
  VectorTypePassive xp(bp.rows(),1);

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
      b(i,0) = bp(i,0);
  }

  auto tstart = chrono::high_resolution_clock::now();

  codi::RealReverse::TapeType& tape = codi::RealReverse::getGlobalTape();
  tape.setActive();

  // Register inputs
  for(int i=0;i<A.rows();i++) {
      for(int j=0;j<A.cols();j++) {
          tape.registerInput(A(i,j));
      }
      tape.registerInput(b(i,0));
  }

  TEST_SOLVER<MatrixType TEST_SOLVER_TEMPLATE_ARGS> solver(A TEST_SOLVER_ARGS);
  x = solver.solve(b);

  for(unsigned int i=0; i<Ap.rows(); i++) {
      tape.registerOutput(x(i,0));
      x(i,0).setGradient(1.0);
  }

  auto tstart_reverse = chrono::high_resolution_clock::now();
  tape.evaluate();
  auto tend = chrono::high_resolution_clock::now();

  ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

  /*
  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).getGradient() << std::endl;
      }
      std::cout << b(i,0).getGradient() << std::endl;
  }
  */

  tape.reset();

}
#elif defined TEST_FADBAD
#define FILE_SUFFIX _fadbad
#include <iostream>

namespace Eigen {

template<> struct NumTraits<fadbad::B<double> >
    : NumTraits<double>
{
  typedef fadbad::B<double> Real;
  typedef fadbad::B<double> NonInteger;
  typedef fadbad::B<double> Nested;
  typedef fadbad::B<double> Literal;

  enum {
    IsComplex = 0,
    IsInteger = 0,
    IsSigned = 1,
    RequireInitialization = 1,
    ReadCost = 1,
    AddCost = 1,
    MulCost = 1
  };
};

}


template <typename MatrixTypePassive, typename VectorTypePassive>
void fadbadBench(ofstream& ofs, const MatrixTypePassive& Ap, const VectorTypePassive& bp) {
  typedef Matrix<fadbad::B<double>, Dynamic, Dynamic> MatrixType;
  typedef Matrix<fadbad::B<double>, Dynamic, 1> VectorType;
  MatrixType A(Ap.rows(), Ap.cols());
  VectorType b(bp.rows(), 1);
  VectorType x(bp.rows(), 1);
  VectorTypePassive xp(bp.rows(),1);

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
      b(i,0) = bp(i,0);
  }

  auto tstart = chrono::high_resolution_clock::now();

  TEST_SOLVER<MatrixType TEST_SOLVER_TEMPLATE_ARGS> solver(A TEST_SOLVER_ARGS);
  x = solver.solve(b);

  auto tstart_reverse = chrono::high_resolution_clock::now();
  for(unsigned int i=0; i<Ap.rows(); i++) {
    x(i,0).diff(0,1);
  }
  auto tend = chrono::high_resolution_clock::now();

  ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).d(0) << std::endl;
      }
      std::cout << b(i,0).d(0) << std::endl;
  }
}
#elif defined TEST_STAN
#ifdef TEST_SYMBOLIC
#define FILE_SUFFIX _stan_sym
#else
#define FILE_SUFFIX _stan
#endif

template <typename MatrixTypePassive, typename VectorTypePassive>
void stanBench(ofstream& ofs, const MatrixTypePassive& Ap, const VectorTypePassive& bp) {
  typedef Matrix<stan::math::var, Dynamic, Dynamic> MatrixType;
  typedef Matrix<stan::math::var, Dynamic, 1> VectorType;
  MatrixType A(Ap.rows(), Ap.cols());
  VectorType b(bp.rows(), 1);

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
      b(i,0) = bp(i,0);
  }

  auto tstart = chrono::high_resolution_clock::now();

#ifdef TEST_SYMBOLIC
 stan::math::var Z = stan::math::sum(stan::math::mdivide_left(A,b));
#else
  TEST_SOLVER<MatrixType TEST_SOLVER_TEMPLATE_ARGS> solver(A TEST_SOLVER_ARGS);
  stan::math::var Z = (solver.solve(b)).sum();
#endif
  auto tstart_reverse = chrono::high_resolution_clock::now();
  Z.grad();
  auto tend = chrono::high_resolution_clock::now();

  ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

  /*
  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).adj() << std::endl;
      }
      std::cout << b(i,0).adj() << std::endl;
  }
  */

}
#else
    #define USE_DCO
    #define FILE_SUFFIX _sym
#endif

#define STRING(a) #a
#define STRING_Helper(a) STRING(a)

#define CONCAT_HELPER(a,b) a ## b

#define CONCAT(a,b) CONCAT_HELPER(a,b)
#define CONCAT_STRING(a,b) STRING_Helper(CONCAT_HELPER(a,b))

#define OUTFILE_NAME CONCAT_STRING(TEST_SOLVER, FILE_SUFFIX) ".dat"

// Dynamic size
template<typename Scalar> void createSystem(Matrix<Scalar, Dynamic, Dynamic>& A, Matrix<Scalar, Dynamic, 1>& b)
{
    A = Matrix<Scalar, Dynamic, Dynamic>::Random(A.rows(), A.cols());
    b = Matrix<Scalar, Dynamic, 1>::Random(b.rows(), 1);
}

// Make sure A is pos definite
template <typename MatrixType, typename VectorType> void setupSystem(MatrixType& A, VectorType& b) {
    createSystem(A, b);

    MatrixType A1 = A * A.adjoint();
    A = A1;

    // let's make sure the matrix is not singular or near singular
    for (int k=0; k<3; ++k)
    {
      MatrixType A2 = MatrixType::Random(A.rows(),A.cols());
      A1 += A2 * A2.adjoint();
    }
    A = A1;
}

#ifdef USE_DCO
#include <unsupported/Eigen/dco>

typedef typename dco::ga1s<double> DCO_MODE;

template <typename MatrixTypePassive, typename VectorTypePassive>
void firstOrderBench(ofstream& ofs, const MatrixTypePassive& Ap, const VectorTypePassive& bp) {
    typedef Matrix<typename DCO_MODE::type, Dynamic, Dynamic> MatrixType;
    typedef Matrix<typename DCO_MODE::type, Dynamic, 1> VectorType;
    MatrixType A(Ap.rows(), Ap.cols());
    VectorType b(bp.rows(), 1);
    VectorType x(bp.rows(), 1);

    for(unsigned int i=0; i<Ap.rows(); i++) {
        for(unsigned int j=0; j<Ap.cols(); j++) {
            A(i,j) = Ap(i,j);
        }
        b(i,0) = bp(i,0);
    }

    auto tstart = chrono::high_resolution_clock::now();

    // Register inputs
    DCO_MODE::global_tape = DCO_MODE::tape_t::create();
    for(int i=0;i<A.rows();i++) {
        for(int j=0;j<A.cols();j++) {
            DCO_MODE::global_tape->register_variable(A(i, j));
        }
        DCO_MODE::global_tape->register_variable(b(i, 0));
    }

    // Calculation
    TEST_SOLVER<MatrixType TEST_SOLVER_TEMPLATE_ARGS> solver(A TEST_SOLVER_ARGS);
    x = solver.solve(b);

    // Seed with ones
    for(int i=0;i<A.rows();i++)
        dco::derivative(x(i, 0)) = 1.0;

    auto tstart_reverse = chrono::high_resolution_clock::now();
    DCO_MODE::global_tape->interpret_adjoint();

    auto tend = chrono::high_resolution_clock::now();

    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << " " << dco::size_of(DCO_MODE::global_tape) << endl;

    // Remove tape
    DCO_MODE::tape_t::remove(DCO_MODE::global_tape);
}
#endif

template <typename MatrixTypePassive, typename VectorTypePassive>
void passiveBench(ofstream& ofs, const MatrixTypePassive& A, const VectorTypePassive& b) {
    VectorTypePassive x(b.rows(), 1);

    auto tstart = chrono::high_resolution_clock::now();

    // Calculation
    TEST_SOLVER<MatrixTypePassive TEST_SOLVER_TEMPLATE_ARGS> solver(A TEST_SOLVER_ARGS);
    x = solver.solve(b);

    auto tend = chrono::high_resolution_clock::now();

    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << endl;
}

void bench(ofstream& ofs, const unsigned int size) {
    typedef Matrix<double, Dynamic, Dynamic> MatrixType;
    typedef Matrix<double, Dynamic, 1> VectorType;
    MatrixType A(size, size);
    VectorType b(size, 1);

    setupSystem(A, b);

    #ifdef TEST_PASSIVE
        passiveBench(ofs, A, b);
    #elif defined TEST_ADOLC
        adolcBench(ofs, A, b);
    #elif defined TEST_ADEPT
        adeptBench(ofs, A, b);
    #elif defined TEST_CODI
        codiBench(ofs, A, b);
    #elif defined TEST_FADBAD
        fadbadBench(ofs, A, b);
    #elif defined TEST_STAN
        stanBench(ofs, A, b);
    #else
        firstOrderBench(ofs, A, b);
    #endif
}


int main(int argc, char *argv[]) {
    const int seed = 1519906151;
    srand(seed);

    ofstream ofs;
    ofs.open(OUTFILE_NAME, ofstream::out | ofstream::app);

    bench(ofs, atoi(argv[1]));

    ofs.close();

    return 0;
}
