#!/bin/bash

# -I../tools/stan/math/lib/eigen_3.3.3
CompileStringBase="g++ -std=c++14 -O3 -march=native -I../../../../../ -I../tools/stan/math/ -I../tools/stan/math/lib/boost_1.69.0 -I../tools/stan/math/lib/sundials_4.1.0/include -D_REENTRANT -DTEST_STAN"
Filename="race.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # Symbolic
   OutputName="race_stan.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   CompileString=$CompileStringProduct" "$Filename
   echo $CompileString
   $CompileString &
   pids[0]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

for i in {10000000..100000000..10000000}
do
	for j in {1..2}
	do
		# Run
		./race_stan.out $i
	done
done


