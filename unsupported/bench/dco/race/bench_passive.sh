#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DTEST_PASSIVE"
Filename="race.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # Symbolic
   OutputName="race_passive.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   CompileString=$CompileStringProduct" "$Filename
   echo $CompileString
   $CompileString &
   pids[0]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

for i in {10000000..100000000..10000000}
do
	for j in {1..2}
	do
		# Run
		./race_passive.out $i
	done
done


