#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -I../../../../../../dco_cpp_dev/src"
Filename="race.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # No Symbolic
   OutputName="race_dco.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   CompileString=$CompileStringProduct" "$Filename
   echo $CompileString
   $CompileString &
   pids[1]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

for i in {10000000..100000000..10000000}
do
	for j in {1..2}
	do
		# Run
		./race_dco.out $i
	done
done
