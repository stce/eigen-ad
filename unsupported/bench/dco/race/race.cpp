#include<vector>
#include <fstream>
#include <iostream>
#include <math.h>
#include <chrono>
using namespace std;

#include "x22.hpp"

#ifdef TEST_PASSIVE
#define FILE_SUFFIX _passive
template<typename T>
void driver(ofstream& ofs, const vector<T>& xv, T& yv, vector<T>& g) {
  size_t n=xv.size();

  auto tstart = chrono::high_resolution_clock::now();
  f(xv,yv);
  auto tend = chrono::high_resolution_clock::now();

  ofs << xv.size() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << endl;
}
#elif defined TEST_STAN
#define FILE_SUFFIX _stan
#include "stan/math.hpp"

template<typename T>
void driver(ofstream& ofs, const vector<T>& xv, T& yv, vector<T>& g) {
  size_t n=xv.size();
  vector<stan::math::var> x(n); stan::math::var y;

  auto tstart = chrono::high_resolution_clock::now();

  for (size_t j=0;j<n;j++) {
    x[j]=xv[j];
  }
  f(x,y);

  auto tstart_reverse = chrono::high_resolution_clock::now();
  y.grad();
  auto tend = chrono::high_resolution_clock::now();

  ofs << xv.size() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << " "
      << endl;

  for (size_t j=0;j<n;j++) g[j]=x[j].adj();
}
#else
#define FILE_SUFFIX _dco
#include "dco.hpp"
using namespace dco;

template<typename T>
void driver(ofstream& ofs, const vector<T>& xv, T& yv, vector<T>& g) {
  typedef ga1s<T> DCO_MODE;
  typedef typename DCO_MODE::type DCO_TYPE;
  typedef typename DCO_MODE::tape_t DCO_TAPE_TYPE;
  size_t n=xv.size();
  vector<DCO_TYPE> x(n); DCO_TYPE y;

  auto tstart = chrono::high_resolution_clock::now();

  DCO_MODE::global_tape=DCO_TAPE_TYPE::create();
  for (size_t j=0;j<n;j++) {
    DCO_MODE::global_tape->register_variable(x[j]);
    value(x[j])=xv[j];
  }
  f(x,y);
  DCO_MODE::global_tape->register_output_variable(y);
  yv=value(y);
  derivative(y)=1;

  auto tstart_reverse = chrono::high_resolution_clock::now();
  DCO_MODE::global_tape->interpret_adjoint();
  auto tend = chrono::high_resolution_clock::now();

  ofs << xv.size() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << " "
      << dco::size_of(DCO_MODE::global_tape, DCO_MODE::tape_t::size_of_checkpoints | DCO_MODE::tape_t::size_of_stack | DCO_MODE::tape_t::size_of_internal_adjoint_vector) << endl;

  for (size_t j=0;j<n;j++) g[j]=derivative(x[j]);

  DCO_TAPE_TYPE::remove(DCO_MODE::global_tape);
}
#endif


#define STRING(a) #a
#define STRING_Helper(a) STRING(a)

#define CONCAT_HELPER(a,b) a ## b

#define CONCAT(a,b) CONCAT_HELPER(a,b)
#define CONCAT_STRING(a,b) STRING_Helper(CONCAT_HELPER(a,b))

#define OUTFILE_NAME CONCAT_STRING(race, FILE_SUFFIX) ".dat"

int main(int c, char *v[]) {
    //assert(c==2); (void)c;
    cout.precision(15);
    size_t n=atoi(v[1]);
    vector<double> x(n), g(n); double y;
    for (size_t i=0;i<n;i++) x[i]=std::cos(double(i));

    ofstream ofs;
    ofs.open(OUTFILE_NAME, ofstream::out | ofstream::app);

    driver(ofs, x, y, g);

    //for(int i=0;i<g.size();i++) std::cout << g[i] << std::endl;

    ofs.close();

    return 0;
}
