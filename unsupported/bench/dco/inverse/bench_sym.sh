#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DDCO_AUTO_SUPPORT -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src"
Filename="dco_eigen_bench_inverse.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # Symbolic
   OutputName="inverse_sym.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   CompileString=$CompileStringProduct" "$Filename
   echo $CompileString
   $CompileString &
   pids[0]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

# Run
./inverse.out


