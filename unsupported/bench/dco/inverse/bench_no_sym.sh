#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DDCO_AUTO_SUPPORT -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src"
Filename="dco_eigen_bench_inverse.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # No Symbolic
   OutputName="inverse_no_sym.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   CompileString=$CompileStringProduct" -DDCO_EIGEN_NO_SYMBOLIC_SOLVERS "$Filename
   echo $CompileString
   $CompileString &
   pids[1]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

# Run
./inverse_no_sym.out


