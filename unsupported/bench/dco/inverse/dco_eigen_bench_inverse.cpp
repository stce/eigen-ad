#ifdef EIGEN_AD_NO_SYMBOLIC_SOLVERS
  #define EIGEN_AD_NO_SYMBOLIC_PRODUCT
#endif

#include <fstream>
using namespace std;

#include <chrono>

#include <Eigen/Core>
#include <Eigen/LU>
#include <unsupported/Eigen/dco>
using namespace Eigen;

#ifdef EIGEN_AD_NO_SYMBOLIC_SOLVERS
    #define FILE_SUFFIX _no_sym
#elif defined TEST_PASSIVE
    #define FILE_SUFFIX _passive
#else
    #define FILE_SUFFIX _sym
#endif

#define STRING(a) #a
#define STRING_Helper(a) STRING(a)

#define CONCAT_HELPER(a,b) a ## b

#define CONCAT(a,b) CONCAT_HELPER(a,b)
#define CONCAT_STRING(a,b) STRING_Helper(CONCAT_HELPER(a,b))

#define OUTFILE_NAME CONCAT_STRING(inverse, FILE_SUFFIX) ".dat"

typedef typename dco::ga1s<double> DCO_MODE;

// Dynamic size
template<typename Scalar> void createSystem(Matrix<Scalar, Dynamic, Dynamic>& A)
{
    A = Matrix<Scalar, Dynamic, Dynamic>::Random(A.rows(), A.cols());
    A = (A+A.transpose()).eval();
}

// Make sure A is pos definite
template <typename MatrixType> void setupSystem(MatrixType& A) {
    createSystem(A);
}

template <typename MatrixTypePassive>
void firstOrderBench(ofstream& ofs, const MatrixTypePassive& Ap) {
    typedef Matrix<typename DCO_MODE::type, Dynamic, Dynamic> MatrixType;
    MatrixType A(Ap.rows(), Ap.cols());
    MatrixType C(Ap.rows(), Ap.cols());

    for(unsigned int i=0; i<Ap.rows(); i++) {
        for(unsigned int j=0; j<Ap.cols(); j++) {
            A(i,j) = Ap(i,j);
        }
    }
    auto tstart = chrono::high_resolution_clock::now();

    // Register inputs
    DCO_MODE::global_tape = DCO_MODE::tape_t::create();
    for(int i=0;i<A.rows();i++) {
        for(int j=0;j<A.cols();j++) {
            DCO_MODE::global_tape->register_variable(A(i, j));
        }
    }

    // Calculation
    C = A.inverse();

    // Seed with ones
    for(int i=0;i<C.rows();i++) {
        for(int j=0;j<C.cols();j++) {
            dco::derivative(C(i, j)) = 1.0;
        }
    }

    auto tstart_reverse = chrono::high_resolution_clock::now();
    DCO_MODE::global_tape->interpret_adjoint();

    auto tend = chrono::high_resolution_clock::now();


    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << " "
        << dco::size_of(DCO_MODE::global_tape, DCO_MODE::tape_t::size_of_checkpoints | DCO_MODE::tape_t::size_of_stack | DCO_MODE::tape_t::size_of_internal_adjoint_vector) << endl;

    // Remove tape
    DCO_MODE::tape_t::remove(DCO_MODE::global_tape);
}

template <typename MatrixTypePassive>
void passiveBench(ofstream& ofs, const MatrixTypePassive& A) {
    MatrixTypePassive C(A.rows(), A.cols());

    auto tstart = chrono::high_resolution_clock::now();

    // Calculation
    C = A.inverse();

    auto tend = chrono::high_resolution_clock::now();

    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << endl;
}

void bench(ofstream& ofs, const unsigned int size) {
    typedef Matrix<double, Dynamic, Dynamic> MatrixType;
    MatrixType A(size, size);

    setupSystem(A);

    #ifdef TEST_PASSIVE
        passiveBench(ofs, A);
    #else
        firstOrderBench(ofs, A);
    #endif
}

int main() {
    const int seed = 1519906151;
    srand(seed);

    ofstream ofs;
    ofs.open(OUTFILE_NAME, ofstream::out | ofstream::app);

    for(unsigned int size=200; size<=5000; size+=400) {
        for(unsigned int repeat=0; repeat<2; repeat++) {
            bench(ofs, size);
        }
    }

    ofs.close();

    return 0;
}
