#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DDCO_AUTO_SUPPORT -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src"
Filename="dco_eigen_bench_inverse.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # Passive
   OutputName="inverse_passive.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   CompileString=$CompileStringProduct" -DTEST_PASSIVE "$Filename
   echo $CompileString
   $CompileString &
   pids[2]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

# Run
./inverse_passive.out


