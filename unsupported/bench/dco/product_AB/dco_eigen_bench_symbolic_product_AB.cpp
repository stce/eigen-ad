#include <fstream>
using namespace std;

#include <chrono>

#ifdef TEST_STAN
#include "stan/math.hpp"
#else
#include <Eigen/Core>
#endif
using namespace Eigen;

#ifdef EIGEN_AD_NO_SYMBOLIC_PRODUCT
    #define FILE_SUFFIX _no_sym
    #define USE_DCO
#elif defined TEST_PASSIVE
    #define FILE_SUFFIX _passive
#elif defined TEST_ADOLC
    #define FILE_SUFFIX _adolc
    #include <adolc/adolc.h>
    #include <unsupported/Eigen/AdolcForward>

template <typename MatrixTypePassive>
void adolcBench(ofstream& ofs, const MatrixTypePassive& Ap, const MatrixTypePassive& Bp) {
    typedef Matrix<adouble, Dynamic, Dynamic> MatrixType;
    MatrixType A(Ap.rows(), Ap.cols());
    MatrixType B(Bp.rows(), Bp.cols());
    MatrixType C(Ap.rows(), Bp.cols());
    MatrixTypePassive Cp(C.rows(),C.cols());

    auto tstart = chrono::high_resolution_clock::now();

    trace_on(1,1);

    for(unsigned int i=0; i<Ap.rows(); i++) {
        for(unsigned int j=0; j<Ap.cols(); j++) {
            A(i,j) <<= Ap(i,j);
        }
    }
    for(unsigned int i=0; i<Bp.rows(); i++) {
        for(unsigned int j=0; j<Bp.cols(); j++) {
            B(i,j) <<= Bp(i,j);
        }
    }

    // Calculation
    C = A * B;
    for(unsigned int i=0; i<Cp.rows(); i++) {
        for(unsigned int j=0; j<Cp.cols(); j++) {
            C(i,j) >>= Cp(i,j);
        }
    }
    trace_off();

    auto tstart_reverse = chrono::high_resolution_clock::now();
    int sizeInput  = Ap.size()+Bp.size();
    int sizeOutput = Cp.size();
    double* u = new double[sizeOutput];
    for(unsigned int i=0; i<sizeOutput; i++) {
      u[i] = 1.0;
    }
    double* Z = new double[sizeInput];

    fos_reverse(1,sizeOutput,sizeInput,u,Z);

    auto tend = chrono::high_resolution_clock::now();

    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

    /*
    for(int i=0; i<sizeInput; i++) {
      std::cout << Z[i] << std::endl;
    }
    */
}
#elif defined TEST_ADEPT
#ifdef TEST_SYMBOLIC
#define FILE_SUFFIX _adept_sym
#else
#define FILE_SUFFIX _adept
#endif
#include <adept.h>
#include <adept_arrays.h>

namespace Eigen {

template<> struct NumTraits<adept::adouble>
    : NumTraits<double>
{
  typedef adept::adouble Real;
  typedef adept::adouble NonInteger;
  typedef adept::adouble Nested;
  typedef int Literal;

  enum {
    IsComplex = 0,
    IsInteger = 0,
    IsSigned = 1,
    RequireInitialization = 1,
    ReadCost = 1,
    AddCost = 1,
    MulCost = 1
  };
};

}

template <typename MatrixTypePassive>
void adeptBench(ofstream& ofs, const MatrixTypePassive& Ap, const MatrixTypePassive& Bp) {
  adept::Stack s;

#ifdef TEST_SYMBOLIC
  typedef adept::aMatrix MatrixType;
#else
  typedef Matrix<adept::adouble, Dynamic, Dynamic> MatrixType;
#endif
  MatrixType A(Ap.rows(), Ap.cols());
  MatrixType B(Bp.rows(), Bp.cols());
  MatrixType C(Ap.rows(), Bp.cols());

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
          B(i,j) = Bp(i,j);
      }
  }

  auto tstart = chrono::high_resolution_clock::now();

  s.new_recording();

#ifdef TEST_SYMBOLIC
  C = adept::matmul(A,B);
#else
  C = A * B;
#endif

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
        C(i,j).set_gradient(1.0);
      }
  }

  auto tstart_reverse = chrono::high_resolution_clock::now();
  s.reverse();
  auto tend = chrono::high_resolution_clock::now();

  ofs << Ap.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << " " << s.memory() << endl;

  /*
  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).get_gradient() << std::endl;
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
        std::cout << B(i,j).get_gradient() << std::endl;
      }
  }
  */
}
#elif defined TEST_CODI
#define FILE_SUFFIX _codi
#include <codi.hpp>
template <typename MatrixTypePassive>
void codiBench(ofstream& ofs, const MatrixTypePassive& Ap, const MatrixTypePassive& Bp) {
  typedef Matrix<codi::RealReverse, Dynamic, Dynamic> MatrixType;
  MatrixType A(Ap.rows(), Ap.cols());
  MatrixType B(Bp.rows(), Bp.cols());
  MatrixType C(Ap.rows(), Bp.cols());
  MatrixTypePassive Cp(C.rows(),C.cols());

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
          B(i,j) = Bp(i,j);
      }
  }

  auto tstart = chrono::high_resolution_clock::now();

  codi::RealReverse::TapeType& tape = codi::RealReverse::getGlobalTape();
  tape.setActive();

  // Register inputs
  for(int i=0;i<A.rows();i++) {
      for(int j=0;j<A.cols();j++) {
          tape.registerInput(A(i,j));
      }
  }
  for(int i=0;i<B.rows();i++) {
      for(int j=0;j<B.cols();j++) {
        tape.registerInput(B(i,j));
      }
  }

  C = A * B;

  for(unsigned int i=0; i<Cp.rows(); i++) {
      for(unsigned int j=0; j<Cp.cols(); j++) {
        tape.registerOutput(C(i,j));
        C(i,j).setGradient(1.0);
      }
  }

  auto tstart_reverse = chrono::high_resolution_clock::now();
  tape.evaluate();
  auto tend = chrono::high_resolution_clock::now();

  ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

  tape.reset();

  /*
  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).getGradient() << std::endl;
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
        std::cout << B(i,j).getGradient() << std::endl;
      }
  }
  */
}
#elif defined TEST_FADBAD
#define FILE_SUFFIX _fadbad
#include "badiff.h"
#include <iostream>

namespace Eigen {

template<> struct NumTraits<fadbad::B<double> >
    : NumTraits<double>
{
  typedef fadbad::B<double> Real;
  typedef fadbad::B<double> NonInteger;
  typedef fadbad::B<double> Nested;
  typedef int Literal;

  enum {
    IsComplex = 0,
    IsInteger = 0,
    IsSigned = 1,
    RequireInitialization = 1,
    ReadCost = 1,
    AddCost = 1,
    MulCost = 1
  };
};

}

template <typename MatrixTypePassive>
void fadbadBench(ofstream& ofs, const MatrixTypePassive& Ap, const MatrixTypePassive& Bp) {
  /*
  typedef Matrix<fadbad::B<double>, Dynamic, Dynamic> MatrixType;
  MatrixType A(Ap.rows(), Ap.cols());
  MatrixType B(Bp.rows(), Bp.cols());

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
          B(i,j) = Bp(i,j);
      }
  }

  auto tstart = chrono::high_resolution_clock::now();

  fadbad::B<double> Z = (A*B).sum();

  auto tstart_reverse = chrono::high_resolution_clock::now();
  Z.diff(0,1);
  auto tend = chrono::high_resolution_clock::now();
  */
  typedef Matrix<fadbad::B<double>, Dynamic, Dynamic> MatrixType;
  MatrixType A(Ap.rows(), Ap.cols());
  MatrixType B(Bp.rows(), Bp.cols());
  MatrixType C(Ap.rows(), Bp.cols());

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
          B(i,j) = Bp(i,j);
      }
  }

  auto tstart = chrono::high_resolution_clock::now();

  C = A * B;

  auto tstart_reverse = chrono::high_resolution_clock::now();
  for(unsigned int i=0; i<C.rows(); i++) {
      for(unsigned int j=0; j<C.cols(); j++) {
        C(i,j).diff(0,1);
      }
  }
  auto tend = chrono::high_resolution_clock::now();

  ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

  /*
  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).d(0) << std::endl;
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
        std::cout << B(i,j).d(0) << std::endl;
      }
  }
  */

}
#elif defined TEST_STAN
#ifdef TEST_SYMBOLIC
#define FILE_SUFFIX _stan_sym
#else
#define FILE_SUFFIX _stan
#endif

template <typename MatrixTypePassive>
void stanBench(ofstream& ofs, const MatrixTypePassive& Ap, const MatrixTypePassive& Bp) {
  typedef Matrix<stan::math::var, Dynamic, Dynamic> MatrixType;
  MatrixType A(Ap.rows(), Ap.cols());
  MatrixType B(Bp.rows(), Bp.cols());
  MatrixType C(Ap.rows(), Bp.cols());
  MatrixTypePassive Cp(C.rows(),C.cols());

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
          B(i,j) = Bp(i,j);
      }
  }

  auto tstart = chrono::high_resolution_clock::now();

  #ifdef TEST_SYMBOLIC
  stan::math::var Z = stan::math::sum(stan::math::multiply(A, B));
  #else
  C = A * B;
  stan::math::var Z = C.sum();
  #endif

  auto tstart_reverse = chrono::high_resolution_clock::now();
  Z.grad();
  auto tend = chrono::high_resolution_clock::now();

  ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

  /*
  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).adj() << std::endl;
      }
  }
  for(unsigned int i=0; i<Bp.rows(); i++) {
      for(unsigned int j=0; j<Bp.cols(); j++) {
        std::cout << B(i,j).adj() << std::endl;
      }
  }
  */

}
#else
    #define USE_DCO
    #define FILE_SUFFIX _sym
#endif

#define STRING(a) #a
#define STRING_Helper(a) STRING(a)

#define CONCAT_HELPER(a,b) a ## b

#define CONCAT(a,b) CONCAT_HELPER(a,b)
#define CONCAT_STRING(a,b) STRING_Helper(CONCAT_HELPER(a,b))

#define OUTFILE_NAME CONCAT_STRING(product, FILE_SUFFIX) ".dat"

// Dynamic size
template<typename Scalar> void createSystem(Matrix<Scalar, Dynamic, Dynamic>& A, Matrix<Scalar, Dynamic, Dynamic>& B)
{
    A = Matrix<Scalar, Dynamic, Dynamic>::Random(A.rows(), A.cols());
    B = Matrix<Scalar, Dynamic, Dynamic>::Random(B.rows(), B.cols());
}

// Make sure A is pos definite
template <typename MatrixType> void setupSystem(MatrixType& A, MatrixType& B) {
    createSystem(A, B);
}

#ifdef USE_DCO
#include <unsupported/Eigen/dco>

typedef typename dco::ga1s<double> DCO_MODE;

template <typename MatrixTypePassive>
void firstOrderBench(ofstream& ofs, const MatrixTypePassive& Ap, const MatrixTypePassive& Bp) {
    typedef Matrix<typename DCO_MODE::type, Dynamic, Dynamic> MatrixType;
    MatrixType A(Ap.rows(), Ap.cols());
    MatrixType B(Bp.rows(), Bp.cols());
    MatrixType C(Ap.rows(), Bp.cols());

    for(unsigned int i=0; i<Ap.rows(); i++) {
        for(unsigned int j=0; j<Ap.cols(); j++) {
            A(i,j) = Ap(i,j);
        }
    }
    for(unsigned int i=0; i<Bp.rows(); i++) {
        for(unsigned int j=0; j<Bp.cols(); j++) {
            B(i,j) = Bp(i,j);
        }
    }
    auto tstart = chrono::high_resolution_clock::now();

    // Register inputs
    DCO_MODE::global_tape = DCO_MODE::tape_t::create();
    for(int i=0;i<A.rows();i++) {
        for(int j=0;j<A.cols();j++) {
            DCO_MODE::global_tape->register_variable(A(i, j));
        }
    }
    for(int i=0;i<B.rows();i++) {
        for(int j=0;j<B.cols();j++) {
            DCO_MODE::global_tape->register_variable(B(i, j));
        }
    }

    // Calculation
    C = A * B;

    // Seed with ones
    for(int i=0;i<C.rows();i++) {
        for(int j=0;j<C.cols();j++) {
            dco::derivative(C(i, j)) = 1.0;
        }
    }

    auto tstart_reverse = chrono::high_resolution_clock::now();
    DCO_MODE::global_tape->interpret_adjoint();
    auto tend = chrono::high_resolution_clock::now();

    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << " "
        << dco::size_of(DCO_MODE::global_tape, DCO_MODE::tape_t::size_of_checkpoints | DCO_MODE::tape_t::size_of_stack | DCO_MODE::tape_t::size_of_internal_adjoint_vector) << endl;

    /*
    for(int i=0;i<A.rows();i++) {
        for(int j=0;j<A.cols();j++) {
            std::cout << dco::derivative(A(i, j)) << std::endl;
        }
    }
    for(int i=0;i<B.rows();i++) {
        for(int j=0;j<B.cols();j++) {
            std::cout << dco::derivative(B(i, j)) << std::endl;
        }
    }
    */

    // Remove tape
    DCO_MODE::tape_t::remove(DCO_MODE::global_tape);
}
#endif

template <typename MatrixTypePassive>
void passiveBench(ofstream& ofs, const MatrixTypePassive& A, const MatrixTypePassive& B) {
    MatrixTypePassive C(A.rows(), B.cols());

    auto tstart = chrono::high_resolution_clock::now();

    // Calculation
    C = A * B;

    auto tend = chrono::high_resolution_clock::now();

    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << endl;
}

void bench(ofstream& ofs, const unsigned int size) {
    typedef Matrix<double, Dynamic, Dynamic> MatrixType;
    MatrixType A(size, size);
    MatrixType B(size, size);

    setupSystem(A, B);

    #ifdef TEST_PASSIVE
        passiveBench(ofs, A, B);
    #elif defined TEST_ADOLC
        adolcBench(ofs, A, B);
    #elif defined TEST_ADEPT
        adeptBench(ofs, A, B);
    #elif defined TEST_CODI
        codiBench(ofs, A, B);
    #elif defined TEST_FADBAD
        fadbadBench(ofs, A, B);
    #elif defined TEST_STAN
        stanBench(ofs, A, B);
    #else
        firstOrderBench(ofs, A, B);
    #endif
}

int main(int argc, char *argv[]) {
    const int seed = 1519906151;
    srand(seed);

    ofstream ofs;
    ofs.open(OUTFILE_NAME, ofstream::out | ofstream::app);

    bench(ofs, atoi(argv[1]));

    ofs.close();

    return 0;
}
