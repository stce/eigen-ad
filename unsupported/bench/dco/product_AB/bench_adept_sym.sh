#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src -I../tools/adept-2.0.5/install/include"
LinkerString="-L../tools/adept-2.0.5/install/lib -ladept"
Filename="dco_eigen_bench_symbolic_product_AB.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # Symbolic
   OutputName="product_adept_sym.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName" -DTEST_ADEPT -DTEST_SYMBOLIC"
   CompileString=$CompileStringProduct" "$Filename" "$LinkerString
   echo $CompileString
   $CompileString &
   pids[0]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

LD_LIBRARY_PATH=$LD_LIBRARY_PATH":"$(pwd)"/../tools/adept-2.0.5/install/lib"
export LD_LIBRARY_PATH

for i in {100..1000..100}
do
	for j in {1..2}
	do
		# Run
		./product_adept_sym.out $i
	done
done
