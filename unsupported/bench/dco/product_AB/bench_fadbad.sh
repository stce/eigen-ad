#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src -I../tools/FADBAD++"
Filename="dco_eigen_bench_symbolic_product_AB.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # Symbolic
   OutputName="product_fadbad.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName" -DTEST_FADBAD"
   CompileString=$CompileStringProduct" "$Filename
   echo $CompileString
   $CompileString &
   pids[0]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

for i in {100..1000..100}
do
	for j in {1..2}
	do
		# Run
		./product_fadbad.out $i
	done
done



