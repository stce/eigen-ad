#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DDCO_AUTO_SUPPORT -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src"
Filename="dco_eigen_bench_symbolic_product_AB.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # No Symbolic
   OutputName="product_no_sym.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   #-DDCO_CHUNK_TAPE 
   CompileString=$CompileStringProduct" -DDCO_EIGEN_NO_SYMBOLIC_PRODUCT "$Filename
   echo $CompileString
   $CompileString &
   pids[1]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

export DCO_MAX_ALLOCATION=62914560

for i in {100..1000..100}
do
	for j in {1..2}
	do
		# Run
		./product_no_sym.out $i
	done
done

