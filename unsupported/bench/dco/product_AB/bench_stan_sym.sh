#!/bin/bash

# -I../tools/stan/math/lib/eigen_3.3.3
CompileStringBase="g++ -std=c++14 -O3 -march=native -I../../../../../ -I../tools/stan/math/ -I../tools/stan/math/lib/boost_1.69.0 -I../tools/stan/math/lib/sundials_4.1.0/include -D_REENTRANT"
Filename="dco_eigen_bench_symbolic_product_AB.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # Symbolic
   OutputName="product_stan_sym.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName" -DTEST_STAN -DTEST_SYMBOLIC"
   CompileString=$CompileStringProduct" "$Filename
   echo $CompileString
   $CompileString &
   pids[0]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

for i in {100..1000..100}
do
	for j in {1..2}
	do
		# Run
		./product_stan_sym.out $i
	done
done


