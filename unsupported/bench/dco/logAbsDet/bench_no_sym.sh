#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DDCO_AUTO_SUPPORT -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src"
Filename="dco_eigen_bench_logAbsDet.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # No Symbolic
   OutputName="logAbsDet_no_sym.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   CompileString=$CompileStringProduct" -DDCO_EIGEN_NO_SYMBOLIC_SOLVERS "$Filename
   echo $CompileString
   $CompileString &
   pids[1]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

for i in {200..5000..400}
do
	for j in {1..2}
	do
		# Run
		./logAbsDet_no_sym.out $i
	done
done
