#!/bin/bash

CompileStringBase="g++ -std=c++14 -O3 -march=native -DDCO_AUTO_SUPPORT -I../../../../../ -I../../../../ -I../../../../../../dco_cpp_dev/src"
Filename="dco_eigen_bench_logAbsDet.cpp"

# Compile
i=0
if [ -z $1 ];
then
   # Passive
   OutputName="logAbsDet_passive.out"
   CompileStringProduct=$CompileStringBase" -o "$OutputName
   CompileString=$CompileStringProduct" -DTEST_PASSIVE "$Filename
   echo $CompileString
   $CompileString &
   pids[2]=$!

   i=$((i+1))

   # wait for all compiles
   for pid in ${pids[*]}; do
   	wait $pid
   done
fi

for i in {200..5000..400}
do
	for j in {1..2}
	do
		# Run
		./logAbsDet_passive.out $i
	done
done


