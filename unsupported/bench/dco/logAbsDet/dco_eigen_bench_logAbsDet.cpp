#ifdef EIGEN_AD_NO_SYMBOLIC_SOLVERS
  #define EIGEN_AD_NO_SYMBOLIC_PRODUCT
#endif

#include <fstream>
using namespace std;

#include <chrono>

#ifdef TEST_STAN
#include "stan/math.hpp"
#else
#include <Eigen/Core>
#endif

using namespace Eigen;

#ifdef EIGEN_AD_NO_SYMBOLIC_SOLVERS
    #define USE_DCO
    #define FILE_SUFFIX _no_sym
#elif defined TEST_PASSIVE
    #define FILE_SUFFIX _passive
#elif defined TEST_STAN
#ifdef TEST_SYMBOLIC
#define FILE_SUFFIX _stan_sym
#else
#define FILE_SUFFIX _stan
#endif

template <typename MatrixTypePassive>
void stanBench(ofstream& ofs, const MatrixTypePassive& Ap) {
  typedef Matrix<stan::math::var, Dynamic, Dynamic> MatrixType;
  MatrixType A(Ap.rows(), Ap.cols());

  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          A(i,j) = Ap(i,j);
      }
  }

  auto tstart = chrono::high_resolution_clock::now();

  #ifdef TEST_SYMBOLIC
  stan::math::var Z = stan::math::log_determinant(A);
  #else
  Eigen::FullPivHouseholderQR<MatrixType> hh = A.fullPivHouseholderQr();
  stan::math::var Z = hh.logAbsDeterminant();
  #endif

  auto tstart_reverse = chrono::high_resolution_clock::now();
  Z.grad();
  auto tend = chrono::high_resolution_clock::now();

  ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << endl;

  /*
  for(unsigned int i=0; i<Ap.rows(); i++) {
      for(unsigned int j=0; j<Ap.cols(); j++) {
          std::cout << A(i,j).adj() << std::endl;
      }
  }
  */

}
#else
    #define USE_DCO
    #define FILE_SUFFIX _sym
#endif

#define STRING(a) #a
#define STRING_Helper(a) STRING(a)

#define CONCAT_HELPER(a,b) a ## b

#define CONCAT(a,b) CONCAT_HELPER(a,b)
#define CONCAT_STRING(a,b) STRING_Helper(CONCAT_HELPER(a,b))

#define OUTFILE_NAME CONCAT_STRING(logAbsDet, FILE_SUFFIX) ".dat"


// Dynamic size
template<typename Scalar> void createSystem(Matrix<Scalar, Dynamic, Dynamic>& A)
{
    A = Matrix<Scalar, Dynamic, Dynamic>::Random(A.rows(), A.cols());
    A = (A+A.transpose()).eval();
}

// Make sure A is pos definite
template <typename MatrixType> void setupSystem(MatrixType& A) {
    createSystem(A);
}

#ifdef USE_DCO
#include <Eigen/QR>
#include <unsupported/Eigen/dco>

typedef typename dco::ga1s<double> DCO_MODE;

template <typename MatrixTypePassive>
void firstOrderBench(ofstream& ofs, const MatrixTypePassive& Ap) {
    typedef Matrix<typename DCO_MODE::type, Dynamic, Dynamic> MatrixType;
    MatrixType A(Ap.rows(), Ap.cols());
    typename DCO_MODE::type x;

    for(unsigned int i=0; i<Ap.rows(); i++) {
        for(unsigned int j=0; j<Ap.cols(); j++) {
            A(i,j) = Ap(i,j);
        }
    }
    auto tstart = chrono::high_resolution_clock::now();

    // Register inputs
    DCO_MODE::global_tape = DCO_MODE::tape_t::create();
    for(int i=0;i<A.rows();i++) {
        for(int j=0;j<A.cols();j++) {
            DCO_MODE::global_tape->register_variable(A(i, j));
        }
    }

    // Calculation
    FullPivHouseholderQR<MatrixType> qr(A);
    x = qr.logAbsDeterminant();

    // Seed with ones
    dco::derivative(x) = 1.0;

    auto tstart_reverse = chrono::high_resolution_clock::now();
    DCO_MODE::global_tape->interpret_adjoint();

    auto tend = chrono::high_resolution_clock::now();


    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart_reverse).count() << " "
        << dco::size_of(DCO_MODE::global_tape, DCO_MODE::tape_t::size_of_checkpoints | DCO_MODE::tape_t::size_of_stack | DCO_MODE::tape_t::size_of_internal_adjoint_vector) << endl;

    /*
    for(int i=0;i<A.rows();i++) {
        for(int j=0;j<A.cols();j++) {
            std::cout << dco::derivative(A(i, j)) << std::endl;
        }
    }
    */

    // Remove tape
    DCO_MODE::tape_t::remove(DCO_MODE::global_tape);

}
#endif

template <typename MatrixTypePassive>
void passiveBench(ofstream& ofs, const MatrixTypePassive& A) {
    typename MatrixTypePassive::Scalar x;

    auto tstart = chrono::high_resolution_clock::now();

    // Calculation
    FullPivHouseholderQR<MatrixTypePassive> qr(A);
    x = qr.logAbsDeterminant();

    auto tend = chrono::high_resolution_clock::now();

    ofs << A.rows() << " " << chrono::duration_cast<chrono::milliseconds>(tend-tstart).count() << endl;
}

void bench(ofstream& ofs, const unsigned int size) {
    typedef Matrix<double, Dynamic, Dynamic> MatrixType;
    MatrixType A(size, size);

    setupSystem(A);

    #ifdef TEST_PASSIVE
        passiveBench(ofs, A);
    #elif defined TEST_STAN
        stanBench(ofs, A);
    #else
        firstOrderBench(ofs, A);
    #endif
}

int main(int argc, char *argv[]) {
    const int seed = 1519906151;
    srand(seed);

    ofstream ofs;
    ofs.open(OUTFILE_NAME, ofstream::out | ofstream::app);

    bench(ofs, atoi(argv[1]));

    ofs.close();

    return 0;
}
