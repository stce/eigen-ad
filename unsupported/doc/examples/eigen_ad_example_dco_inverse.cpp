#include <Eigen/Core>
#include <Eigen/LU>
#include <unsupported/Eigen/dco>

using namespace Eigen;
using namespace std;

typedef dco::ga1s<double> DCO_MODE;

// Solve C=A^-1 reduce C to a scalar z via summation
// Compute Jacobian dz/dA
int main() {
    Matrix3dco_a1d A,C;
    DCO_MODE::type z;
    Matrix3d J_A;

    A = Matrix3dco_a1d::Random();
    A = (A + A.transpose()).eval();

    // Create tape
    DCO_MODE::global_tape = DCO_MODE::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_MODE::global_tape->register_variable(A(i,j));
        }
    }

    // Compute inverse
    PartialPivLU<Matrix3dco_a1d> lu(A);
    C = lu.inverse();
    z = C.sum();

    // Seed
    dco::derivative(z) = 1.0;

    // Calculate derivatives
    DCO_MODE::global_tape->interpret_adjoint();

    // Extract derivatives
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            J_A(i,j) = dco::derivative(A(i,j));
        }
    }

    // Print output
    cout << "z=" << dco::value(z) << endl << endl;
    cout << "J_A=" << endl << J_A << endl << endl;

    cout << "Tape size=" << dco::size_of(DCO_MODE::global_tape) << endl;

    // Remove tape
    DCO_MODE::tape_t::remove(DCO_MODE::global_tape);
}

