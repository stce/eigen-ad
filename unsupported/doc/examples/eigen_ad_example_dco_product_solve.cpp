#include <Eigen/Core>
#include <Eigen/LU>
#include <unsupported/Eigen/dco>

using namespace Eigen;
using namespace std;

typedef dco::ga1s<double> DCO_MODE;

// Solve Cx=b with C=A*B and reduce x to a scalar z via summation
// Compute Jacobians dz/dA, dz/dB and gradient dz/db
int main() {
    Matrix3dco_a1d A,B,C;
    Vector3dco_a1d b,x;
    DCO_MODE::type z;
    Matrix3d J_A, J_B;
    Vector3d grad_b;

    A = Matrix3dco_a1d::Random();
    B = Matrix3dco_a1d::Random();
    b = Vector3dco_a1d::Random();

    // Create tape
    DCO_MODE::global_tape = DCO_MODE::tape_t::create();

    // Register inputs
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            DCO_MODE::global_tape->register_variable(A(i,j));
            DCO_MODE::global_tape->register_variable(B(i,j));
        }
        DCO_MODE::global_tape->register_variable(b(i,0));
    }

    // Solve
    C = A * B;
    FullPivLU<Matrix3dco_a1d> lu(C);
    x = lu.solve(b);
    z = x.sum();

    // Seed
    dco::derivative(z) = 1.0;

    // Calculate derivatives
    DCO_MODE::global_tape->interpret_adjoint();

    // Extract derivatives
    for(int i=0; i<A.rows(); i++) {
        for(int j=0; j<A.cols(); j++) {
            J_A(i,j) = dco::derivative(A(i,j));
            J_B(i,j) = dco::derivative(B(i,j));
        }
        grad_b(i) = dco::derivative(b(i));
    }

    // Print output
    cout << "z=" << dco::value(z) << endl << endl;
    cout << "J_A=" << endl << J_A << endl << endl;
    cout << "J_B=" << endl << J_B << endl << endl;
    cout << "grad_b=" << endl << grad_b << endl << endl;

    cout << "Tape size=" << dco::size_of(DCO_MODE::global_tape) << endl;

    // Remove tape
    DCO_MODE::tape_t::remove(DCO_MODE::global_tape);
}
