\documentclass[a4paper,bookmarks=true,oneside]{book}

\usepackage[english]{libraries/generalstyles} 

\usepackage[export]{adjustbox}
\usepackage{diagbox}
\usepackage{wrapfig}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsxtra}
\usepackage[dvips]{epsfig,psfrag}
\usepackage{listings}
\usepackage{geometry}
\usepackage{caption}
\usepackage{cancel}
\usepackage{booktabs}
\usepackage{csquotes}
\usepackage{hyperref}
\usepackage{floatflt}
\usepackage{ltablex}
\usepackage{pbox}
\usepackage{forest}
\usepackage{tikz} 
  \usetikzlibrary{shapes,arrows,positioning,calc,fit} 

\geometry{a4paper, top=25mm, left=30mm, right=25mm, bottom=30mm,headsep=10mm, footskip=12mm}
\setlength\parindent{0em}

\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\usepackage{placeins}

\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}

\begin{document}

\include{titlepage}

\definecolor{mybg}{rgb}{0.99,0.99,0.99}
\definecolor{myblue}{rgb}{0.0,0.1,0.6}
\definecolor{mygreen}{rgb}{0.1,0.4,0}
\definecolor{myred}{rgb}{0.7,0.0,0}
\definecolor{myopenmp}{rgb}{0.8,0.1,0.8}

\lstset{
	language=C++,
	backgroundcolor=\color{mybg}, 
    basicstyle=\lst@ifdisplaystyle\normalsize\fi\ttfamily,
    keywordstyle=\normalsize\ttfamily\bfseries,
    commentstyle=\color{mygreen}\itshape,
    stringstyle=\color{mygreen},
    numberstyle=\tiny,
    showstringspaces=false,
    tabsize=2,
    numbers=left,
    frame=none,
    basewidth=0.5em,
    breaklines=true, 
    breakatwhitespace,
    morekeywords=[2]{ga1s,ga1s_mod,ga1sm_mod,ga1v_mod,ga1vm_mod,gt1s,global_tape,tape_t,type,derivative,value,get,set,tape_options,jacobian_preaccumulator_t,external_adjoint_object_t,derivative_t,value_t,local_gradient_t,passive_t,passive_value,is_dco_type,is_adjoint_type,is_tangent_type,create,remove,iterator_t,get_position,register_variable,register_output_variable,register_input,register_output,write_data,read_data,interpret_adjoint,interpret_adjoint_from,interpret_adjoint_to,interpret_adjoint_from_to,interpret_adjoint_and_reset_to,reset,reset_to,interpret_adjoint_and_zero_adjoints_from_to,interpret_adjoint_and_zero_adjoints_to,zero_adjoints,zero_adjoints_from,zero_adjoints_to,zero_adjoints_from_to,create_callback_object,insert_callback,get_output_adjoint,increment_input_adjoint,tape,tape_index,mode,vector_reference,create_local_gradient_object,finalize,put,switch_to_passive,switch_to_active,is_active,tape_options_t,chunk_size_in_byte,log_level_enum,logger,level,stream,size_of,set_sparse_interpret,unset_sparse_interpret,sparse_interpret},
    keywordstyle=[2]{\color{myblue}},
    captionpos=t
  }

\pagestyle{useheadings}
\newcounter{savesection}

\tableofcontents

\addtocontents{toc}{\protect\thispagestyle{empty}} 

\newpage

% content
%\pagenumbering{gobble}
%\sectionnumbering{Roman}
\setcounter{page}{1}
\pagenumbering{arabic}
\sectionnumbering{arabic}

\chapter{General}

\section{About}
This guide summarizes the new API introduced in Eigen-AD and is mostly aimed at authors of Algorithmic Differentiation (AD) by overloading tools. It provides information about the required structure and additions necessary such that an AD by overloading (AD-O) tool can profit from Eigen-AD. The here presented steps are more technical and code focused. Refer to the arXiv paper of Eigen-AD~\cite{peltzer2019eigenad} for a more general introduction.

\par

If you are interested in the changes made to the Eigen source code, see Appendix~\ref{appendix:changesToEigen} for a documented table.

\section{Requirements}
Due to the excessive use of \lstinline{auto}{} return-type-deduction in the Eigen code inside Eigen-AD, a \cpp{}-14 compatible compiler is required to use it. Therefore, using your AD-O tool together with Eigen-AD will also require a \cpp{}-14 conformant compiler. 
\par
Note that only the official Eigen modules are tested for the source code changes introduced in Eigen-AD at the moment; support for any unsupported module can therefore not be guaranteed.

\section{Package structure}
Eigen-AD consists of different hierarchical levels or packages in order to realize the optimization of AD-O to Eigen as generic as possible. Refer to Figure~\ref{fig:modules} for a graphical representation. In the following, each element is briefly presented.
\begin{itemize}

\item \textbf{Eigen-AD:} While the project as a whole has been named Eigen-AD, in the context of this technical guide the term refers to the Eigen source code located in the \textit{Eigen} subdirectory. Several changes had to be made to the Eigen source code in order to realize optimizations for AD-O tools. These changes are designed as \textit{entry points}, i.e.\ they do not modify the default behaviour of the Eigen code but allow additional modules to change it where desired. The new entry points are therefore not AD related and care was taken to allow a backport into the original Eigen code base. They differ in their level of obtrusiveness, with the auto-return-type-deduction being the most intrusive change. 

\item \textbf{Eigen-AD base module:} This module uses the new entry points and defines an API on its part which now is aimed at AD-O tools. It is not designed for a specific tool though.

\item \textbf{AD-O tool module:} The above mentioned API is accessed by another module which now is specific to a certain AD-O tool. Such a module should be implemented by an author of an AD-O tool and it can control whether optimizations should be enabled and also needs to provide certain typedefs and functions.
\end{itemize} 

The remainder of this technical report focuses on the Eigen-AD API and the steps required to implement an AD-O tool module.

\begin{figure}
	\begin{center}
		\includegraphics[width=1\textwidth]{graphics/modules.jpg}
                \caption[caption]{Eigen-AD package architecture.}
		\label{fig:modules}
	\end{center}
\end{figure}

\chapter{Eigen-AD API}
\label{chap:API}
This chapter introduces the API provided by the Eigen-AD base module. The here shown structs must be specialized by the AD-O tool module. Almost all come with a dummy template parameter denoted as \lstinline{enable_if}{} in order to effectively specialize using SFINAE and \lstinline{std::enable_if}{}\footnote{\url{https://en.cppreference.com/w/cpp/types/enable_if}}, but using a direct specialization is also possible. However, the SFINAE approach is generally recommended for extensive tools as only a single specialization of each Eigen-AD struct is required and the corresponding conditions can be formulated precisely.
\par
Implementations examples will be given using a generic AD-type \lstinline{ADType<T>}{} as an example, where \lstinline{T}{} is the value type. This example type also comes with template expression types named \lstinline{ADTypeExpr<...>}{}, which contain a typedef \textit{type} pointing to their corresponding \lstinline{ADType}{}. 
\par
All of the following structs must be located in the \lstinline{Eigen::Eigen_AD}{} namespace.

\section{General API}
\label{sec:APIgeneral}
The general Eigen-AD API defines mandatory aspects which must be present in an AD-O tool module. Not providing these will prevent the optimizations shown in later sections from working or will result in compiler errors.
\par
For a better understanding how to specialize the structs, examples are given using explicit specializations and using SFINAE with \lstinline{std::enable_if}{}.

\subsection{\lstinline{is_ad_type}{}}
\begin{lstlisting}
// API
template<typename T, typename enable_if = void> struct is_ad_type;
\end{lstlisting}
The \lstinline{is_ad_type}{} struct should be specialized for your tool's AD type (including possible template expressions) and provide an enumeration with an enumerator \textit{value} set to \textit{true}.
\par
Possible implementations:

\begin{enumerate}
\item Direct specialization:
\begin{lstlisting}
// In your Eigen module:
template<typename T> struct is_ad_type<ADType<T> > {
  enum { value = true }; 
};  
\end{lstlisting}
  
\item SFINAE specialization:
\begin{lstlisting}
// Somewhere in your AD tool:
template<typename T> struct is_ADType { enum { value = false }; };
template<typename T> struct is_ADType<ADType<T> > { enum { value = true  }; };
    
// In your Eigen module:
template<typename T> 
struct is_ad_type<T, typename std::enable_if<is_ADType<T>::value>::type> {
  enum { value = true }; 
};  
\end{lstlisting}
\end{enumerate}

\subsection{\lstinline{value_type}{}}
\begin{lstlisting}
// API
template<typename T, typename enable_if = void> struct value_type;
\end{lstlisting}
The \lstinline{value_type}{} struct should be specialized for your tool's AD type (including possible template expressions) and provide a typedef \textit{type} pointing to your AD types value type (i.e.\ the passive type for first order).
\par
Possible implementations:

\begin{enumerate}
\item Direct specialization:
\begin{lstlisting}
// In your Eigen module:
template<typename T> struct value_type<ADType<T> > {
  typedef T type;
};  
\end{lstlisting}
  
\item SFINAE specialization:
\begin{lstlisting}
// Somewhere in your AD tool:
template<typename T> struct value_type { typedef T type; };
template<typename T> struct value_type<ADType<T> > { typedef T type; };
    
// In your Eigen module:
template<typename T> 
struct value_type<T, typename std::enable_if<is_ad_type<T>::value>::type> {
  typedef typename value_type<T>::type type; 
};  
\end{lstlisting}
\end{enumerate}

\subsection{\lstinline{active_type}{}}
\begin{lstlisting}
// API
template<typename T, typename enable_if = void> struct active_type;
\end{lstlisting}
The \lstinline{active_type}{} is only required if your AD tools uses its own expression templates. Specialize it for your tool's template expressions and provide a typedef \textit{type} pointing to the actual AD type belonging to the template expression.
\par
Possible implementations:

\begin{enumerate}
\item Direct specialization:
\begin{lstlisting}
// In your Eigen module:
template<...> struct active_type<ADTypeExpr<...> > {
  typedef typename ADTypeExpr<...>::type type;
};  
\end{lstlisting}
  
\item SFINAE specialization:
\begin{lstlisting}
// Somewhere in your AD tool:
template<typename T> struct is_expr_type { enum { value = false }; };
template<...> struct is_expr_type<ADTypeExpr<...> > { enum { value = true }; };
    
// In your Eigen module:
template<typename T> 
struct active_type<T, typename std::enable_if<is_expr_type<T>::value>::type> {
  typedef typename T::type type; 
};  
\end{lstlisting}
\end{enumerate}

\subsection{\lstinline{scalar_binary_return_type}{}}
\begin{lstlisting}
// API
template<typename ScalarA, typename ScalarB, typename BinaryOp, typename enable_if = void>
struct scalar_binary_return_type;
\end{lstlisting}
The \lstinline{scalar_binary_return_type}{} struct can be used to modify the Eigen internal \lstinline{Scalar BinaryOpTraits}{} struct\footnote{\url{https://eigen.tuxfamily.org/dox/structEigen_1_1ScalarBinaryOpTraits.html}}. Allowed combinations must provide a typedef named \textit{ReturnType}, which indicates the result type of the operation.
\par
There are at least two use cases where a specialization is required:
\begin{itemize}
\item The AD tool uses expression templates.\\
If your tool returns template expressions -- regardless whether the auto-return-type-deduction optimization from the next section is activated or not -- they might be used inside the Eigen internals. You need to make sure that the return type of a binary operation involving a template expressions results in the corresponding AD type.

\item A higher order type is mixed with a lower order type.\\
AD types of different order might be mixed. This also includes passive types with first order. In this case the combination must be allowed and the return type should be prescribed to be the respective higher order type.
\end{itemize}
\par
The following implementation example handles template expressions:

\begin{lstlisting}
// Somewhere in your AD tool:
template <typename T, typename U, typename BinaryOp>
struct scalar_binary_return_type<T, U, BinaryOp, typename std::enable_if<is_expr_type<T>::value || is_expr_type<U>::value>::type> {
  // Change intermediates to their respective AD type. 
  typedef typename internal::conditional<is_expr_type<T>::value, typename T::type, typename U::type>::type ReturnType;
}; 				
\end{lstlisting} 

\section{Auto-Return-Type-Deduction}
\label{sec:APIauto}
The following API can be used to enable auto-return-type-deduction for your AD type. Enabling this optimization only makes sense if your AD-O tool utilizes expression templates.
\subsection{\lstinline{enable_auto_return_type}{}}
\begin{lstlisting}
// API
template<typename T, typename enable_if = void> struct enable_auto_return_type;
\end{lstlisting}
To enable auto-return-type-deduction, specialize the \lstinline{is_ad_type}{} struct for your tool's AD type and its template expressions and provide an enumeration with an enumerator \textit{value} set to \textit{true}.
\\

\section{Products}
\label{sec:APIproducts}
The following API can be used to enable and provide symbolic products.

\subsection{\lstinline{product_enable}{}}
\begin{lstlisting}
// API
template<typename Scalar, int M, int N, int Depth, typename Lhs, typename Rhs, typename enable_if = void>
struct product_enable { };
\end{lstlisting}
The \lstinline{product_enable}{} struct can be used to enable the usage of symbolic evaluation for a certain product. Specialize this for your AD type using the \lstinline{Scalar}{} template argument for certain product dimensions (using \lstinline{M}{}, \lstinline{N}{}, \lstinline{Depth}{}), for certain matrix types \lstinline{Lhs}{}, \lstinline{Rhs}{} and provide an enumeration with an enumerator \textit{value} set to \textit{true}.
\par 
A possible implementation for a matrix-matrix product between two dense matrices would look like this:

\begin{lstlisting}
// In your Eigen module:
template<typename Scalar, int M, int N, int Depth, typename Lhs, typename Rhs>
struct product_enable<Scalar, M, N, Depth, Lhs, Rhs,
  typename std::enable_if
  <(
    is_ADType<Scalar>::value &&
    M > 1 && N > 1 && Depth > 1 &&
    is_same<typename internal::traits<Lhs>::StorageKind, Dense>::value &&
    is_same<typename internal::traits<Rhs>::StorageKind, Dense>::value
  )>::type> {
  enum { value = true };
};
\end{lstlisting}

\subsection{\lstinline{product}{}}
\begin{lstlisting}
// API
template <typename Scalar, typename Lhs, typename Rhs, typename LhsEval, typename RhsEval, typename LhsValue, typename RhsValue, typename DstValue>
struct product;
\end{lstlisting}
After enabling a symbolic product, the entry point for your AD-O tool is given by defining the \lstinline{product}{} struct. Note that no specialization is possible or required, i.e.\ you must define this struct only once. The template arguments are provided for convenience reasons and can be used in the symbolic implementation:

\begin{itemize}
  \item \lstinline{Scalar}{} is the scalar type used in the product, i.e.\ the AD tool's data type.
  \item \lstinline{Lhs}{} and \lstinline{Rhs}{} are the types of the two operands of the product. Note that these types may be template expressions which still need to be evaluated.
  \item \lstinline{LhsEval}{} and \lstinline{RhsEval}{} are the resulting types after calling \lstinline{eval()}{} on objects of type \lstinline{Lhs}{} and \lstinline{Rhs}{}. They usually are dense matrices.
  \item \lstinline{LhsValue}{}, \lstinline{RhsValue}{} and \lstinline{DstValue}{} are the corresponding matrices but with the value type of your AD type as scalar. They can be used to evaluate the product using Eigen's optimized kernels.
\end{itemize}

\par
The actual code for evaluating the product must be provided inside the \lstinline{evalTo}{} function. The following is a rough example how the full implementation could look like; your tool specific code should go into the '\textit{...}' labeled sections. Your tool must support the evaluation of handwritten routines in the adjoint run.

\begin{lstlisting}
// In your Eigen module:
template <typename Scalar, typename Lhs, typename Rhs, typename LhsEval, typename RhsEval, typename LhsValue, typename RhsValue, typename DstValue>
struct product {

template<typename Dst>
static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs) {  
  // Force evaluation of lhs and rhs into temporaries.
  const LhsEval& lhsEvaluated = lhs.eval();
  const RhsEval& rhsEvaluated = rhs.eval();
  
  // Extract the values of the input matrices.
  LhsValue lhs_value = ...
  RhsValue rhs_value = ...
  
  // Evaluate the value type product.
  DstValue dst_value(dst.rows(), dst.cols());
  generic_product_impl<LhsValue, RhsValue>::evalTo(dst_value, lhs_value, rhs_value);
  
  // Copy dst_value back to dst
  ... 
  
  // Save references to the adjoints of the inputs and the output for the adjoint run. 
  ...
  
  // Save the values of the input matrices for the adjoint run.
  ...
}
\end{lstlisting}

\section{Dense Solvers}
\label{sec:APIsolvers}
The following API can be used to enable and provide symbolic operations for the dense solvers.

\subsection{\lstinline{solver_enable}{}}
\begin{lstlisting}
// API
template<typename _MatrixType, typename enable_if = void>
struct solver_enable { };
\end{lstlisting}
To enable symbolic dense solvers, specialize the \lstinline{solver_enable}{} struct for your tool's AD type using the \lstinline{_MatrixType}{} template argument (e.g.\ using the \lstinline{_MatrixType::Scalar}{} typedef) and provide an enumeration with an enumerator \textit{value} set to \textit{true}.
\par
You can explicitly disallow usage of the symbolic specialization for e.g.\ \lstinline{Ref<...>}{} matrices using the following specialization:

\begin{lstlisting}
// In your Eigen module:
template<typename T> struct is_Ref { enum { value=false }; };
template<typename T> struct is_Ref<Ref<T> > { enum { value=true }; };
  
template<typename _MatrixType>
struct solver_enable<_MatrixType, typename std::enable_if<(!is_Ref<_MatrixType>::value && is_ADType<typename _MatrixType::Scalar>::value)>::type> {
  enum { value = true };
};
\end{lstlisting}


\subsection{\lstinline{solver}{}}
\begin{lstlisting}
// API
template<typename _MatrixType, typename _ValueSolverType, typename _WrapperSolverType, typename enable_if = void>
struct solver : public solver_container<_WrapperSolverType> { };
\end{lstlisting}
The entry points for all symbolic operations of dense solvers must be provided as functions in the \lstinline{solver}{} struct. An instantiation of this struct is responsible for all symbolic operations the corresponding solver object performs during its lifetime. The template arguments can be used as specialization conditions and also directly in your implementations:

\begin{itemize}
  \item \lstinline{_MatrixType}{} is the matrix type used by the solver. It's scalar should be your tool's AD type.
  \item \lstinline{_ValueSolverType}{} is the dense solver type instantiated in the code (e.g.\ \textit{LLT} or \textit{FullPivLU}), but with the value type of your AD type as matrix scalar. An object of this type should be used for any actual evaluation.
  \item \lstinline{_WrapperSolverType}{} should be used to declare the parent class of your specialization.
\end{itemize}

Using the \lstinline{solver_container}{} parent class is part of an experimental feature to allow the computation of scalar AD derivatives if a symbolic operation required by the user is not implemented. By providing the \lstinline{EIGEN_AD_SOLVER_FALLBACK_TO_ALG}{} define, the base class will provide all possible functions and attempts to compute the derivatives by scalar AD by overloading. In case you want to explicitly define a copy constructor for the \lstinline{solver}{} struct, make sure to call the base class implementation as well:

\begin{lstlisting}
typedef solver_container<_WrapperSolverType> Base;

// Copy constructor
solver(const solver& copy) : Base(copy) {}
\end{lstlisting}

\par

The lifetime of the \lstinline{solver}{} struct is bound to the actual solver object. Therefore, instantiating the value solver as a class member will require another decomposition in the adjoint run. Instead, it is recommend to keep the value solver in memory so it can be reused in the adjoint run. If possible, your AD tool should handle the memory allocation and deallocation of the value solver object and this struct should only access it. The Eigen-AD API therefore requires to define the following function in the struct:

\begin{lstlisting}
// Mandatory
ValueSolverType& getSolver() const;
\end{lstlisting}

The main entry point for any operation regarding the dense solvers is the \lstinline{compute(A)}{} function, which computes the corresponding decomposition of \textit{A} before any other solver function can be evaluated. In scalar AD by overloading, the corresponding decomposition computation is stored in memory. For a symbolic evaluation, the decomposition should now be performed using the value type solver in order to prevent this. Defining this function in the \lstinline{solver}{} struct is mandatory. Note that the SVD solvers may use an additional parameter \textit{computationOptions}. If you want to support these solvers, the \lstinline{compute_svd}{} functions functions must be defined as well.

\begin{lstlisting}
// Mandatory
template<typename InputType>
void compute(const EigenBase<InputType>& matrix);

// Mandatory for SVD
template<typename InputType>
void compute_svd(const EigenBase<InputType>& matrix);

// Mandatory for SVD
template<typename InputType>
void compute_svd(const EigenBase<InputType>& matrix, unsigned int computationOptions);
\end{lstlisting}

Note that you should always call \lstinline{Base::compute}{} or \lstinline{Base::compute_svd}{} in your implementations in order to record the decomposition if the \lstinline{EIGEN_AD_SOLVER_FALLBACK_TO_ALG}{} define is provided by the user.
\par

All other of the functions are optional and can be defined in order to initiate a symbolic evaluation:

\begin{lstlisting}
// Optional

// Solve.
template<typename RhsType, typename DstType> 
void solve(const RhsType &rhs, DstType &dst) const;

// Transposed solve.
template<bool Conjugate, typename RhsType, typename DstType> 
void solve_transposed(const RhsType &rhs, DstType &dst) const;

// Inverse.
template<typename DstType> void inverse(DstType& dst) const;

// Determinant. 
typename internal::traits<_MatrixType>::Scalar determinant() const;

// Abs determinant.
typename _MatrixType::RealScalar absDeterminant() const;

// Log abs determinant.
typename _MatrixType::RealScalar logAbsDeterminant() const;

// Kernel.
template<typename DstType>void kernel(DstType& dst) const;

// Image.
template<typename DstType> void image(DstType& dst, const _MatrixType& originalMatrix) const;

// LU decomposition.
_MatrixType matrixLU() const;

// L of LU decomposition.
auto matrixL() const;

// Cholesky LLT decomposition.
_MatrixType matrixLLT() const;

// Cholesky LDLT decomposition.
_MatrixType matrixLDLT() const;

// Upper triangular part of Cholesky decomposition OR the U matrix of SVD.
auto matrixU() const;

// Performing a rank update.
template <typename SolverType, typename T, typename U>
void rankUpdate(SolverType& solver, const T& vec, const U& factor);

// QR decomposition.
_MatrixType matrixQR() const;

// Q of QR decomposition.
auto matrixQ() const;

// R of QR decomposition.
_MatrixType matrixR() const;

// Householder Q of QR decomposition.
auto householderQ() const;

// H coefficients of QR decomposition.
auto hCoeffs() const;

// Z of CompleteOrthogonalDecomposition. 
_MatrixType matrixZ() const;

// T of CompleteOrthogonalDecomposition. 
_MatrixType matrixT() const;

// QTZ of CompleteOrthogonalDecomposition. 
_MatrixType matrixQTZ() const;

// Z coefficients of CompleteOrthogonalDecomposition. 
auto zCoeffs() const;

// V of SVD. 
auto matrixV() const;

// Singular values of SVD. 
auto singularValues() const;
  
\end{lstlisting}

A possible implementation could look like the following, where your tool specific code should go into the '\textit{...}' labeled sections. Again, your tool must support the evaluation of handwritten routines in the adjoint run.

\begin{lstlisting}
// In your Eigen module:
template<typename _MatrixType, typename _ValueSolverType, typename _WrapperSolverType>
struct solver<_MatrixType, _ValueSolverType, _WrapperSolverType, typename std::enable_if<is_ADType<Scalar>::value>::type>
: public solver_container<_WrapperSolverType> {
  // Typedefs
  typedef solver_container<_WrapperSolverType> Base;
  typedef typename _ValueSolverType::MatrixType         ValueMatrixType;

  // Member variables.
  ValueSolverType* m_value_solver; // The value solver whose memory is handled by the AD tool.
  
  // Constructors.
  solver() : m_value_solver(nullptr) { }
  solver(const solver& copy) : Base(copy) { }
  
  // Mandatory.
  ValueSolverType& getSolver() const {
    return *m_value_solver;
  }
  
  template<typename InputType>
  void compute(const EigenBase<InputType>& matrix) {
    // Call base function.
    Base::compute(matrix);	
    
    // Let your AD tool instantiate the value solver
    m_value_solver = ...
	
    // Extract the values of the input matrix.
    ValueMatrixType matrix_value = ...
	
    // Compute the decomposition using the value solver.
    m_value_solver->compute(matrix_value);
	
    // Save references to the adjoints of the input matrix for the adjoint run. 
    // Remember that matrix might be an expression, so eval it first.
    ...
  }

  // Implement a symbolic solve() function.
  template<typename RhsType, typename DstType>
  void solve(const RhsType &rhs, DstType &dst) const {    
    // Extract the values of the input vector.
    typedef ... RhsTypeValue;
    RhsTypeValue rhs_value = ...
	
    // Use the value type solver to solve for rhs_val
    typedef ... DstTypeValue;
    DstTypeValue dst_value;
    m_value_solver->_solve_impl(rhs_value, dst_value);
	
    // Copy dst_value back to dst
    ...
	
    // Save references to the adjoints of the input and the output vector for the adjoint run.  
    ...
    
    // Save the values of dst for the adjoint run. 
    ...
  }
};
\end{lstlisting}

In this example, only a symbolic version of the \lstinline{solve}{} routine is implemented. The value type solver pointed on by \lstinline{m_value_solver}{} is kept in memory by the AD tool for the adjoint run and therefore no additional decomposition computation is required.

\section{Additional Defines}
Eigen-AD accepts the following additional defines regardless of which AD tool being used:

\begin{itemize}
\item \lstinline{EIGEN_AD_FORCE_INCLUDE_SOLVERS}{}: Include the Eigen-AD solver specializations regardless of whether the Eigen solver headers are included.
\item \lstinline{EIGEN_AD_NO_AUTO_RETURN_TYPE}{}: Globally disable auto-return-type-deduction optimization.
\item \lstinline{EIGEN_AD_NO_SYMBOLIC_PRODUCT}{}: Globally disable usage of symbolic products.
\item \lstinline{EIGEN_AD_NO_SYMBOLIC_SOLVERS}{}: Globally disable usage of symbolic solver operations.
\item \lstinline{EIGEN_AD_SOLVER_FALLBACK_TO_ALG}{}: Fallback to algorithmic solver functions (experimental).
\end{itemize}

\chapter{Implementing an AD-O tool module}
After specializing the presented Eigen-AD API, implementing an own module is straight forward. The following file structure is recommended for implementing the source files, using the generic name \textit{ADTool} as an example:

\begin{forest}
  for tree={
    font=\ttfamily,
    grow'=0,
    child anchor=west,
    parent anchor=south,
    anchor=west,
    calign=first,
    edge path={
      \noexpand\path [draw, \forestoption{edge}]
      (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
    },
    before typesetting nodes={
      if n=1
        {insert before={[,phantom]}}
        {}
    },
    fit=band,
    before computing xy={l=15pt},
  }
[unsupported
  [Eigen
      [src
        [ADTool
          [\lstinline{ADTool_eigen.h}{}]
          [\lstinline{ADTool_eigen_Product.h}{}]
          [\lstinline{ADTool_eigen_SDSolver.h}{}]
        ]
      ]
    [ADTool]
  ]
]
\end{forest}

The API given in Chapter~\ref{chap:API} from Sections~\ref{sec:APIgeneral} and \ref{sec:APIauto} should go into \lstinline{ADTool_eigen.h}{}, the product API from Section~\ref{sec:APIproducts} into \lstinline{ADTool_eigen_Product.h}{} and the solver API from Section~\ref{sec:APIsolvers} into \lstinline{ADTool_eigen_SDSolver.h}{}.
\par
An user of the AD tool module should then include the header file \lstinline{ADTool}{} to use the module; the file should like like this:

\begin{lstlisting}
#ifndef EIGEN_ADTOOL
#define EIGEN_ADTOOL

// Some ADTool defines
#define ADTOOL_DEFINE_X

// Include Eigen-AD base module
#include "AD"

// Include the actual ADTool or rather its API
#include "ADTool.hpp"

// Some other ADTool specifics
ADTOOL_MACRO_X(...)

// Include the AD-O tool module files
#include "src/ADTool/ADTool_eigen.h"
#include "src/ADTool/ADTool_eigen_Product.h"
#include "src/ADTool/ADTool_eigen_SDSolver.h"

#endif // EIGEN_ADTOOL
\end{lstlisting}

Additional documentation and examples should be placed according to the following structure:

\begin{forest}
  for tree={
    font=\ttfamily,
    grow'=0,
    child anchor=west,
    parent anchor=south,
    anchor=west,
    calign=first,
    edge path={
      \noexpand\path [draw, \forestoption{edge}]
      (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
    },
    before typesetting nodes={
      if n=1
        {insert before={[,phantom]}}
        {}
    },
    fit=band,
    before computing xy={l=15pt},
  }
[unsupported
  [doc
    [ADTool
      [\lstinline{*}{}]
    ]
    [examples 
      [\lstinline{eigen_ad_example_adtool_*.cpp}{}]
    ]
  ]
]
\end{forest}

\chapter{Using the Eigen-AD testsystem}
Eigen-AD extends the Eigen testsystem by providing a tangent and an adjoint version for each existing test. These \textit{compatibility} tests are automatically created via CMake. The AD tool to use must be selected via CMake by defining the 

\begin{lstlisting}
EIGEN_AD_TOOL
\end{lstlisting}

variable. Leaving it blank and running CMake will output the list of available AD tools.
\par
The following file structure is required by the Eigen-AD testsystem (substituting \textit{ADTool} or \textit{adtool} with your AD tool name):

\begin{forest}
  for tree={
    font=\ttfamily,
    grow'=0,
    child anchor=west,
    parent anchor=south,
    anchor=west,
    calign=first,
    edge path={
      \noexpand\path [draw, \forestoption{edge}]
      (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
    },
    before typesetting nodes={
      if n=1
        {insert before={[,phantom]}}
        {}
    },
    fit=band,
    before computing xy={l=15pt},
  }
[unsupported
  [cmake
    [ADTool
      [\lstinline{CMakeLists.txt}{}]
      [\lstinline{*.cmake (optional)}{}]
      [\lstinline{exclude_test_feature.txt (optional)}{}]
    ]
  ]
  [test
   	  [\lstinline{eigen_ad_test_adtool_pre.h}{}]
   	  [\lstinline{eigen_ad_test_adtool.h}{}]  
   	  [\lstinline{eigen_ad_test_adtool_*.cpp (optional feature tests)}{}]  
   	]
  ]
]
\end{forest}

The \lstinline{CMakeLists.txt}{} file can be used to load your tool into the testsystem by e.g.\ using the CMake \lstinline{INCLUDE_DIRECTORIES}{} and \lstinline{LINK_DIRECTORIES}{} commands. You can place additional CMake files into the directory to help locate your AD tool (e.g.\ \lstinline{FindADTool.cmake}{}). Inside the \lstinline{CMakeLists.txt}{} file, the following CMake variables can be defined:

\begin{itemize}
\item \lstinline{EIGEN_AD_TOOL_TANGENT}{} (Required): Set to \textit{true} to include your AD tool's tangent type in the testsystem.
\item \lstinline{EIGEN_AD_TOOL_ADJOINT}{} (Required): Set to \textit{true} to include your AD tool's adjoint type in the testsystem.
\item \lstinline{EIGEN_AD_TOOL_COMPILE_FLAGS}{}: Additional compile flags for the AD tool tests.
\item \lstinline{EIGEN_AD_TOOL_LINK_LIBRARIES}{}: Libraries to link against for the AD tool tests.
\item \lstinline{EIGEN_AD_TOOL_ENVIRONMENT}{}: Runtime environment variables when executing the AD tool tests.
\end{itemize}

In case you want to prevent certain tests from running with your AD tool module features (e.g.\ if some solver functions are not yet available), put these test names into the \lstinline{exclude_test_feature.txt}{} file (one per line).
\par
The Eigen testsystem defines a range of additional macros. For compatibility reasons, your AD tool header should be included before that using the \lstinline{eigen_ad_test_adtool_pre.h}{} file. Do not include your AD tool module here yet.
\par
\lstinline{eigen_ad_test_adtool.h}{} is included after all of the Eigen testsystem functions are defined. Include your AD tool module in this file. Besides that, the following typedefs must be defined for the compatibility tests:

\begin{lstlisting}
EIGEN_AD_TEST_TYPE_FLOAT     // Float
EIGEN_AD_TEST_TYPE_DOUBLE    // Double
EIGEN_AD_TEST_TYPE_LDOUBLE   // Long double
EIGEN_AD_TEST_TYPE_CFLOAT    // complex<float>
EIGEN_AD_TEST_TYPE_CDOUBLE   // complex<double>
\end{lstlisting}

These should be set to your AD tool's tangent type if the \lstinline{EIGEN_AD_TEST_TANGENT}{} define is provided and to the adjoint type in case of the \lstinline{EIGEN_AD_TEST_ADJOINT}{} define.
\par
The following Eigen-AD test functions must also be defined in the \lstinline{eigen_ad_test_adtool.h}{} (again using the generic \lstinline{ADTool<T>}{} type as example):
\begin{lstlisting}
namespace Eigen {
namespace Eigen_AD {
namespace test {

// Return the value of your AD type instance.
template<T>
struct value<ADTool<T> > {
  static auto eval(const ADTool<T>& arg) { return ... }
};

// Return the passive type of your AD type.
template<T>
struct passive_type<ADTool<T> > {
 typedef ... type;
};

}
}
}
\end{lstlisting}

Depending on your AD tool and the tests, additional Eigen testsystem functions might also need to be specialized. This can be done inside the \lstinline{eigen_ad_test_adtool.h}{} file as well. 
\par
Additional \textit{feature} tests, i.e.\ AD tool specific tests, can be added as well using the naming \\
\lstinline{eigen_ad_test_adtool_*.cpp}{}.
\par
Note that at this point, only the official Eigen modules are tested with the compatibility tests. This implies that all unsupported modules are not tested and might not work with Eigen-AD or the source changes at all.\par

\pagenumbering{gobble}
\sectionnumbering{Roman}
\setcounter{section}{\numexpr\value{savesection}}

\appendix
\include{changesEigen}

% references
\nocite{*}
\bibliographystyle{hunsrt}
\bibliography{references/references}

\end{document}

