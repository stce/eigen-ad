// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef DCO_EIGEN_META_H
#define DCO_EIGEN_META_H

namespace Eigen {
namespace Eigen_AD {
namespace dco_eigen {

// Check if an expression is a gbcp type
template<typename T> struct is_dco_gbcp_type { enum { value = dco::internal::is_gbcp_type<T>::value }; };

// Check if an expression is a dco type
template<typename T, typename U = T> struct is_dco_type { enum { value = (dco::mode<typename Eigen::internal::remove_all<T>::type>::is_dco_type || dco::mode<typename Eigen::internal::remove_all<U>::type>::is_dco_type ||
                                                                          is_dco_gbcp_type<T>::value || is_dco_gbcp_type<U>::value) }; };

// Check if an expression is a dco intermediate type
template<typename T> struct is_dco_intermediate_type { enum { value = dco::mode<typename Eigen::internal::remove_all<T>::type>::is_intermediate_type }; };

// Check if an expression is a dco vector AD type
template<typename T> struct is_dco_vector_ad_type { enum { value = false }; };
template<typename T, size_t VEC_SIZE> struct is_dco_vector_ad_type<dco::ga1v<T, VEC_SIZE> > { enum { value = true }; };
template<typename DCO_TAPE_REAL, size_t VEC_SIZE> struct is_dco_vector_ad_type<dco::internal::active_type<DCO_TAPE_REAL, dco::internal::single_tape_data<dco::ga1v<DCO_TAPE_REAL, VEC_SIZE> > > > { enum { value = true }; };

// Check if T is a LLT (Cholesky) solver
template<typename T> struct is_cholesky_solver { enum { value = false, UpLo=0 }; };
template<typename T, int _UpLo> struct is_cholesky_solver<LLT<T, _UpLo> > { enum { value = true, UpLo=_UpLo }; };

} // end namespace dco_eigen
} // end namespace Eigen_AD
} // end namespace Eigen

#endif // DCO_EIGEN_META_H

