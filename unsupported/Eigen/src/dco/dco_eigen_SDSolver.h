// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef DCO_EIGEN_SDSOLVER_H
#define DCO_EIGEN_SDSOLVER_H

/******************************************************************************
 *
 * Internals
 *
 *****************************************************************************/
namespace Eigen {
namespace Eigen_AD {
namespace dco_eigen {

  //
  // Helper structs
  //

  // A container struct used for a 2 parameter external dco function
  template<typename MatrixType>
  struct SVDData {
    const MatrixType* matrix;
    unsigned int computationOptions;

    SVDData(const MatrixType& matrix_, const unsigned int& computationOptions_) : matrix(&matrix_), computationOptions(computationOptions_) {}
  };

  template<typename T> struct is_Ref_impl { enum { value=false }; };
  template<typename T> struct is_Ref_impl<Ref<T> > { enum { value=true }; };
  template<typename T> struct is_Ref { enum { value=is_Ref_impl<typename Eigen::internal::remove_all<T>::type>::value }; };

  template<typename T> struct is_HouseholderQR_impl { enum { value=false }; };
  template<typename T> struct is_HouseholderQR_impl<HouseholderQR<T> > { enum { value=true }; };
  template<typename T> struct is_HouseholderQR { enum { value=is_HouseholderQR_impl<typename Eigen::internal::remove_all<T>::type>::value }; };

  template<typename T> struct is_COD_impl { enum { value=false }; };
  template<typename T> struct is_COD_impl<CompleteOrthogonalDecomposition<T> > { enum { value=true }; };
  template<typename T> struct is_COD { enum { value=is_COD_impl<typename Eigen::internal::remove_all<T>::type>::value }; };

  template <typename InverseType, typename Solver>
  static typename std::enable_if<!is_COD<Solver>::value>::type get_inverse(InverseType& inverse, Solver& solver) {
    inverse = solver.inverse();
  }

  // No inverse() for COD. Call pseudoInverse().
  template <typename InverseType, typename Solver>
  static typename std::enable_if<is_COD<Solver>::value>::type get_inverse(InverseType& inverse, Solver& solver) {
    inverse = solver.pseudoInverse();
  }
} // end namespace dco
} // end namespace Eigen_AD
} // end namespace Eigen

//
// dco callbacks
//
namespace dco {
namespace helper {
  using namespace Eigen::Eigen_AD::dco_eigen;

  template<typename DCO_MODE, typename _Solver, typename _MatrixType>
  class dco_eigen_external_adjoint_object_decomposition : public DCO_MODE::callback_object_t {
    public:
      typedef _MatrixType MatrixType;
      typedef _Solver Solver;
      Matrix<DCO_TAPE_INT, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> A_adj_idx;
      Solver* refSolverObject;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF_VECTORIZABLE_FIXED_SIZE(DCO_TAPE_INT, MatrixType::RowsAtCompileTime * MatrixType::ColsAtCompileTime )

      template<typename InputType>
      dco_eigen_external_adjoint_object_decomposition(const EigenBase<InputType>& matrix) : A_adj_idx(matrix.derived().rows(), matrix.derived().cols()) {
        refSolverObject = new Solver(matrix.derived());
      }

      template<typename InputType>
      dco_eigen_external_adjoint_object_decomposition(const SVDData<InputType>& data) : A_adj_idx(data.matrix->rows(), data.matrix->cols()) {
        refSolverObject = new Solver(*(data.matrix), data.computationOptions);
      }

      ~dco_eigen_external_adjoint_object_decomposition() { delete refSolverObject; }

      template<typename InputType>
      void register_A(const EigenBase<InputType>& matrix) {
        for(int i=0; i<matrix.rows(); i++) {
          for(int j=0; j<matrix.cols(); j++) {
            A_adj_idx(i, j) = matrix.derived()(i,j)._tape_index();
          }
        }
      }

      // Also supports ga1v as Scalar
      void increment_adjoints_of_A(const Matrix<typename DCO_MODE::derivative_t, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> &adj) {
        for(int i=0; i<adj.rows(); i++) {
          for(int j=0; j<adj.cols(); j++) {
            this->get_tape()->_adjoint(A_adj_idx(i, j)) += adj(i, j);
          }
        }
      }

      virtual mem_long_t size_in_byte() {
        mem_long_t S = DCO_MODE::callback_object_t::size_in_byte();
        S += A_adj_idx.size() * sizeof(DCO_TAPE_INT) + sizeof(Solver) + sizeof(Solver*); // Dynamic memory of solver not taken into account
        return S;
      }
  };

  template<typename DCO_MODE, typename DstType, typename DecompEAOType>
  class dco_eigen_external_adjoint_object_solve: public DCO_MODE::callback_object_t {
    public:
      Matrix<typename DCO_MODE::value_t, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> x;
      Matrix<DCO_TAPE_INT, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> rhs_adj_idx;
      Matrix<DCO_TAPE_INT, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> x_adj_idx;
      DecompEAOType* decomp_callback;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF_VECTORIZABLE_FIXED_SIZE(typename DCO_MODE::value_t, DstType::RowsAtCompileTime * DstType::ColsAtCompileTime)

      dco_eigen_external_adjoint_object_solve(const DstType& dst) : x(dst.rows(), dst.cols()), rhs_adj_idx(dst.rows(), dst.cols()), x_adj_idx(dst.rows(), dst.cols()), decomp_callback(nullptr) { }

      //
      // Register input/output
      //
      template<typename RhsType>
      void register_rhs(const RhsType& rhs) {
        for(int i=0; i<rhs.rows(); i++) {
          for(int j=0; j<rhs.cols(); j++) {
            rhs_adj_idx(i, j) = rhs(i,j)._tape_index();
          }
        }
      }

      void register_x(DstType& x) {
        for(int i=0; i<x.rows(); i++) {
          for(int j=0; j<x.cols(); j++) {
            this->x(i, j) = dco::value(x(i, j));

            typename DCO_MODE::type tmp;
            tmp = dco::value(x(i,j));
            this->get_tape()->register_variable(tmp);
            x_adj_idx(i, j) = tmp._tape_index();
            x(i,j) = tmp;
          }
        }
      }

      //
      // Fill gap routines
      //

      // ga1s
      // rhs and x can be vectors or matrices
      template<bool Transposed, typename RhsType>
      typename std::enable_if<!::Eigen::Eigen_AD::dco_eigen::is_dco_vector_ad_type<typename RhsType::Scalar>::value>::type solve_impl_fill_gap() {
        Matrix<typename DCO_MODE::value_t, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> x_a1(x.rows(), x.cols());
        Matrix<typename DCO_MODE::value_t, RhsType::RowsAtCompileTime, RhsType::ColsAtCompileTime> b_a1(x.rows(), x.cols());
        for(int i=0;i<x_a1.rows();i++)
          for(int j=0;j<x_a1.cols();j++)
            x_a1(i, j) = this->_adjoint_vector->_read_adjoint(x_adj_idx(i, j));

        // Solve A^{T} * b_a1 = x_a1 by using the saved decomposition for A applying the Eigen build-in transposed solve implementation
        if(!Transposed) {
          decomp_callback->refSolverObject->template _solve_impl_transposed<false>(x_a1, b_a1); // O(n^2)
        } else {
          decomp_callback->refSolverObject->template _solve_impl(x_a1, b_a1); // O(n^2)
        }

        // Compute and increment adjoints of A
        decomp_callback->increment_adjoints_of_A(compute_adjoints_of_A<Transposed>(b_a1));

        // Increment adjoints of b
        for(int i=0;i<b_a1.rows();i++)
          for(int j=0;j<b_a1.cols();j++)
            this->_adjoint_vector->_increment_adjoint(rhs_adj_idx(i, j), b_a1(i, j));
      }

      // ga1v version
      // Requires rhs and x to be a vector, not a matrix
      template<bool Transposed, typename RhsType>
      typename std::enable_if<::Eigen::Eigen_AD::dco_eigen::is_dco_vector_ad_type<typename RhsType::Scalar>::value>::type solve_impl_fill_gap() {
        eigen_assert(rhs_adj_idx.cols() == 1 && x_adj_idx.cols() == 1 && "dco/c++/eigen does not support solving for a matrix right hand side with ga1v type!");
        eigen_assert(!DCO_MODE::global_tape->scalar_callback_mode() && "scalar_callback_mode() is not disabled!");

        Matrix<typename DCO_MODE::value_t, DstType::RowsAtCompileTime, dco::helper::vecsize_info<typename dco::mode<typename RhsType::Scalar>::derivative_t>::vecsize> x_a1(x.rows(), dco::helper::vecsize_info<typename dco::mode<typename RhsType::Scalar>::derivative_t>::vecsize);
        Matrix<typename DCO_MODE::value_t, RhsType::RowsAtCompileTime, dco::helper::vecsize_info<typename dco::mode<typename RhsType::Scalar>::derivative_t>::vecsize> b_a1(x.rows(), dco::helper::vecsize_info<typename dco::mode<typename RhsType::Scalar>::derivative_t>::vecsize);

        // Get AD vector of output vector
        for(int i=0;i<x_a1.rows();i++) {
          auto& ad_vec = this->get_tape()->_adjoint(x_adj_idx(i, 0));
          for(int j=0;j<x_a1.cols();j++) {
            x_a1(i, j) = ad_vec[j];
          }
        }

        // Solve A^{T} * b_a1 = x_a1 by using the saved decomposition for A applying the Eigen build-in transposed solve implementation
        if(!Transposed) {
          decomp_callback->refSolverObject->template _solve_impl_transposed<false>(x_a1, b_a1); // O(n^2)
        } else {
          decomp_callback->refSolverObject->template _solve_impl(x_a1, b_a1); // O(n^2)
        }

        // Compute and increment adjoints of A
        decomp_callback->increment_adjoints_of_A(compute_adjoints_of_A_vector<Transposed>(b_a1));

        // Increment AD vector of input vector b
        for(int i=0;i<b_a1.rows();i++) {
          auto& ad_vec = this->get_tape()->_adjoint(rhs_adj_idx(i, 0));
          for(int j=0;j<b_a1.cols();j++) {
            ad_vec[j] += b_a1(i, j);
          }
        }
      }

      // Compute adjoints of A (includes special handling for Cholesky)
      template<bool Transposed, typename Rhs_Adj_Type>
      Matrix<typename DCO_MODE::derivative_t, DecompEAOType::MatrixType::RowsAtCompileTime, DecompEAOType::MatrixType::ColsAtCompileTime>
      compute_adjoints_of_A(const Rhs_Adj_Type& b_a1) {
        typedef Matrix<typename DCO_MODE::derivative_t, DecompEAOType::MatrixType::RowsAtCompileTime, DecompEAOType::MatrixType::ColsAtCompileTime> Return_Type;
        Return_Type adj_matrix = Return_Type::Zero(decomp_callback->A_adj_idx.rows(), decomp_callback->A_adj_idx.cols());

        // Default implementation
        if(!::Eigen::Eigen_AD::dco_eigen::is_cholesky_solver<typename DecompEAOType::Solver>::value) {
          // A_a1 = - b_a1 * x^{T}
          if(!Transposed) {
            for(int i=0; i<b_a1.rows(); i++) {
              for(int j=0; j<x.rows(); j++) {
                for(int k=0; k<b_a1.cols(); k++) {
                  adj_matrix(i, j) += - b_a1(i,k) * x(j,k);
                }
              }
            }
          } else {
            // Increment the adjoints in transposed fashion
            for(int j=0; j<x.rows(); j++) {
              for(int i=0; i<b_a1.rows(); i++) {
                for(int k=0; k<b_a1.cols(); k++) {
                  adj_matrix(i, j) += - b_a1(i,k) * x(j,k);
                }
              }
            }
          }

        // Special Cholesky implementation
        } else {
          if( (static_cast<int>(::Eigen::Eigen_AD::dco_eigen::is_cholesky_solver<typename DecompEAOType::Solver>::UpLo) == static_cast<int>(Lower)) ) {
            for(int i=0; i<b_a1.rows(); i++) {
              for(int j=0; j<x.rows(); j++) {
                for(int k=0; k<b_a1.cols(); k++) {
                  typename Return_Type::Scalar tmp = - b_a1(i,k) * x(j,k);
                  if(j < i) tmp += - b_a1(j,k) * x(i,k);
                  else if(j > i) tmp = 0.0;
                  adj_matrix(i, j) += tmp;
                }
              }
            }
          } else {
            for(int i=0; i<b_a1.rows(); i++) {
              for(int j=0; j<x.rows(); j++) {
                for(int k=0; k<b_a1.cols(); k++) {
                  typename Return_Type::Scalar tmp = - b_a1(i,k) * x(j,k);
                  if(j > i) tmp += - b_a1(j,k) * x(i,k);
                  else if(j < i) tmp = 0.0;
                  adj_matrix(i, j) += tmp;
                }
              }
            }
          }
        }

        return adj_matrix;
      }

      template<bool Transposed, typename Rhs_Adj_Type>
      Matrix<typename DCO_MODE::derivative_t, DecompEAOType::MatrixType::RowsAtCompileTime, DecompEAOType::MatrixType::ColsAtCompileTime>
      compute_adjoints_of_A_vector(const Rhs_Adj_Type& b_a1) {
        typedef Matrix<typename DCO_MODE::derivative_t, DecompEAOType::MatrixType::RowsAtCompileTime, DecompEAOType::MatrixType::ColsAtCompileTime> Return_Type;
        Return_Type adj_matrix = Return_Type::Zero(decomp_callback->A_adj_idx.rows(), decomp_callback->A_adj_idx.cols());

        eigen_assert(x.cols() == 1 && "compute_adjoints_of_A_vector does not support solving for a right hand side matrix using ga1v!");

        // Default implementation
        if(!::Eigen::Eigen_AD::dco_eigen::is_cholesky_solver<typename DecompEAOType::Solver>::value) {
          // A_a1 = - b_a1 * x^{T}
          if(!Transposed) {
            for(int i=0; i<b_a1.rows(); i++) {
              for(int j=0; j<x.rows(); j++) {
                for(int k=0; k<b_a1.cols(); k++) {
                  adj_matrix(i, j)[k] = - b_a1(i,k) * x(j,0);
                }
              }
            }
          } else {
            // Increment the adjoints in transposed fashion
            for(int j=0; j<x.rows(); j++) {
              for(int i=0; i<b_a1.rows(); i++) {
                for(int k=0; k<b_a1.cols(); k++) {
                  adj_matrix(i, j)[k] = - b_a1(i,k) * x(j,0);
                }
              }
            }
          }

        // Special Cholesky implementation
        } else {
          if( (static_cast<int>(::Eigen::Eigen_AD::dco_eigen::is_cholesky_solver<typename DecompEAOType::Solver>::UpLo) == static_cast<int>(Lower)) ) {
            for(int i=0; i<b_a1.rows(); i++) {
              for(int j=0; j<x.rows(); j++) {
                for(int k=0; k<b_a1.cols(); k++) {
                  typename DCO_MODE::value_t tmp = - b_a1(i,k) * x(j,0);
                  if(j < i) tmp += - b_a1(j,k) * x(i,0);
                  else if(j > i) tmp = 0.0;
                  adj_matrix(i, j)[k] = tmp;
                }
              }
            }
          } else {
            for(int i=0; i<b_a1.rows(); i++) {
              for(int j=0; j<x.rows(); j++) {
                for(int k=0; k<b_a1.cols(); k++) {
                  typename DCO_MODE::value_t tmp = - b_a1(i,k) * x(j,0);
                  if(j > i) tmp += - b_a1(j,k) * x(i,0);
                  else if(j < i) tmp = 0.0;
                  adj_matrix(i, j)[k] = tmp;
                }
              }
            }
          }
        }

        return adj_matrix;
      }

      virtual mem_long_t size_in_byte() {
        mem_long_t S = DCO_MODE::callback_object_t::size_in_byte();
        S += x.size() * sizeof(typename DCO_MODE::value_t) + sizeof(DecompEAOType*) + rhs_adj_idx.size() * sizeof(rhs_adj_idx) * 2;
        return S;
      }
  };

  template<typename DCO_MODE, typename DstType, typename DecompEAOType>
  class dco_eigen_external_adjoint_object_inverse: public DCO_MODE::callback_object_t {
    public:    
      typedef Matrix<typename DCO_MODE::value_t, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> MatrixType;
      MatrixType A_inv_trans;
      Matrix<DCO_TAPE_INT, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> C_adj_idx;
      DecompEAOType* decomp_callback;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF_VECTORIZABLE_FIXED_SIZE(typename MatrixType::Scalar, MatrixType::RowsAtCompileTime * MatrixType::ColsAtCompileTime)

      dco_eigen_external_adjoint_object_inverse(const DstType& dst) : A_inv_trans(dst.rows(), dst.cols()), C_adj_idx(dst.rows(), dst.cols()), decomp_callback(nullptr) { }

      //
      // Register output
      //
      void register_C(DstType& C) {
        for(int i=0; i<C.rows(); i++) {
          for(int j=0; j<C.cols(); j++) {
            typename DCO_MODE::type tmp;
            tmp = dco::value(C(i,j));
            this->get_tape()->register_variable(tmp);
            C_adj_idx(i, j) = tmp._tape_index();
            C(i,j) = tmp;
          }
        }
      }

      //
      // Fill gap routine
      //
      void inverse_fill_gap() {
        MatrixType A_inv_a1(A_inv_trans.rows(), A_inv_trans.cols());

        for(int i=0;i<A_inv_a1.rows();i++)
          for(int j=0;j<A_inv_a1.cols();j++)
            A_inv_a1(i, j) = this->_adjoint_vector->_read_adjoint(C_adj_idx(i, j));

        MatrixType A_a1 = -A_inv_trans * A_inv_a1 * A_inv_trans;

        decomp_callback->increment_adjoints_of_A(A_a1);
      }

      virtual mem_long_t size_in_byte() {
        mem_long_t S = DCO_MODE::callback_object_t::size_in_byte();
        S += A_inv_trans.size() * sizeof(typename MatrixType::Scalar) + sizeof(DecompEAOType*) + C_adj_idx.size() * sizeof(DCO_TAPE_INT);
        return S;
      }
  };


  template<typename DCO_MODE, typename DecompEAOType>
  class dco_eigen_external_adjoint_object_logabsdeterminant: public DCO_MODE::callback_object_t {
  public:
      typedef typename DecompEAOType::Solver Solver;

      DCO_TAPE_INT x_adj_idx;
      DecompEAOType* decomp_callback;

      dco_eigen_external_adjoint_object_logabsdeterminant() : decomp_callback(nullptr) { }

      //
      // Register output
      //
      void register_x(typename DCO_MODE::type& x) {
        typename DCO_MODE::type tmp;
        tmp = dco::value(x);
        this->get_tape()->register_variable(tmp);
        x_adj_idx = tmp._tape_index();
        x = tmp;
      }

      //
      // Fill gap routine
      //
      template <typename T = Solver>
      void logdeterminant_fill_gap() {
        typename DCO_MODE::value_t x_a1 = this->_adjoint_vector->_read_adjoint(x_adj_idx);

        typename Solver::MatrixType A_a1(decomp_callback->A_adj_idx.rows(), decomp_callback->A_adj_idx.cols());
        get_inverse(A_a1);

        decomp_callback->increment_adjoints_of_A(A_a1 * x_a1);
      }

      template <typename T = Solver>
      typename std::enable_if<!is_HouseholderQR<T>::value && !is_COD<T>::value>::type get_inverse(typename Solver::MatrixType& inverse) {
        inverse = decomp_callback->refSolverObject->inverse();
      }

      // No inverse() for HouseholderQR. We could perform another decomposition, but we no longer save the input matrix! Throw error.
      template <typename T = Solver>
      typename std::enable_if<is_HouseholderQR<T>::value>::type get_inverse(typename Solver::MatrixType&) {
        eigen_assert(false && "Can't compute an inverse using HouseholderQR! Use a different solver.");
      }

      // No inverse() for COD. Call pseudoInverse().
      template <typename T = Solver>
      typename std::enable_if<is_COD<T>::value>::type get_inverse(typename Solver::MatrixType& inverse) {
        inverse = decomp_callback->refSolverObject->pseudoInverse();
      }

      virtual mem_long_t size_in_byte() {
        mem_long_t S = DCO_MODE::callback_object_t::size_in_byte();
        S += sizeof(DecompEAOType*) + sizeof(DCO_TAPE_INT);
        return S;
      }
  };

  template <typename EAO, bool Transposed, typename Rhs>
  void solve_impl_callback(EAO* D) {
    D->template solve_impl_fill_gap<Transposed, Rhs>();
  }

  template <typename EAO>
  void inverse_callback(EAO* D) {
    D->inverse_fill_gap();
  }

  template <typename EAO>
  void logdeterminant_callback(EAO* D) {
    D->logdeterminant_fill_gap();
  }
} // end namespace helper
} // end namespace dcp

/******************************************************************************
 *
 * Eigen-AD Dense Solver API
 *
 *****************************************************************************/
namespace Eigen {
namespace Eigen_AD {

template<typename _MatrixType>
struct solver_enable<_MatrixType,
                              typename std::enable_if<(!dco_eigen::is_Ref<_MatrixType>::value && dco::mode<typename _MatrixType::Scalar>::is_adjoint_type)>::type> {
  enum { value = true };
};

template<typename _MatrixType, typename _ValueSolverType, typename _WrapperSolverType>
struct solver<_MatrixType, _ValueSolverType, _WrapperSolverType,
                       typename std::enable_if<(dco::mode<typename _MatrixType::Scalar>::is_adjoint_type)>::type>
: public solver_container<_WrapperSolverType> {
  typedef solver_container<_WrapperSolverType> Base;
  typedef _MatrixType                                   MatrixType;
  typedef _ValueSolverType                              ValueSolverType;
  typedef typename _ValueSolverType::MatrixType         ValueMatrixType;

  typedef typename dco::mode<typename MatrixType::Scalar> DCO_MODE;
  typedef typename dco::helper::dco_eigen_external_adjoint_object_decomposition<DCO_MODE, ValueSolverType, MatrixType> DCO_EAO_TYPE_DECOMP;

  DCO_EAO_TYPE_DECOMP* m_last_decomp_callback;    // Pointer to callback object of the current decomposition
  ValueSolverType* m_value_solver;                // Only used if we are passive (ie no active tape present)

  solver() : m_last_decomp_callback(nullptr), m_value_solver(nullptr) { }
  solver(const solver& copy) : Base(copy), m_last_decomp_callback(copy.m_last_decomp_callback), m_value_solver(nullptr) { if(copy.m_value_solver) m_value_solver = new ValueSolverType(*copy.m_value_solver); }
  ~solver() { delete m_value_solver; }

  ValueSolverType& getSolver() const {
    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      eigen_assert(m_value_solver && "SDSolver is not initialized.");
      return *(m_value_solver);
    } else {
      eigen_assert(m_last_decomp_callback && "SDSolver is not initialized.");
      return *(m_last_decomp_callback->refSolverObject);
    }
  }

  // This function can be called with InputType equal to MatrixType or some expression (e.g. CWiseBinaryOp) where eval() returns the MatrixType object
  // We dont want to call reinterpret_cast on an expression but instead always on the object returned by eval()
  // matrix.derived() returns the InputType object
  // If called on a Matrix, eval() returns a const reference to said Matrix; if called on an expression it returns a rvalue to the Matrix
  // We can only call reinterpret_cast on a lvalue!
  // Therefore we bind the rvalue (if present) to a const lvalue reference
  template<typename InputType>
  void compute(const EigenBase<InputType>& matrix) {
    Base::compute(matrix);
    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      if(!m_value_solver) {
        m_value_solver = new ValueSolverType;
      }
      ValueMatrixType valueMatrix(matrix.derived().rows(), matrix.derived().cols());
      dco_eigen::extractMatrixValues(matrix.derived(), valueMatrix);
      m_value_solver->compute(valueMatrix);
    } else {
      typedef typename dco::gbcp<typename InputType::Scalar>::type Scalar_gbcp;
      typedef Matrix<Scalar_gbcp, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> Matrix_gbcp;
      const typename InputType::EvalReturnType &tmp = matrix.derived().eval();
      Matrix_gbcp const &p_matrix = reinterpret_cast<Matrix_gbcp const&>(tmp);

      m_last_decomp_callback = DCO_MODE::global_tape->template create_callback_object<DCO_EAO_TYPE_DECOMP>(p_matrix);
      m_last_decomp_callback->register_A(matrix.derived());
    }
  }

  // Used for SVD solvers, no computationOptions given. If a solver object already exists, we want to reuse its computationOptions.
  template<typename InputType>
  void compute_svd(const EigenBase<InputType>& matrix) {
    Base::compute_svd(matrix);
    unsigned int computationOptions = 0;
    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      if(!m_value_solver) {
        m_value_solver = new ValueSolverType;
      } else {
        computationOptions = m_value_solver->computationOptions();
      }
      ValueMatrixType valueMatrix(matrix.derived().rows(), matrix.derived().cols());
      dco_eigen::extractMatrixValues(matrix.derived(), valueMatrix);
      m_value_solver->compute(valueMatrix, computationOptions);
    } else {
      if(m_last_decomp_callback) {
        computationOptions = m_last_decomp_callback->refSolverObject->computationOptions();
      }
      typedef typename dco::gbcp<typename InputType::Scalar>::type Scalar_gbcp;
      typedef Matrix<Scalar_gbcp, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> Matrix_gbcp;
      const typename InputType::EvalReturnType &tmp = matrix.derived().eval();
      Matrix_gbcp const &p_matrix = reinterpret_cast<Matrix_gbcp const&>(tmp);

      dco_eigen::SVDData<Matrix_gbcp> data(p_matrix, computationOptions);
      m_last_decomp_callback = DCO_MODE::global_tape->template create_callback_object<DCO_EAO_TYPE_DECOMP>(data);
      m_last_decomp_callback->register_A(matrix.derived());
    }
  }

  // Used for SVD solvers with new computationOptions
  template<typename InputType>
  void compute_svd(const EigenBase<InputType>& matrix, unsigned int computationOptions) {
    Base::compute_svd(matrix, computationOptions);
    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      if(!m_value_solver) {
        m_value_solver = new ValueSolverType;
      }
      ValueMatrixType valueMatrix(matrix.derived().rows(), matrix.derived().cols());
      dco_eigen::extractMatrixValues(matrix.derived(), valueMatrix);
      m_value_solver->compute(valueMatrix, computationOptions);
    } else {
      typedef typename dco::gbcp<typename InputType::Scalar>::type Scalar_gbcp;
      typedef Matrix<Scalar_gbcp, MatrixType::RowsAtCompileTime, MatrixType::ColsAtCompileTime> Matrix_gbcp;
      const typename InputType::EvalReturnType &tmp = matrix.derived().eval();
      Matrix_gbcp const &p_matrix = reinterpret_cast<Matrix_gbcp const&>(tmp);

      dco_eigen::SVDData<Matrix_gbcp> data(p_matrix, computationOptions);
      m_last_decomp_callback = DCO_MODE::global_tape->template create_callback_object<DCO_EAO_TYPE_DECOMP>(data);
      m_last_decomp_callback->register_A(matrix.derived());
    }
  }

  template<typename RhsType, typename DstType>
  void solve(const RhsType &rhs, DstType &dst) const {
    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      eigen_assert(m_value_solver && "Passively run SDSolver has not been initialized!");
      typedef Matrix<typename dco::mode<typename RhsType::Scalar>::value_t, RhsType::RowsAtCompileTime, RhsType::ColsAtCompileTime> RhsValueType;
      typedef Matrix<typename dco::mode<typename DstType::Scalar>::value_t, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> DstValueType;
      RhsValueType valueRhs(rhs.rows(), rhs.cols());
      DstValueType valueDst(dst.rows(), dst.cols());
      dco_eigen::extractMatrixValues(rhs, valueRhs);
      m_value_solver->_solve_impl(valueRhs, valueDst);
      dco_eigen::applyMatrixValues(dst, valueDst);
    } else {
      typedef dco::helper::dco_eigen_external_adjoint_object_solve<DCO_MODE, DstType, DCO_EAO_TYPE_DECOMP> DCO_EAO_TYPE;
      DCO_EAO_TYPE* eao = DCO_MODE::global_tape->template create_callback_object<DCO_EAO_TYPE>(dst);
      eao->decomp_callback = m_last_decomp_callback;

      eao->register_rhs(rhs);
      const typename RhsType::EvalReturnType &tmp_rhs = rhs.eval();

// We need to disable gbcp type for the "light" version, as some more changes to the Eigen Householder source are required (QR solvers)
#ifndef DCO_EIGEN_NO_GBCP_SOLVE
      // gbcp for the right hand side
      typedef typename dco::gbcp<typename RhsType::Scalar>::type Rhs_Scalar_gbcp;
      typedef Matrix<Rhs_Scalar_gbcp, RhsType::RowsAtCompileTime, RhsType::ColsAtCompileTime> Rhs_Matrix_gbcp;
      Rhs_Matrix_gbcp const &p_rhs = reinterpret_cast<Rhs_Matrix_gbcp const&>(tmp_rhs);

      // gbcp for the destination
      typedef typename dco::gbcp<typename DstType::Scalar>::type Dst_Scalar_gbcp;
      typedef Matrix<Dst_Scalar_gbcp, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> Dst_Matrix_gbcp;
      Dst_Matrix_gbcp &p_dst = reinterpret_cast<Dst_Matrix_gbcp&>(dst);

      m_last_decomp_callback->refSolverObject->_solve_impl(p_rhs, p_dst);
#else
      // value for the right hand side
      typedef Matrix<typename DCO_MODE::value_t, RhsType::RowsAtCompileTime, RhsType::ColsAtCompileTime> Rhs_Matrix_value;
      Rhs_Matrix_value p_rhs;
      dco_eigen::extractMatrixValues(tmp_rhs, p_rhs);

      m_last_decomp_callback->refSolverObject->_solve_impl(p_rhs, eao->x);

      dco_eigen::applyMatrixValues(dst, eao->x);
#endif

      eao->register_x(dst);

      DCO_MODE::global_tape->scalar_callback_mode() = false;
      DCO_MODE::global_tape->template insert_callback<DCO_EAO_TYPE>(dco::helper::solve_impl_callback<DCO_EAO_TYPE, false, RhsType>,eao);
    }
  }

  template<bool Conjugate, typename RhsType, typename DstType>
  void solve_transposed(const RhsType &rhs, DstType &dst) const {
    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      eigen_assert(m_value_solver && "Passively run SDSolver has not been initialized passively!");
      typedef Matrix<typename dco::mode<typename RhsType::Scalar>::value_t, RhsType::RowsAtCompileTime, RhsType::ColsAtCompileTime> RhsValueType;
      typedef Matrix<typename dco::mode<typename DstType::Scalar>::value_t, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> DstValueType;
      RhsValueType valueRhs(rhs.rows(), rhs.cols());
      DstValueType valueDst(dst.rows(), dst.cols());
      dco_eigen::extractMatrixValues(rhs, valueRhs);
      m_value_solver->template _solve_impl_transposed<Conjugate>(valueRhs, valueDst);
      dco_eigen::applyMatrixValues(dst, valueDst);
    } else {
      typedef dco::helper::dco_eigen_external_adjoint_object_solve<DCO_MODE, DstType, DCO_EAO_TYPE_DECOMP> DCO_EAO_TYPE;
      DCO_EAO_TYPE* eao = DCO_MODE::global_tape->template create_callback_object<DCO_EAO_TYPE>(dst);
      eao->decomp_callback = m_last_decomp_callback;

      eao->register_rhs(rhs);
      const typename RhsType::EvalReturnType &tmp_rhs = rhs.eval();

// We need to disable gbcp type for the "light" version, as some more changes to the Eigen Householder source are required (QR solvers)
#ifndef DCO_EIGEN_NO_GBCP_SOLVE
      // gbcp for the right hand side
      typedef typename dco::gbcp<typename RhsType::Scalar>::type Rhs_Scalar_gbcp;
      typedef Matrix<Rhs_Scalar_gbcp, RhsType::RowsAtCompileTime, RhsType::ColsAtCompileTime> Rhs_Matrix_gbcp;
      Rhs_Matrix_gbcp const &p_rhs = reinterpret_cast<Rhs_Matrix_gbcp const&>(tmp_rhs);

      // gbcp for the destination
      typedef typename dco::gbcp<typename DstType::Scalar>::type Dst_Scalar_gbcp;
      typedef Matrix<Dst_Scalar_gbcp, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> Dst_Matrix_gbcp;
      Dst_Matrix_gbcp &p_dst = reinterpret_cast<Dst_Matrix_gbcp&>(dst);

      m_last_decomp_callback->refSolverObject->template _solve_impl_transposed<Conjugate>(p_rhs, p_dst);
#else
      // value for the right hand side
      typedef Matrix<typename DCO_MODE::value_t, RhsType::RowsAtCompileTime, RhsType::ColsAtCompileTime> Rhs_Matrix_value;
      Rhs_Matrix_value p_rhs;
      dco_eigen::extractMatrixValues(tmp_rhs, p_rhs);

      m_last_decomp_callback->refSolverObject->template _solve_impl_transposed<Conjugate>(p_rhs, eao->x);

      dco_eigen::applyMatrixValues(dst, eao->x);
#endif

      eao->register_x(dst);

      DCO_MODE::global_tape->scalar_callback_mode() = false;
      DCO_MODE::global_tape->template insert_callback<DCO_EAO_TYPE>(dco::helper::solve_impl_callback<DCO_EAO_TYPE, true, RhsType>,eao);
    }
  }

  template<typename DstType>
  void inverse(DstType& dst) const {
    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      eigen_assert(m_value_solver && "Passively run SDSolver has not been initialized!");
      typedef Matrix<typename dco::mode<typename DstType::Scalar>::value_t, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> DstValueType;
      DstValueType valueDst(dst.rows(), dst.cols());
      dco_eigen::get_inverse(valueDst, *m_value_solver);
      dco_eigen::applyMatrixValues(dst, valueDst);
    } else {
      typedef dco::helper::dco_eigen_external_adjoint_object_inverse<DCO_MODE, DstType, DCO_EAO_TYPE_DECOMP> DCO_EAO_TYPE;
      DCO_EAO_TYPE* eao = DCO_MODE::global_tape->template create_callback_object<DCO_EAO_TYPE>(dst);
      eao->decomp_callback = m_last_decomp_callback;

      // gbcp for the destination
      typedef typename dco::gbcp<typename DstType::Scalar>::type Dst_Scalar_gbcp;
      typedef Matrix<Dst_Scalar_gbcp, DstType::RowsAtCompileTime, DstType::ColsAtCompileTime> Dst_Matrix_gbcp;
      Dst_Matrix_gbcp &p_dst = reinterpret_cast<Dst_Matrix_gbcp&>(dst);

      // Compute inverse
      dco_eigen::get_inverse(p_dst, *m_last_decomp_callback->refSolverObject);

      // Save inverse for later use
      for(int i=0;i<dst.rows();i++) {
        for(int j=0;j<dst.cols();j++) {
          eao->A_inv_trans(i, j) = dco::value(dst(j, i));
        }
      }

      // Register output
      eao->register_C(dst);

      DCO_MODE::global_tape->template insert_callback<DCO_EAO_TYPE>(dco::helper::inverse_callback<DCO_EAO_TYPE>,eao);
    }
  }

  typename MatrixType::RealScalar logAbsDeterminant() const {
    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      eigen_assert(m_value_solver && "Passively run SDSolver has not been initialized!");
      return m_value_solver->logAbsDeterminant();
    } else {
      typedef dco::helper::dco_eigen_external_adjoint_object_logabsdeterminant<DCO_MODE, DCO_EAO_TYPE_DECOMP> DCO_EAO_TYPE;
      DCO_EAO_TYPE* eao = DCO_MODE::global_tape->template create_callback_object<DCO_EAO_TYPE>();
      eao->decomp_callback = m_last_decomp_callback;

      typename MatrixType::RealScalar x = m_last_decomp_callback->refSolverObject->logAbsDeterminant();
      eao->register_x(x);

      DCO_MODE::global_tape->template insert_callback<DCO_EAO_TYPE>(dco::helper::logdeterminant_callback<DCO_EAO_TYPE>,eao);

      return x;
    }
  }

};

} // end namespace Eigen_AD
} // end namespace Eigen

#endif // DCO_EIGEN_SDSOLVER_H
