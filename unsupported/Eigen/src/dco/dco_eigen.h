// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef DCO_EIGEN_H
#define DCO_EIGEN_H

namespace Eigen {
namespace Eigen_AD {
/******************************************************************************
 *
 * Eigen-AD API
 *
 *****************************************************************************/
// Indicate that T is an AD type
template<typename T>
struct is_ad_type<T, typename std::enable_if<dco_eigen::is_dco_type<T>::value>::type> {
  enum { value = true };
};

// Define the value type (= primal type in case of first order) of T
template<typename T>
struct value_type<T, typename std::enable_if<dco_eigen::is_dco_type<T>::value>::type> {
  typedef typename dco::mode<T>::value_t type;
};

// Define the active type of T (in case of intermediates)
template<typename T>
struct active_type<T, typename std::enable_if<dco_eigen::is_dco_intermediate_type<T>::value>::type> {
  typedef typename dco::mode<T>::type type;
};

// Enable auto return type for dco types by default
template<typename T>
struct enable_auto_return_type<T, typename std::enable_if<dco_eigen::is_dco_type<T>::value>::type> {
  enum { value = true };
};

// Allow a binary op between a dco active, dco intermediate, dco gbcp type and all complex<> versions among themselves or with a regular passive type and specify the return type.
// Hit when
//     T or U are a dco type, so either an active type, a gbcp type or an intermediate or T or U are a complex<dco type>
// and their passive types are the same (=they are compatible) (same applies for complex!)
//
// Further explanation for passive types must be same:
// Otherwise causes ambiguousity when calculating A*A between Matrix * Matrix operator, in GeneralProduct.h, which takes a MatrixBase<T> as template argument and returns a Product<> expression
// and with the macro generated scalar operator * (EIGEN_MAKE_SCALAR_BINARY_OP in Macros.h), which takes the templated argument T and returns CwiseBinaryOp<internal::scalar_product_op<Scalar,T>,Derived,Constant<T> >
// So somewhere in A*A there is a scalar multiplication of a dco type and non dco type which do not have the same passive type and which should be handled by the native macro operator
// A template instantiation debugger would be awesome for this...
template <typename T, typename U, typename BinaryOp>
struct scalar_binary_return_type<T, U, BinaryOp,
                            typename std::enable_if<(dco_eigen::is_dco_type<typename internal::get_inner_type<T>::type, typename internal::get_inner_type<U>::type>::value
                                                     && std::is_same<typename dco::mode<typename Eigen::internal::remove_all<typename internal::get_inner_type<T>::type>::type>::passive_t,
                                                                     typename dco::mode<typename Eigen::internal::remove_all<typename internal::get_inner_type<U>::type>::type>::passive_t>::value
                                                    )> ::type > {
  // The following typedef gives the ReturnType of the operation
  // If one of the types is a dco intermediate return the other one
  // If atleast one has order > 0, return the type of the one with the higher order
  // If both are of the same order (0 or higher), return the gbcp type. If both or none are gbcp, return the lhs type.
  typedef typename Eigen::internal::conditional<dco_eigen::is_dco_intermediate_type<typename internal::get_inner_type<T>::type>::value, U,
            typename Eigen::internal::conditional<dco_eigen::is_dco_intermediate_type<typename internal::get_inner_type<U>::type>::value, T,
              // No intermediates from here
              typename Eigen::internal::conditional<(dco::mode<typename internal::get_inner_type<U>::type>::order > dco::mode<typename internal::get_inner_type<T>::type>::order), U,
                  typename Eigen::internal::conditional<(dco::mode<typename internal::get_inner_type<U>::type>::order < dco::mode<typename internal::get_inner_type<T>::type>::order), T,
                    // Order is same, make sure to return gbcp type if present
                    typename Eigen::internal::conditional<dco_eigen::is_dco_gbcp_type<typename internal::get_inner_type<U>::type>::value, U, T>::type
                  >::type
                >::type
              >::type
            >::type ReturnType;
};

// Convenience functions
namespace dco_eigen {

  template<typename ActiveT, typename ValueT>
  static void extractMatrixValues(const ActiveT& active, ValueT& value) {
    value.resize(active.rows(), active.cols());
    for(int i=0; i<value.rows(); i++) {
      for(int j=0; j<value.cols(); j++) {
        value(i,j) = dco::value(active(i,j));
      }
    }
  }

  template<typename ActiveT, typename ValueT>
  static void applyMatrixValues(ActiveT& active, const ValueT& value) {
    active.resize(value.rows(), value.cols());
    for(int i=0; i<value.rows(); i++) {
      for(int j=0; j<value.cols(); j++) {
        dco::value(active(i,j)) = value(i,j);
      }
    }
  }

} // end namespace dco
} // end namespace Eigen_AD

/******************************************************************************
 *
 * Internals
 *
 *****************************************************************************/
namespace internal {
  // Required for e.g. vectorwiseop_7_dco
  template<class DCO_TAPE_REAL, class DATA_HANDLER>
  struct is_arithmetic<typename dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER> > {
    enum { value = is_arithmetic<typename dco::internal::active_type<DCO_TAPE_REAL, DATA_HANDLER>::VALUE_TYPE>::value };
  };
} // end namespace internal

// Typedefs
#define EIGEN_MAKE_TYPEDEFS(Type, TypeSuffix, Size, SizeSuffix)   \
typedef Matrix<Type, Size, Size> Matrix##SizeSuffix##TypeSuffix;  \
typedef Matrix<Type, Size, 1>    Vector##SizeSuffix##TypeSuffix;  \
typedef Matrix<Type, 1, Size>    RowVector##SizeSuffix##TypeSuffix; \
typedef Array<Type, Size, Size>  Array##SizeSuffix##SizeSuffix##TypeSuffix;  \
typedef Array<Type, Size, 1>     Array##SizeSuffix##TypeSuffix; \
typedef AlignedBox<Type, Size>   AlignedBox##SizeSuffix##TypeSuffix;

#define EIGEN_MAKE_TYPEDEFS_DCO(Type, TypeSuffix)   \
typedef AlignedBox<Type, 1>          AlignedBox1##TypeSuffix; \
typedef Quaternion<Type>             Quaternion##TypeSuffix; \
typedef Transform<Type,2,Isometry>   Isometry2##TypeSuffix; \
typedef Transform<Type,2,Affine>     Affine2##TypeSuffix; \
typedef Transform<Type,2,Projective> Projective2##TypeSuffix; \
typedef Transform<Type,3,Isometry>   Isometry3##TypeSuffix; \
typedef Transform<Type,3,Affine>     Affine3##TypeSuffix; \
typedef Transform<Type,3,Projective> Projective3##TypeSuffix;

#define EIGEN_MAKE_FIXED_TYPEDEFS(Type, TypeSuffix, Size)         \
typedef Matrix<Type, Size, Dynamic> Matrix##Size##X##TypeSuffix;  \
typedef Matrix<Type, Dynamic, Size> Matrix##X##Size##TypeSuffix; \
typedef Array<Type, Size, Dynamic>  Array##Size##X##TypeSuffix;  \
typedef Array<Type, Dynamic, Size>  Array##X##Size##TypeSuffix;

#define EIGEN_MAKE_TYPEDEFS_ALL_SIZES(Type, TypeSuffix) \
EIGEN_MAKE_TYPEDEFS_DCO(Type, TypeSuffix) \
EIGEN_MAKE_TYPEDEFS(Type, TypeSuffix, 2, 2) \
EIGEN_MAKE_TYPEDEFS(Type, TypeSuffix, 3, 3) \
EIGEN_MAKE_TYPEDEFS(Type, TypeSuffix, 4, 4) \
EIGEN_MAKE_TYPEDEFS(Type, TypeSuffix, Dynamic, X) \
EIGEN_MAKE_FIXED_TYPEDEFS(Type, TypeSuffix, 2) \
EIGEN_MAKE_FIXED_TYPEDEFS(Type, TypeSuffix, 3) \
EIGEN_MAKE_FIXED_TYPEDEFS(Type, TypeSuffix, 4)

EIGEN_MAKE_TYPEDEFS_ALL_SIZES(dco::gt1s<float>::type,                            dco_t1f)
EIGEN_MAKE_TYPEDEFS_ALL_SIZES(dco::gt1s<double>::type,                           dco_t1d)
EIGEN_MAKE_TYPEDEFS_ALL_SIZES(std::complex<dco::gt1s<double>::type>,             dco_t1cd)
EIGEN_MAKE_TYPEDEFS_ALL_SIZES(dco::gt1s<typename dco::gt1s<double>::type>::type, dco_t2t1d)

EIGEN_MAKE_TYPEDEFS_ALL_SIZES(dco::ga1s<float>::type,                            dco_a1f)
EIGEN_MAKE_TYPEDEFS_ALL_SIZES(dco::ga1s<double>::type,                           dco_a1d)
EIGEN_MAKE_TYPEDEFS_ALL_SIZES(std::complex<dco::ga1s<double>::type>,             dco_a1cd)
EIGEN_MAKE_TYPEDEFS_ALL_SIZES(dco::ga1s<typename dco::gt1s<double>::type>::type, dco_t2a1d)

#undef EIGEN_MAKE_TYPEDEFS_ALL_SIZES
#undef EIGEN_MAKE_TYPEDEFS
#undef EIGEN_MAKE_TYPEDEFS_1
#undef EIGEN_MAKE_FIXED_TYPEDEFS
} // end namespace Eigen

#endif // DCO_EIGEN_H
