// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef DCO_EIGEN_PRODUCT_H
#define DCO_EIGEN_PRODUCT_H

/******************************************************************************
 *
 * Internals
 *
 *****************************************************************************/
namespace dco {
namespace helper {
  using namespace Eigen;

  template<typename DCO_MODE, typename DstMatrix, typename LhsMatrix, typename RhsMatrix>
  class dco_eigen_external_adjoint_object_product: public DCO_MODE::external_adjoint_object_t {
    public:
      LhsMatrix lhs;
      RhsMatrix rhs;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF( (bool(((LhsMatrix::RowsAtCompileTime * LhsMatrix::ColsAtCompileTime)!=Eigen::Dynamic) && ((sizeof(typename LhsMatrix::Scalar)*(LhsMatrix::RowsAtCompileTime * LhsMatrix::ColsAtCompileTime))%EIGEN_MAX_ALIGN_BYTES==0))) ||
                                          (bool(((RhsMatrix::RowsAtCompileTime * RhsMatrix::ColsAtCompileTime)!=Eigen::Dynamic) && ((sizeof(typename RhsMatrix::Scalar)*(RhsMatrix::RowsAtCompileTime * RhsMatrix::ColsAtCompileTime))%EIGEN_MAX_ALIGN_BYTES==0))))

      void product_fill_gap() {
          DstMatrix dst_a1(lhs.rows(), rhs.cols());
          LhsMatrix lhs_a1(lhs.rows(), lhs.cols());
          RhsMatrix rhs_a1(rhs.rows(), rhs.cols());

          for(int i=0; i<dst_a1.rows(); i++) {
              for(int j=0; j<dst_a1.cols(); j++) {
                  dst_a1(i, j) = this->get_output_adjoint();
              }
          }

          lhs_a1 = dst_a1 * rhs.transpose();
          rhs_a1 = lhs.transpose() * dst_a1;

          for(int i=0; i<lhs_a1.rows(); i++) {
              for(int j=0; j<lhs_a1.cols(); j++) {
                  this->increment_input_adjoint(lhs_a1(i, j));
              }
          }
          for(int i=0; i<rhs_a1.rows(); i++) {
              for(int j=0; j<rhs_a1.cols(); j++) {
                  this->increment_input_adjoint(rhs_a1(i, j));
              }
          }
      }

      virtual mem_long_t size_in_byte() {
        mem_long_t S = DCO_MODE::external_adjoint_object_t::size_in_byte();
        S += lhs.size() * sizeof(typename LhsMatrix::Scalar) + rhs.size() * sizeof(typename RhsMatrix::Scalar);
        return S;
      }
  };

  template<typename DCO_EAO_PRODUCT>
  void product_callback(DCO_EAO_PRODUCT* D) {
      D->product_fill_gap();
  }

} // end namespace helper
} // end namespace dco


/******************************************************************************
 *
 * Eigen-AD Product API
 *
 *****************************************************************************/
namespace Eigen {
namespace Eigen_AD {

template<typename Scalar, int M, int N, int Depth, typename Lhs, typename Rhs>
struct product_enable<Scalar, M, N, Depth, Lhs, Rhs,
                               typename std::enable_if<( dco::mode<Scalar>::is_adjoint_type &&
                                                         M > 1 && N > 1 &&
                                                         std::is_same<typename Eigen::internal::traits<Lhs>::StorageKind, Dense>::value &&
                                                         std::is_same<typename Eigen::internal::traits<Rhs>::StorageKind, Dense>::value &&
                                                         ( !dco_eigen::is_dco_gbcp_type<Scalar>::value ||
                                                           ( dco_eigen::is_dco_gbcp_type<Scalar>::value &&
                                                             dco::mode<Scalar>::order > 0
                                                           )
                                                         )
                                                       )>::type> {
  enum { value = true };
};

template <typename Scalar, typename Lhs, typename Rhs, typename LhsEval, typename RhsEval, typename LhsValue, typename RhsValue, typename DstValue>
struct product {

  template<typename Dst>
  static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs) {
    typedef dco::mode<typename dco::mode<Scalar>::active_t> DCO_MODE;

    if(!DCO_MODE::global_tape || !DCO_MODE::global_tape->is_active()) {
      Eigen::internal::generic_product_impl<Lhs, Rhs,
                           typename Eigen::internal::evaluator_traits<Lhs>::Shape, typename Eigen::internal::evaluator_traits<Rhs>::Shape,
                           Eigen::internal::product_type<LhsValue,RhsValue>::value>::evalTo(dst, lhs, rhs);
    } else {
      // Force evaluation of lhs and rhs into temporaries
      const LhsEval& lhsEvaluated = lhs.eval();
      const RhsEval& rhsEvaluated = rhs.eval();

      // Create External Adjoint Object
      typedef dco::helper::dco_eigen_external_adjoint_object_product<DCO_MODE, DstValue, LhsValue, RhsValue> DCO_EAO_PRODUCT;
      DCO_EAO_PRODUCT* product_eao = DCO_MODE::global_tape->template create_callback_object<DCO_EAO_PRODUCT>();
      product_eao->lhs.resize(lhs.rows(), lhs.cols());
      product_eao->rhs.resize(rhs.rows(), rhs.cols());

      // Register active inputs in EAO and save input values for later use
      for(int i=0;i<lhs.rows();i++) {
        for(int j=0;j<lhs.cols();j++) {
          product_eao->lhs(i, j) = product_eao->register_input(lhsEvaluated(i, j));
        }
      }
      for(int i=0;i<rhs.rows();i++) {
        for(int j=0;j<rhs.cols();j++) {
          product_eao->rhs(i, j) = product_eao->register_input(rhsEvaluated(i, j));
        }
      }

      // Actual evaluation
      if(DCO_MODE::order == 1) {
        DstValue dstValue(dst.rows(), dst.cols());
        Eigen::internal::generic_product_impl<LhsValue, RhsValue>::evalTo(dstValue, product_eao->lhs, product_eao->rhs);

        // Get output
        for(int i=0;i<dst.rows();i++) {
          for(int j=0;j<dst.cols();j++) {
            dst(i,j) = product_eao->register_output(dstValue(i, j));
          }
        }
      } else {
        // Define gbcp type for Lhs and Rhs
        typedef typename dco::gbcp<Scalar>::type GbcpScalar;
        typedef Matrix<GbcpScalar, LhsEval::RowsAtCompileTime, LhsEval::ColsAtCompileTime, LhsEval::Options, LhsEval::MaxRowsAtCompileTime, LhsEval::MaxColsAtCompileTime> LhsGbcp;
        typedef Matrix<GbcpScalar, RhsEval::RowsAtCompileTime, RhsEval::ColsAtCompileTime, RhsEval::Options, RhsEval::MaxRowsAtCompileTime, RhsEval::MaxColsAtCompileTime> RhsGbcp;
        const LhsGbcp& lhsGbcp = reinterpret_cast<const LhsGbcp&>(lhsEvaluated);
        const RhsGbcp& rhsGbcp = reinterpret_cast<const RhsGbcp&>(rhsEvaluated);

        // Define gbcp type for Dst (this can be a Map<> type as well!)
        typedef Matrix<GbcpScalar, Dst::RowsAtCompileTime, Dst::ColsAtCompileTime> DstGbcp;
        typedef Stride<Eigen::internal::traits<Dst>::OuterStrideAtCompileTime, Eigen::internal::traits<Dst>::InnerStrideAtCompileTime> StrideType;
        Map<DstGbcp, Eigen::internal::traits<Dst>::Alignment, StrideType> DstMap(&reinterpret_cast<GbcpScalar&>(dst(0,0)), dst.rows(), dst.cols(), StrideType(dst.outerStride(), dst.innerStride()));

        Eigen::internal::generic_product_impl<LhsGbcp, RhsGbcp>::evalTo(DstMap, lhsGbcp, rhsGbcp);

        // Get output
        for(int i=0;i<dst.rows();i++) {
          for(int j=0;j<dst.cols();j++) {
            dst(i,j) = product_eao->register_output(DstMap(i, j));
          }
        }
      }

      // Insert callback
      DCO_MODE::global_tape->template insert_callback<DCO_EAO_PRODUCT>(dco::helper::product_callback<DCO_EAO_PRODUCT>,product_eao);
    }
  }
};

} // end namespace Eigen_AD
} // end namespace Eigen

#endif // DCO_EIGEN_PRODUCT_H
