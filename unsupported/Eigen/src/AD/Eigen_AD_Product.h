#ifndef EIGEN_AD_PRODUCT_H
#define EIGEN_AD_PRODUCT_H

#ifndef EIGEN_AD_NO_SYMBOLIC_PRODUCT
  #define EIGEN_AD_NO_SYMBOLIC_PRODUCT 0
#else
  #undef EIGEN_AD_NO_SYMBOLIC_PRODUCT
  #define EIGEN_AD_NO_SYMBOLIC_PRODUCT 1
#endif

namespace Eigen {
namespace Eigen_AD {

/******************************************************************************
 *
 * Eigen-AD Product API
 *
 * Specialize/define these structs for your AD-O tool module.
 *
 *****************************************************************************/
// If Scalar is your AD type, and you want to enable the symbolic product for
// dimensions M, N, Depth and for the types Lhs and Rhs (e.g. Dense Matrices),
// define an enum with 'value=true' in your specialization.
template<typename Scalar, int M, int N, int Depth, typename Lhs, typename Rhs, typename enable_if = void>
struct product_enable;

// Define the struct with the following function:
//
// template<typename Dst>
// static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs)
//
// and use it as the entry point for symbolic evaluation.
template <typename Scalar, typename Lhs, typename Rhs, typename LhsEval, typename RhsEval, typename LhsValue, typename RhsValue, typename DstValue>
struct product;

/******************************************************************************
 *
 * Internals
 *
 *****************************************************************************/
template<typename Scalar, int M, int N, int Depth, typename Lhs, typename Rhs, typename enable_if>
struct product_enable { enum { value = false }; };

} // end namespace Eigen_AD

namespace internal {
  enum ProductImplTypeDco
  { SymbolicProduct = 42 };

  // Forward to product_enable
  template<typename Scalar, int M, int N, int Depth, typename Lhs, typename Rhs>
  struct eigen_enable_custom_product<Scalar, M, N, Depth, Lhs, Rhs, typename std::enable_if<Eigen_AD::product_enable<Scalar, M, N, Depth, Lhs, Rhs>::value && !EIGEN_AD_NO_SYMBOLIC_PRODUCT>::type> {
    enum { value = true };
  };

  #define EIGEN_MAKE_PRODUCT_SELECTOR_AD(M, N, Depth)  \
    struct product_type_selector<M, N, Depth, Lhs, Rhs, Scalar, typename std::enable_if<Eigen_AD::product_enable<Scalar, M, N, Depth, Lhs, Rhs>::value && !EIGEN_AD_NO_SYMBOLIC_PRODUCT>::type > { \
      enum { ret = SymbolicProduct }; \
    };

  template<int M, int N, typename Lhs, typename Rhs, typename Scalar> EIGEN_MAKE_PRODUCT_SELECTOR_AD(M, N, 1)
  template<int M, typename Lhs, typename Rhs, typename Scalar>        EIGEN_MAKE_PRODUCT_SELECTOR_AD(M, 1, 1)
  template<int N, typename Lhs, typename Rhs, typename Scalar>        EIGEN_MAKE_PRODUCT_SELECTOR_AD(1, N, 1)
  template<int Depth,typename Lhs, typename Rhs, typename Scalar>     EIGEN_MAKE_PRODUCT_SELECTOR_AD(1, 1, Depth)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(1, 1, 1)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Small, 1, Small)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(1, Small, Small)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Small, Small, Small)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Small, Small, 1)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Small, Large, 1)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Large, Small, 1)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(1, Large, Small)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(1, Large, Large)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(1, Small, Large)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Large, 1, Small)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Large, 1, Large)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Small, 1, Large)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Small, Small, Large)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Large, Small, Large)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Small, Large, Large)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Large, Large, Large)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Large, Small, Small)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Small, Large, Small)
  template<typename Lhs, typename Rhs, typename Scalar>               EIGEN_MAKE_PRODUCT_SELECTOR_AD(Large, Large, Small)

  template<typename Lhs, typename Rhs>
  struct generic_product_impl<Lhs,Rhs,DenseShape,DenseShape,SymbolicProduct> {
    typedef typename Product<Lhs,Rhs>::Scalar Scalar;
    typedef typename remove_all<typename Lhs::EvalReturnType>::type LhsEval;
    typedef typename remove_all<typename Rhs::EvalReturnType>::type RhsEval;

    enum {
      LhsRows                 = LhsEval::RowsAtCompileTime,
      LhsCols                 = LhsEval::ColsAtCompileTime,
      LhsOptions              = LhsEval::Options,
      LhsMaxRowsAtCompileTime = LhsEval::MaxRowsAtCompileTime,
      LhsMaxColsAtCompileTime = LhsEval::MaxColsAtCompileTime,
      RhsRows                 = RhsEval::RowsAtCompileTime,
      RhsCols                 = RhsEval::ColsAtCompileTime,
      RhsOptions              = RhsEval::Options,
      RhsMaxRowsAtCompileTime = RhsEval::MaxRowsAtCompileTime,
      RhsMaxColsAtCompileTime = RhsEval::MaxColsAtCompileTime,
    };

    typedef typename Eigen_AD::value_type<Scalar>::type value_t;
    typedef Matrix<value_t, LhsRows, LhsCols, LhsOptions, LhsMaxRowsAtCompileTime, LhsMaxColsAtCompileTime> LhsValue;
    typedef Matrix<value_t, RhsRows, RhsCols, RhsOptions, RhsMaxRowsAtCompileTime, RhsMaxColsAtCompileTime> RhsValue;

    template<typename Dst>
    static EIGEN_STRONG_INLINE void evalTo(Dst& dst, const Lhs& lhs, const Rhs& rhs) {
      typedef Matrix<value_t, Dst::RowsAtCompileTime, Dst::ColsAtCompileTime> DstValue;
      Eigen_AD::product<Scalar, Lhs, Rhs, LhsEval, RhsEval, LhsValue, RhsValue, DstValue>::evalTo(dst, lhs, rhs);
    }

    template<typename Dst>
    static EIGEN_STRONG_INLINE void addTo(Dst& dst, const Lhs& lhs, const Rhs& rhs) {
      Matrix<typename Product<Lhs, Rhs>::Scalar, Lhs::RowsAtCompileTime, Rhs::ColsAtCompileTime> tmp(dst.rows(), dst.cols());
      evalTo(tmp, lhs, rhs);
      dst += tmp;
    }

    template<typename Dst>
    static EIGEN_STRONG_INLINE void subTo(Dst& dst, const Lhs& lhs, const Rhs& rhs) {
      Matrix<typename Product<Lhs, Rhs>::Scalar, Lhs::RowsAtCompileTime, Rhs::ColsAtCompileTime> tmp(dst.rows(), dst.cols());
      evalTo(tmp, lhs, rhs);
      dst -= tmp;
    }
  };

} // end namespace internal
} // end namespace Eigen

#endif // EIGEN_AD_PRODUCT_H
