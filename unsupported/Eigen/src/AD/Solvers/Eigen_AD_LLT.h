// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#if (defined EIGEN_LLT_H) || (defined EIGEN_AD_FORCE_INCLUDE_SOLVERS)
#ifndef EIGEN_AD_LLT_H
#define EIGEN_AD_LLT_H

#define SOLVER_INSTANTIATION_LLT_AD LLT<_MatrixType, _UpLo, EIGEN_IF_CUSTOM_SOLVER_AD(_MatrixType)>

namespace Eigen {

template<typename _MatrixType, int _UpLo>
class SOLVER_INSTANTIATION_LLT_AD
  : public Eigen_AD::SDSolver<_MatrixType,
                          LLT<_MatrixType, _UpLo> > {
  public:
    typedef _MatrixType MatrixType;
    typedef Eigen_AD::SDSolver<MatrixType, LLT<MatrixType, _UpLo> > Base;

    EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(LLT)

    // FIXME StorageIndex defined in EIGEN_GENERIC_PUBLIC_INTERFACE should be int
    enum {
      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
    };

    enum {
      PacketSize = Base::ValueSolverType::PacketSize,
      AlignmentMask = Base::ValueSolverType::AlignmentMask,
      UpLo = _UpLo
    };

    typedef internal::LLT_Traits<MatrixType,UpLo> Traits;

    //
    // Constructors
    //
    LLT() {}
    LLT(Index, Index) {}

    template<typename InputType>
    explicit LLT(const EigenBase<InputType>& matrix) {
      this->compute(matrix.derived());
    }

    //
    // Potential symbolic functions
    //
    inline typename Traits::MatrixU matrixU() const { return this->matrixU_sym(); }
    inline typename Traits::MatrixL matrixL() const { return this->matrixL_sym(); }
    inline const MatrixType matrixLLT() const { return this->matrixLLT_sym(); }
    template<typename VectorType>
    LLT rankUpdate(const VectorType& vec, const RealScalar& sigma = 1) { this->rankUpdate_sym(vec, sigma); return *this; }

    //
    // Other interface functions
    //
    inline ValueRealScalar rcond() const { return this->getSolver().rcond(); }
    ComputationInfo info() const { return this->getSolver().info(); }
    const LLT& adjoint() const { return *this; };
};

} // end namespace Eigen

#endif // EIGEN_AD_LLT_H
#endif // EIGEN_LLT_H
