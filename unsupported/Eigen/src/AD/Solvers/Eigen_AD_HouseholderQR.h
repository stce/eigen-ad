// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#if (defined EIGEN_QR_H) || (defined EIGEN_AD_FORCE_INCLUDE_SOLVERS)
#ifndef EIGEN_AD_HOUSEHOLDERQR_H
#define EIGEN_AD_HOUSEHOLDERQR_H

#define SOLVER_INSTANTIATION_HOUSEHOLDERQR_AD HouseholderQR<_MatrixType, EIGEN_IF_CUSTOM_SOLVER_AD(_MatrixType)>

namespace Eigen {

template<typename _MatrixType>
class SOLVER_INSTANTIATION_HOUSEHOLDERQR_AD
    : public Eigen_AD::SDSolver<_MatrixType,
                            HouseholderQR<_MatrixType> > {
  public:
    typedef _MatrixType MatrixType;
    typedef Eigen_AD::SDSolver<MatrixType, HouseholderQR<MatrixType> > Base;

    EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(HouseholderQR)

    // FIXME StorageIndex defined in EIGEN_GENERIC_PUBLIC_INTERFACE should be int
    enum {
      MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
    };
    typedef Matrix<Scalar, RowsAtCompileTime, RowsAtCompileTime, (MatrixType::Flags&RowMajorBit) ? RowMajor : ColMajor, MaxRowsAtCompileTime, MaxRowsAtCompileTime> MatrixQType;
    typedef typename internal::plain_diag_type<MatrixType>::type HCoeffsType;
    typedef typename internal::plain_row_type<MatrixType>::type RowVectorType;
    typedef HouseholderSequence<MatrixType,typename internal::remove_all<typename HCoeffsType::ConjugateReturnType>::type> HouseholderSequenceType;

    //
    // Constructors
    //
    HouseholderQR() {}
    HouseholderQR(Index, Index) {}

    template<typename InputType>
    explicit HouseholderQR(const EigenBase<InputType>& matrix) {
      this->compute(matrix.derived());
    }

    //
    // Potential symbolic functions
    //
    HouseholderSequenceType householderQ() const { return this->householderQ_sym(); }
    const MatrixType matrixQR() const            { return this->matrixQR_sym(); }
    const HCoeffsType& hCoeffs() const           { return this->hCoeffs_sym(); }
    RealScalar absDeterminant() const            { return this->absDeterminant_sym(); }
    RealScalar logAbsDeterminant() const         { return this->logAbsDeterminant_sym(); }

    //
    // Other interface functions
    //

};

} // end namespace Eigen

#endif // EIGEN_AD_HOUSEHOLDERQR_H
#endif // EIGEN_QR_H
