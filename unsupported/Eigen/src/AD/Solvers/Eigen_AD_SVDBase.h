// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_SVDBASE_H
#define EIGEN_AD_SVDBASE_H

namespace Eigen {

namespace internal {
  template<typename _MatrixType, typename _SolverType>
  struct traits<Eigen_AD::SymbolicSVDBase<_MatrixType, _SolverType> >
   : traits<_SolverType > {
    typedef MatrixXpr XprKind;
    typedef SolverStorage StorageKind;
    typedef int StorageIndex;
    enum { Flags = 0 };
  };
} // end namespace internal

namespace Eigen_AD {

template<typename _MatrixType, typename _SolverType> class SymbolicSVDBase
    : public SDSolver<_MatrixType, SymbolicSVDBase<_MatrixType, _SolverType> > {
public:
  typedef _MatrixType MatrixType;
  typedef _SolverType SolverType;
  typedef SDSolver<_MatrixType, SymbolicSVDBase<_MatrixType, _SolverType> > Base;
  template<typename Derived> friend struct Eigen::internal::solve_assertion;

  //EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(SymbolicSVDBase)
  typedef typename Base::ValueSolverType::MatrixType ValueMatrixType;
  typedef typename MatrixType::Scalar Scalar;
  typedef typename Base::ValueSolverType::Scalar ValueScalar;
  typedef typename NumTraits<typename MatrixType::Scalar>::Real RealScalar;
  typedef typename Base::ValueSolverType::RealScalar ValueRealScalar;
  typedef typename Eigen::internal::traits<SymbolicSVDBase>::StorageIndex StorageIndex;
  typedef typename Base::ValueSolverType::StorageIndex ValueStorageIndex;
  typedef Eigen::Index Index; ///< \deprecated since Eigen 3.3
  enum {
    RowsAtCompileTime = MatrixType::RowsAtCompileTime,
    ColsAtCompileTime = MatrixType::ColsAtCompileTime,
    DiagSizeAtCompileTime = Base::ValueSolverType::DiagSizeAtCompileTime,
    MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
    MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime,
    MaxDiagSizeAtCompileTime = Base::ValueSolverType::MaxDiagSizeAtCompileTime,
    MatrixOptions = MatrixType::Options
  };

  typedef Matrix<Scalar, RowsAtCompileTime, RowsAtCompileTime, MatrixOptions, MaxRowsAtCompileTime, MaxRowsAtCompileTime> MatrixUType;
  typedef Matrix<Scalar, ColsAtCompileTime, ColsAtCompileTime, MatrixOptions, MaxColsAtCompileTime, MaxColsAtCompileTime> MatrixVType;
  typedef typename Eigen::internal::plain_diag_type<MatrixType, RealScalar>::type SingularValuesType;

  SolverType& derived() { return *static_cast<SolverType*>(this); }
  const SolverType& derived() const { return *static_cast<const SolverType*>(this); }

  template<typename InputType>
  SolverType& compute(const EigenBase<InputType>& matrix) {
    this->container.compute_svd(matrix);
    return derived();
  }

  template<typename InputType>
  SolverType& compute(const EigenBase<InputType>& matrix, unsigned int computationOptions) {
    this->container.compute_svd(matrix, computationOptions);
    return derived();
  }

  //
  // Potential symbolic functions
  //
  const MatrixUType matrixU() const { return this->matrixU_sym(); }
  const MatrixVType matrixV() const { return this->matrixV_sym(); }
  const SingularValuesType singularValues() const { return this->singularValues_sym(); }

  //
  // Other interface functions
  //
  Index nonzeroSingularValues() const { return this->getSolver().nonzeroSingularValues(); }
  inline Index rank() const { return this->getSolver().rank(); }
  typename Base::SolverType& setThreshold(const RealScalar& threshold) { this->getSolver().setTreshold(threshold); return derived(); }
  typename Base::SolverType& setThreshold(Default_t) { this->getSolver().setTreshold(); return derived(); }
  ValueRealScalar threshold() const { return this->getSolver().threshold(); }
  inline bool computeU() const { return this->getSolver().computeU(); }
  inline bool computeV() const { return this->getSolver().computeV(); }
  inline unsigned int computationOptions() { return this->getSolver().computationOptions(); }

protected:
  template<bool Transpose_, typename Rhs>
  void _check_solve_assertion(const Rhs& b) const {
    EIGEN_ONLY_USED_FOR_DEBUG(b);
    eigen_assert((Transpose_?this->cols():this->rows())==b.rows() && "SymbolicSVDBase::solve(): invalid number of rows of the right hand side matrix b");
    eigen_assert(computeU() && computeV() && "SymbolicSVDBase::solve(): Both unitaries U and V are required to be computed (thin unitaries suffice).");
  }
};

} // end namespace Eigen_AD
} // end namespace Eigen
#endif // EIGEN_AD_SVDBASE_H
