// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#if (defined EIGEN_LU_H) || (defined EIGEN_AD_FORCE_INCLUDE_SOLVERS)
#ifndef EIGEN_AD_FULLPIVLU_H
#define EIGEN_AD_FULLPIVLU_H

#include "Eigen/src/misc/Kernel.h"
#include "Eigen/src/misc/Image.h"

#define SOLVER_INSTANTIATION_FULLPIVLU_AD FullPivLU<_MatrixType, EIGEN_IF_CUSTOM_SOLVER_AD(_MatrixType)>

namespace Eigen {

template<typename _MatrixType>
class SOLVER_INSTANTIATION_FULLPIVLU_AD
  : public Eigen_AD::SDSolver<_MatrixType,
                          FullPivLU<_MatrixType> > {
  public:
    typedef _MatrixType MatrixType;
    typedef Eigen_AD::SDSolver<MatrixType, FullPivLU<MatrixType> > Base;

    EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(FullPivLU)

    // FIXME StorageIndex defined in EIGEN_GENERIC_PUBLIC_INTERFACE should be int
    enum {
      MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
    };
    typedef typename Base::ValueSolverType::IntRowVectorType IntRowVectorType;
    typedef typename Base::ValueSolverType::IntColVectorType IntColVectorType;
    typedef typename Base::ValueSolverType::PermutationQType PermutationQType;
    typedef typename Base::ValueSolverType::PermutationPType PermutationPType;

    typedef typename MatrixType::PlainObject PlainObject;

    //
    // Constructors
    //
    FullPivLU() {}
    FullPivLU(Index, Index) {}

    template<typename InputType>
    explicit FullPivLU(const EigenBase<InputType>& matrix) {
      this->compute(matrix.derived());
    }

    //
    // Potential symbolic functions
    //
    inline const MatrixType matrixLU() const                          { return this->matrixLU_sym(); }
    typename internal::traits<MatrixType>::Scalar determinant() const { return this->determinant_sym(); }

    inline const Inverse<FullPivLU> inverse() const {
      this->getSolver(); // Initialized assertion
      return Inverse<FullPivLU>(*this);
    }

    inline const internal::kernel_retval<FullPivLU> kernel() const {
      this->getSolver(); // Initialized assertion
      return internal::kernel_retval<FullPivLU>(*this);
    }

    inline const internal::image_retval<FullPivLU>
      image(const MatrixType& originalMatrix) const {
      this->getSolver(); // Initialized assertion
      return internal::image_retval<FullPivLU>(*this, originalMatrix);
    }

    //
    // Other interface functions
    //
    inline Index nonzeroPivots() const       { return this->getSolver().nonzeroPivots(); }
    ValueRealScalar maxPivot() const       { return this->getSolver().maxPivot(); }
    EIGEN_DEVICE_FUNC inline const PermutationPType& permutationP() const { return this->getSolver().permutationP(); }
    inline const PermutationQType& permutationQ() const { return this->getSolver().permutationQ(); }
    FullPivLU& setThreshold(const ValueRealScalar& threshold) { this->getSolver().setThreshold(threshold); return *this; }
    FullPivLU& setThreshold(Default_t) { this->getSolver().setThreshold(); return *this; }
    ValueRealScalar threshold() const { return this->getSolver().threshold(); }
    inline Index rank() const { return this->getSolver().rank(); }
    inline Index dimensionOfKernel() const { return this->getSolver().dimensionOfKernel(); }
    inline bool isInjective() const { return this->getSolver().isInjective(); }
    inline bool isSurjective() const { return this->getSolver().isSurjective(); }
    inline bool isInvertible() const { return this->getSolver().isInvertible(); }
    inline ValueRealScalar rcond() const { return this->getSolver().rcond(); }
};

namespace internal {

  template<typename _MatrixType>
  struct kernel_retval<SOLVER_INSTANTIATION_FULLPIVLU_AD >
    : kernel_retval_base<SOLVER_INSTANTIATION_FULLPIVLU_AD > {
    EIGEN_MAKE_KERNEL_HELPERS(SOLVER_INSTANTIATION_FULLPIVLU_AD)

    enum { MaxSmallDimAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(
              _MatrixType::MaxColsAtCompileTime,
              _MatrixType::MaxRowsAtCompileTime)
    };

    template<typename Dest> void evalTo(Dest& dst) const { dec().kernel_sym(dst); }
  };

  template<typename _MatrixType>
  struct image_retval<SOLVER_INSTANTIATION_FULLPIVLU_AD >
    : image_retval_base<SOLVER_INSTANTIATION_FULLPIVLU_AD > {
    EIGEN_MAKE_IMAGE_HELPERS(SOLVER_INSTANTIATION_FULLPIVLU_AD)

    enum { MaxSmallDimAtCompileTime = EIGEN_SIZE_MIN_PREFER_FIXED(
              _MatrixType::MaxColsAtCompileTime,
              _MatrixType::MaxRowsAtCompileTime)
    };

    template<typename Dest> void evalTo(Dest& dst) const { dec().image_sym(dst, originalMatrix()); }
  };

  template<typename DstXprType, typename _MatrixType>
  struct Assignment<DstXprType, Inverse<SOLVER_INSTANTIATION_FULLPIVLU_AD >,
                    internal::assign_op<typename DstXprType::Scalar, typename SOLVER_INSTANTIATION_FULLPIVLU_AD::Scalar>, Dense2Dense> {
    typedef typename _MatrixType::Scalar Scalar;
    typedef FullPivLU<_MatrixType> LuType;
    typedef Inverse<LuType> SrcXprType;
    static void run(DstXprType &dst, const SrcXprType &src, const internal::assign_op<typename DstXprType::Scalar, Scalar> &) {
      src.nestedExpression().inverse_sym(dst);
    }
  };

} // end namespace internal

} // end namespace Eigen

#endif // EIGEN_AD_FULLPIVLU_H
#endif // EIGEN_LU_H
