// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#if (defined EIGEN_JACOBISVD_H) || (defined EIGEN_AD_FORCE_INCLUDE_SOLVERS)
#ifndef EIGEN_AD_JACOBISVD_H
#define EIGEN_AD_JACOBISVD_H

#define SOLVER_INSTANTIATION_JACOBISVD_AD JacobiSVD<_MatrixType, QRPreconditioner, EIGEN_IF_CUSTOM_SOLVER_AD(_MatrixType)>

namespace Eigen {

template<typename _MatrixType, int QRPreconditioner>
class SOLVER_INSTANTIATION_JACOBISVD_AD
  : public Eigen_AD::SymbolicSVDBase<_MatrixType,
                           JacobiSVD<_MatrixType, QRPreconditioner> > {
  public:
    typedef _MatrixType MatrixType;
    typedef Eigen_AD::SymbolicSVDBase<MatrixType, JacobiSVD<_MatrixType, QRPreconditioner> > Base;

    //EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(JacobiSVD)
    typedef typename Base::ValueSolverType::MatrixType ValueMatrixType;
    typedef typename MatrixType::Scalar Scalar;
    typedef typename Base::ValueSolverType::Scalar ValueScalar;
    typedef typename NumTraits<typename MatrixType::Scalar>::Real RealScalar;
    typedef typename Base::ValueSolverType::RealScalar ValueRealScalar;
    typedef typename MatrixType::StorageIndex StorageIndex;
    typedef typename Base::ValueSolverType::StorageIndex ValueStorageIndex;
    typedef Eigen::Index Index; ///< \deprecated since Eigen 3.3
    enum {
      RowsAtCompileTime = MatrixType::RowsAtCompileTime,
      ColsAtCompileTime = MatrixType::ColsAtCompileTime,
      DiagSizeAtCompileTime = Base::ValueSolverType::DiagSizeAtCompileTime,
      MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime,
      MaxDiagSizeAtCompileTime = Base::ValueSolverType::MaxDiagSizeAtCompileTime,
      MatrixOptions = MatrixType::Options
    };
    typedef typename Base::MatrixUType MatrixUType;
    typedef typename Base::MatrixVType MatrixVType;
    typedef typename Base::SingularValuesType SingularValuesType;

    typedef typename Base::ValueSolverType::RowType RowType;
    typedef typename Base::ValueSolverType::ColType ColType;
    typedef typename Base::ValueSolverType::WorkMatrixType WorkMatrixType;

    //
    // Constructors
    //
    JacobiSVD() {}
    JacobiSVD(Index, Index, unsigned int = 0) {}

    explicit JacobiSVD(const MatrixType& matrix, unsigned int computationOptions = 0) {
      this->compute(matrix.derived(), computationOptions);
    }
};

} // end namespace Eigen

#endif // EIGEN_AD_JACOBISVD_H
#endif // EIGEN_JACOBISVD_H
