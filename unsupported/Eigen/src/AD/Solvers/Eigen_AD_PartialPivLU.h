// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#if (defined EIGEN_PARTIALLU_H) || (defined EIGEN_AD_FORCE_INCLUDE_SOLVERS)
#ifndef EIGEN_AD_PARTIALPIVLU_H
#define EIGEN_AD_PARTIALPIVLU_H

#define SOLVER_INSTANTIATION_PARTIALPIVLU_AD PartialPivLU<_MatrixType, EIGEN_IF_CUSTOM_SOLVER_AD(_MatrixType)>

namespace Eigen {

template<typename _MatrixType>
class SOLVER_INSTANTIATION_PARTIALPIVLU_AD
    : public Eigen_AD::SDSolver<_MatrixType,
                            PartialPivLU<_MatrixType> > {
  public:
    typedef _MatrixType MatrixType;
    typedef Eigen_AD::SDSolver<MatrixType, PartialPivLU<MatrixType> > Base;

    EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(PartialPivLU)

    // FIXME StorageIndex defined in EIGEN_GENERIC_PUBLIC_INTERFACE should be int
    enum {
      MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
    };
    typedef typename Base::ValueSolverType::PermutationType PermutationType;
    typedef typename Base::ValueSolverType::TranspositionType TranspositionType;

    typedef typename MatrixType::PlainObject PlainObject;

    //
    // Constructors
    //
    PartialPivLU() {}
    PartialPivLU(Index, Index) {}

    template<typename InputType>
    explicit PartialPivLU(const EigenBase<InputType>& matrix) {
      this->compute(matrix.derived());
    }

    //
    // Potential symbolic functions
    //
    inline const MatrixType matrixLU() const  { return this->matrixLU_sym(); }
    Scalar determinant() const                { return this->determinant_sym(); }

    inline const Inverse<PartialPivLU> inverse() const {
      this->getSolver(); // Initialized assertion
      return Inverse<PartialPivLU>(*this);
    }

    //
    // Other interface functions
    //
    inline const PermutationType& permutationP() const { return this->getSolver().permutationP(); }
    inline ValueRealScalar rcond() const { return this->getSolver().rcond(); }
};

namespace internal {

  template<typename DstXprType, typename _MatrixType>
  struct Assignment<DstXprType, Inverse<SOLVER_INSTANTIATION_PARTIALPIVLU_AD >,
                    internal::assign_op<typename DstXprType::Scalar, typename SOLVER_INSTANTIATION_PARTIALPIVLU_AD::Scalar>, Dense2Dense> {
    typedef typename _MatrixType::Scalar Scalar;
    typedef PartialPivLU<_MatrixType> LuType;
    typedef Inverse<LuType> SrcXprType;
    static void run(DstXprType &dst, const SrcXprType &src, const internal::assign_op<typename DstXprType::Scalar, Scalar> &) {
      src.nestedExpression().inverse_sym(dst);
    }
  };

} // end namespace internal
} // end namespace Eigen

#endif // EIGEN_AD_PARTIALPIVLU_H
#endif // EIGEN_PARTIALLU_H
