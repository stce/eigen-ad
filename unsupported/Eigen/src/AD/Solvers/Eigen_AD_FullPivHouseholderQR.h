// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#if (defined EIGEN_FULLPIVOTINGHOUSEHOLDERQR_H) || (defined EIGEN_AD_FORCE_INCLUDE_SOLVERS)
#ifndef EIGEN_AD_FULLPIVHOUSEHOLDERQR_H
#define EIGEN_AD_FULLPIVHOUSEHOLDERQR_H

#define SOLVER_INSTANTIATION_FULLPIVHOUSEHOLDERQR_AD FullPivHouseholderQR<_MatrixType, EIGEN_IF_CUSTOM_SOLVER_AD(_MatrixType)>

namespace Eigen {

template<typename _MatrixType>
class SOLVER_INSTANTIATION_FULLPIVHOUSEHOLDERQR_AD
    : public Eigen_AD::SDSolver<_MatrixType,
                            FullPivHouseholderQR<_MatrixType> > {
  public:
    typedef _MatrixType MatrixType;
    typedef Eigen_AD::SDSolver<MatrixType, FullPivHouseholderQR<MatrixType> > Base;

    EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(FullPivHouseholderQR)

    // FIXME StorageIndex defined in EIGEN_GENERIC_PUBLIC_INTERFACE should be int
    enum {
      MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime
    };
    typedef internal::FullPivHouseholderQRMatrixQReturnType<MatrixType> MatrixQReturnType;
    typedef typename internal::plain_diag_type<MatrixType>::type HCoeffsType;
    typedef typename Base::ValueSolverType::IntDiagSizeVectorType IntDiagSizeVectorType;
    typedef typename Base::ValueSolverType::PermutationType PermutationType;
    typedef typename Base::ValueSolverType::RowVectorType RowVectorType;
    typedef typename Base::ValueSolverType::ColVectorType ColVectorType;
    typedef typename MatrixType::PlainObject PlainObject;

    //
    // Constructors
    //
    FullPivHouseholderQR() {}
    FullPivHouseholderQR(Index, Index) {}

    template<typename InputType>
    explicit FullPivHouseholderQR(const EigenBase<InputType>& matrix) {
      this->compute(matrix.derived());
    }

    //
    // Potential symbolic functions
    //
    MatrixQReturnType matrixQ(void) const        { return this->matrixQ_sym(); }
    const MatrixType matrixQR() const            { return this->matrixQR_sym(); }
    const HCoeffsType& hCoeffs() const           { return this->hCoeffs_sym(); }
    RealScalar absDeterminant() const            { return this->absDeterminant_sym(); }
    RealScalar logAbsDeterminant() const         { return this->logAbsDeterminant_sym(); }

    inline const Inverse<FullPivHouseholderQR> inverse() const {
      this->getSolver(); // Initialized assertion
      return Inverse<FullPivHouseholderQR>(*this);
    }

    //
    // Other interface functions
    //
    const PermutationType& colsPermutation() const { return this->getSolver().colsPermutation(); }
    const IntDiagSizeVectorType& rowsTranspositions() const { return this->getSolver().rowsTranspositions(); }
    inline Index rank() const  { return this->getSolver().rank(); }
    inline Index dimensionOfKernel() const { return this->getSolver().dimensionOfKernel(); }
    inline bool isInjective() const { return this->getSolver().isInjective(); }
    inline bool isSurjective() const { return this->getSolver().isSurjective(); }
    inline bool isInvertible() const { return this->getSolver().isInvertible(); }
    FullPivHouseholderQR& setThreshold(const ValueRealScalar& threshold) { this->getSolver().setThreshold(threshold); return *this; }
    FullPivHouseholderQR& setThreshold(Default_t) { this->getSolver().setThreshold(); return *this; }
    ValueRealScalar threshold() const { return this->getSolver().threshold(); }
    inline Index nonzeroPivots() const { return this->getSolver().nonzeroPivots(); }
    ValueRealScalar maxPivot() const { return this->getSolver().maxPivot(); }
};

namespace internal {

  template<typename DstXprType, typename _MatrixType>
  struct Assignment<DstXprType, Inverse<SOLVER_INSTANTIATION_FULLPIVHOUSEHOLDERQR_AD >,
                    internal::assign_op<typename DstXprType::Scalar, typename SOLVER_INSTANTIATION_FULLPIVHOUSEHOLDERQR_AD::Scalar>, Dense2Dense> {
    typedef typename _MatrixType::Scalar Scalar;
    typedef FullPivHouseholderQR<_MatrixType> QrType;
    typedef Inverse<QrType> SrcXprType;
    static void run(DstXprType &dst, const SrcXprType &src, const internal::assign_op<typename DstXprType::Scalar, Scalar> &) {
      src.nestedExpression().inverse_sym(dst);
    }
  };

} // end namespace internal
} // end namespace Eigen

#endif // EIGEN_AD_FULLPIVHOUSEHOLDERQR_H
#endif // EIGEN_FULLPIVOTINGHOUSEHOLDERQR_H
