// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#if (defined EIGEN_BDCSVD_H) || (defined EIGEN_AD_FORCE_INCLUDE_SOLVERS)
#ifndef EIGEN_AD_BDCSVD_H
#define EIGEN_AD_BDCSVD_H

#define SOLVER_INSTANTIATION_BDCSVD_AD BDCSVD<_MatrixType, EIGEN_IF_CUSTOM_SOLVER_AD(_MatrixType)>

namespace Eigen {

template<typename _MatrixType>
class SOLVER_INSTANTIATION_BDCSVD_AD
  : public Eigen_AD::SymbolicSVDBase<_MatrixType,
                           BDCSVD<_MatrixType> > {
  public:
    typedef _MatrixType MatrixType;
    typedef Eigen_AD::SymbolicSVDBase<MatrixType, BDCSVD<_MatrixType> > Base;

    //EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(BDCSVD)
    typedef typename Base::ValueSolverType::MatrixType ValueMatrixType;
    typedef typename MatrixType::Scalar Scalar;
    typedef typename Base::ValueSolverType::Scalar ValueScalar;
    typedef typename NumTraits<typename MatrixType::Scalar>::Real RealScalar;
    typedef typename Base::ValueSolverType::RealScalar ValueRealScalar;
    typedef typename NumTraits<RealScalar>::Literal Literal;
    typedef typename Base::ValueSolverType::Literal ValueLiteral;
    typedef Eigen::Index Index; ///< \deprecated since Eigen 3.3
    enum {
      RowsAtCompileTime = MatrixType::RowsAtCompileTime,
      ColsAtCompileTime = MatrixType::ColsAtCompileTime,
      DiagSizeAtCompileTime = Base::ValueSolverType::DiagSizeAtCompileTime,
      MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime,
      MaxDiagSizeAtCompileTime = Base::ValueSolverType::MaxDiagSizeAtCompileTime,
      MatrixOptions = MatrixType::Options
    };
    typedef typename Base::MatrixUType MatrixUType;
    typedef typename Base::MatrixVType MatrixVType;
    typedef typename Base::SingularValuesType SingularValuesType;

    typedef typename Base::ValueSolverType::MatrixX MatrixX;
    typedef typename Base::ValueSolverType::MatrixXr MatrixXr;
    typedef typename Base::ValueSolverType::VectorType VectorType;
    typedef typename Base::ValueSolverType::ArrayXr ArrayXr;
    typedef typename Base::ValueSolverType::ArrayXi ArrayXi;
    typedef typename Base::ValueSolverType::ArrayRef ArrayRef;
    typedef typename Base::ValueSolverType::IndicesRef IndicesRef;

    //
    // Constructors
    //
    BDCSVD() {}
    BDCSVD(Index, Index, unsigned int = 0) {}

    explicit BDCSVD(const MatrixType& matrix) {
        this->compute(matrix.derived());
    }

    explicit BDCSVD(const MatrixType& matrix, unsigned int computationOptions) {
        this->compute(matrix.derived(), computationOptions);
    }

    //
    // Other interface functions
    //
    void setSwitchSize(int s) { this->getSolver().setSwitchSize(s); }
};

} // end namespace Eigen

#endif // EIGEN_AD_BDCSVD_H
#endif // EIGEN_BDCSVD_H
