// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#if (defined EIGEN_LDLT_H) || (defined EIGEN_AD_FORCE_INCLUDE_SOLVERS)
#ifndef EIGEN_AD_LDLT_H
#define EIGEN_AD_LDLT_H

#define SOLVER_INSTANTIATION_LDLT_AD LDLT<_MatrixType, _UpLo, EIGEN_IF_CUSTOM_SOLVER_AD(_MatrixType)>

namespace Eigen {

template<typename _MatrixType, int _UpLo>
class SOLVER_INSTANTIATION_LDLT_AD
  : public Eigen_AD::SDSolver<_MatrixType,
                          LDLT<_MatrixType, _UpLo> > {
  public:
    typedef _MatrixType MatrixType;
    typedef Eigen_AD::SDSolver<MatrixType, LDLT<MatrixType, _UpLo> > Base;

    EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(LDLT)

    // FIXME StorageIndex defined in EIGEN_GENERIC_PUBLIC_INTERFACE should be int
    enum {
      MaxRowsAtCompileTime = MatrixType::MaxRowsAtCompileTime,
      MaxColsAtCompileTime = MatrixType::MaxColsAtCompileTime,
      UpLo = _UpLo
    };

    typedef typename Base::ValueSolverType::TmpMatrixType TmpMatrixType;
    typedef typename Base::ValueSolverType::TranspositionType TranspositionType;
    typedef typename Base::ValueSolverType::PermutationType PermutationType;
    typedef internal::LDLT_Traits<MatrixType,UpLo> Traits;

    //
    // Constructors
    //
    LDLT() {}
    LDLT(Index, Index) {}

    template<typename InputType>
    explicit LDLT(const EigenBase<InputType>& matrix) {
      this->compute(matrix.derived());
    }

    //
    // Potential symbolic functions
    //
    inline typename Traits::MatrixU matrixU() const { return this->matrixU_sym(); }
    inline typename Traits::MatrixL matrixL() const { return this->matrixL_sym(); }
    inline const MatrixType matrixLDLT() const { return this->matrixLDLT_sym(); }
    template <typename Derived>
    LDLT rankUpdate(const MatrixBase<Derived>& w, const RealScalar& alpha=1) { this->rankUpdate_sym(w, alpha); return *this; }

    //
    // Other interface functions
    //
    void setZero() { return this->getSolver().setZero(); }
    inline const TranspositionType& transpositionsP() const { return this->getSolver().transpositionsP(); }
    inline bool isPositive() const { return this->getSolver().isPositive(); }
    inline bool isNegative(void) const { return this->getSolver().isNegative(); }
    inline Diagonal<MatrixType> vectorD() const { return matrixLDLT().diagonal(); }
    inline ValueRealScalar rcond() const { return this->getSolver().rcond(); }
    const LDLT& adjoint() const { return *this; };
    ComputationInfo info() const { return this->getSolver().info(); }
};

} // end namespace Eigen

#endif // EIGEN_AD_LDLT_H
#endif // EIGEN_LDLT_H
