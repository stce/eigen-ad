// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_META_H
#define EIGEN_AD_META_H

namespace Eigen {
namespace Eigen_AD {
namespace internal {

  // Check if UnaryOp or BinaryOp should have boolean return type
  template<typename T> struct is_bool_op { enum { value = false }; };
  template<typename Scalar>  struct is_bool_op< Eigen::internal::scalar_isnan_op<Scalar> >{ enum { value = true }; };
  template<typename Scalar>  struct is_bool_op< Eigen::internal::scalar_isinf_op<Scalar> >{ enum { value = true }; };
  template<typename Scalar>  struct is_bool_op< Eigen::internal::scalar_isfinite_op<Scalar> >{ enum { value = true }; };
  template<typename LhsScalar, typename RhsScalar, Eigen::internal::ComparisonName cmp>  struct is_bool_op< Eigen::internal::scalar_cmp_op<LhsScalar, RhsScalar, cmp> >{ enum { value = true }; };
  template<typename T>  struct is_bool_op< std::equal_to<T> >{ enum { value = true }; };
  template<typename T>  struct is_bool_op< std::not_equal_to<T> >{ enum { value = true }; };

  // Wrapper to return the inner type if we have a complex type
  template<typename T> struct get_inner_type_base { typedef T type; enum { value = false }; };
  template<typename T> struct get_inner_type_base<std::complex<T> > { typedef T type; enum { value = true }; };
  template<typename T> struct get_inner_type { typedef typename get_inner_type_base<typename Eigen::internal::remove_all<T>::type>::type type;
                                               enum { value = get_inner_type_base<typename Eigen::internal::remove_all<T>::type>::value}; };

  // Check if T == complex<U>
  template<typename T, typename U> struct is_complex_of_U_base { enum { value = get_inner_type<T>::value && std::is_same<typename get_inner_type<T>::type, U>::value }; };
  template<typename T, typename U> struct is_complex_of_U { enum { value = is_complex_of_U_base<typename Eigen::internal::remove_all<T>::type,
                                                                                                typename Eigen::internal::remove_all<U>::type>::value}; };
  // Check if T == complex<U> or U == complex<T>
  template<typename T, typename U> struct is_complex_of_T_or_U { enum { value = bool(is_complex_of_U<T,U>::value || is_complex_of_U<U,T>::value) }; };

} // end namespace internal
} // end namespace Eigen_AD
} // end namespace Eigen

#endif // EIGEN_AD_META_H
