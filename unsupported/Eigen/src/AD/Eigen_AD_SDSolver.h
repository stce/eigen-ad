// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_SDSOLVER_H
#define EIGEN_AD_SDSOLVER_H

#ifndef EIGEN_AD_NO_SYMBOLIC_SOLVERS
  #define EIGEN_AD_NO_SYMBOLIC_SOLVERS 0
#else
  #undef EIGEN_AD_NO_SYMBOLIC_SOLVERS
  #define EIGEN_AD_NO_SYMBOLIC_SOLVERS 1
#endif

namespace Eigen {
namespace Eigen_AD {

template<typename _WrapperSolverType> struct solver_container;

/******************************************************************************
 *
 * Eigen-AD Dense Solver API
 *
 * Specialize these structs for your AD-O tool module.
 *
 *****************************************************************************/
// If you want to enable symbolic dense solvers for your AD type (i.e.
// _MatrixType::Scalar), define an enum with 'value=true'.
template<typename _MatrixType, typename enable_if = void>
struct solver_enable;

// Define your symbolic dense solver implementations in your specialization.
// Refer to the implementation of solver_container to see the
// available functions. TODO: Document this better.
template<typename _MatrixType, typename _ValueSolverType, typename _WrapperSolverType, typename enable_if = void>
struct solver : public solver_container<_WrapperSolverType> { };

/******************************************************************************
 *
 * Internals
 *
 *****************************************************************************/
template<typename _MatrixType, typename enable_if> struct solver_enable { enum { value = false }; };

// Since every solver has a dummy template parameter at last position, we can not use parameter packs for optional template arguments as they always have to be last in case of class templates.
// We therefore provide 2 partial specializations using template template parameters which pass the resulting _SolverType and _ValueSolverType to their parent class SDSolver_impl.
template<typename _MatrixType, typename _SolverType>                                                         class SDSolver;
template<typename _MatrixType, typename _SolverType, typename _ValueSolverType, typename _WrapperSolverType> class SDSolver_impl;

// No optional template parameter
template<typename _MatrixType, template<typename, typename> class _SolverType, typename T>
class SDSolver<_MatrixType, _SolverType<_MatrixType, T> >
    : public SDSolver_impl<_MatrixType, _SolverType<_MatrixType, void>,
                           _SolverType<Matrix<typename value_type<typename _MatrixType::Scalar>::type, _MatrixType::RowsAtCompileTime, _MatrixType::ColsAtCompileTime>, void >,
                           _SolverType<internal::Matrix_AD_Wrapper<_MatrixType>, void > > {
};

// One optional template parameter (LLT, LDLT)
template<typename _MatrixType, template<typename, int, typename> class _SolverType, int Arg>
class SDSolver<_MatrixType, _SolverType<_MatrixType, Arg, void> >
    : public SDSolver_impl<_MatrixType, _SolverType<_MatrixType, Arg, void>,
                           _SolverType<Matrix<typename value_type<typename _MatrixType::Scalar>::type, _MatrixType::RowsAtCompileTime, _MatrixType::ColsAtCompileTime>, Arg, void>,
                           _SolverType<internal::Matrix_AD_Wrapper<_MatrixType>, Arg, void > > {
};

// No optional template parameter with SymbolicSVDBase parent class (BDCSVD)
template<typename _MatrixType, typename _SolverType> class SymbolicSVDBase;
template<typename _MatrixType, template<typename, typename> class _SolverType>
class SDSolver<_MatrixType, SymbolicSVDBase<_MatrixType, _SolverType<_MatrixType, void> > >
    : public SDSolver_impl<_MatrixType, SymbolicSVDBase<_MatrixType, _SolverType<_MatrixType, void> >,
                                 _SolverType<Matrix<typename value_type<typename _MatrixType::Scalar>::type, _MatrixType::RowsAtCompileTime, _MatrixType::ColsAtCompileTime>, void>,
                                 _SolverType<internal::Matrix_AD_Wrapper<_MatrixType>, void > > {
};

// One optional template parameter with SymbolicSVDBase parent class (JacobiSVD)
template<typename _MatrixType, template<typename, int, typename> class _SolverType, int Arg>
class SDSolver<_MatrixType, SymbolicSVDBase<_MatrixType, _SolverType<_MatrixType, Arg, void> > >
    : public SDSolver_impl<_MatrixType, SymbolicSVDBase<_MatrixType, _SolverType<_MatrixType, Arg, void> >,
                           _SolverType<Matrix<typename value_type<typename _MatrixType::Scalar>::type, _MatrixType::RowsAtCompileTime, _MatrixType::ColsAtCompileTime>, Arg, void>,
                           _SolverType<internal::Matrix_AD_Wrapper<_MatrixType>, Arg, void > > {
};

template<typename _MatrixType, typename _SolverType, typename _ValueSolverType, typename _WrapperSolverType> class SDSolver_impl
  : public SolverBase< _SolverType > {
  public:
    typedef _MatrixType        MatrixType;
    typedef _SolverType        SolverType;
    typedef _ValueSolverType   ValueSolverType;
    typedef _WrapperSolverType WrapperSolverType;

    solver<MatrixType, ValueSolverType, WrapperSolverType> container;

    auto& getSolver() const {
      return container.getSolver();
    }

    auto rows() const {
      return getSolver().rows();
    }

    auto cols() const {
      return getSolver().cols();
    }

    template<typename InputType>
    SolverType& compute(const EigenBase<InputType>& matrix) {
      container.compute(matrix);
      return *static_cast<SolverType*>(this);
    }

    template<typename RhsType, typename DstType>
    EIGEN_DEVICE_FUNC
    void _solve_impl(const RhsType &rhs, DstType &dst) const {
      container.solve(rhs, dst);
    }

    template<bool Conjugate, typename RhsType, typename DstType>
    EIGEN_DEVICE_FUNC
    void _solve_impl_transposed(const RhsType &rhs, DstType &dst) const {
      container.template solve_transposed<Conjugate>(rhs, dst);
    }

    auto matrixLU_sym() const {
      return container.matrixLU();
    }

    template<typename DstType>
    void inverse_sym(DstType& dst) const {
      container.inverse(dst);
    }

    auto determinant_sym() const {
      return container.determinant();
    }

    auto absDeterminant_sym() const {
      return container.absDeterminant();
    }

    auto logAbsDeterminant_sym() const {
      return container.logAbsDeterminant();
    }

    template<typename DstType>
    void kernel_sym(DstType& dst) const {
      container.kernel(dst);
    }

    template<typename DstType>
    void image_sym(DstType& dst, const MatrixType& originalMatrix) const {
      container.image(dst, originalMatrix);
    }

    auto matrixU_sym() const {
      return container.matrixU();
    }

    auto matrixL_sym() const {
      return container.matrixL();
    }

    auto matrixLLT_sym() const {
      return container.matrixLLT();
    }

    auto matrixLDLT_sym() const {
      return container.matrixLDLT();
    }

    template<typename T, typename U>
    void rankUpdate_sym(const T& vec, const U& factor) {
      container.rankUpdate(*this, vec, factor);
    }

    auto householderQ_sym() const {
      return container.householderQ();
    }

    auto matrixQ_sym() const {
      return container.matrixQ();
    }

    auto matrixR_sym() const {
      return container.matrixR();
    }

    auto matrixQR_sym() const {
      return container.matrixQR();
    }

    auto matrixZ_sym() const {
      return container.matrixZ();
    }

    auto matrixQTZ_sym() const {
      return container.matrixQTZ();
    }

    auto matrixT_sym() const {
      return container.matrixT();
    }

    auto hCoeffs_sym() const {
      return container.hCoeffs();
    }

    auto zCoeffs_sym() const {
      return container.zCoeffs();
    }

    auto matrixV_sym() const {
      return container.matrixV();
    }

    auto singularValues_sym() const {
      return container.singularValues();
    }

  protected:

    template<bool Transpose_, typename Rhs>
    void _check_solve_assertion(const Rhs& b) const {
      EIGEN_ONLY_USED_FOR_DEBUG(b);
      eigen_assert((Transpose_?getSolver().cols():getSolver().rows())==b.rows() && "SDSolver::solve(): invalid number of rows of the right hand side matrix b");
    }
};

//
// Below are the default implementations used when a function is not provided by an AD tool. By default, compilation should fail. If the user is keen, he can try to use algorithmic derivatives instead (experimental).
//
#define EIGEN_AD_SOLVER_FALLBACK_ASSERT eigen_assert(algSolver && "Symbolic solver fallback to algorithmic version not possible! Info for AD Tool developers: Make sure you call the parent compute() routine in your specialization.");
template<typename _WrapperSolverType> struct solver_container {
  typedef _WrapperSolverType WrapperSolverType;
#ifdef EIGEN_AD_SOLVER_FALLBACK_TO_ALG
  WrapperSolverType* algSolver;

  solver_container() : algSolver(nullptr) { }
  solver_container(const solver_container& copy) : algSolver(nullptr) { if(copy.algSolver) algSolver = new WrapperSolverType(*copy.algSolver); }
  ~solver_container() { delete algSolver; }

  WrapperSolverType& getSolver() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return *algSolver;
  }

  template<typename InputType>
  void compute(const EigenBase<InputType>& matrix) {
      if(!algSolver) algSolver = new WrapperSolverType(matrix.derived());
      else algSolver->compute(matrix);
  }

  template<typename InputType>
  void compute_svd(const EigenBase<InputType>& matrix) {
      if(!algSolver) algSolver = new WrapperSolverType(matrix.derived());
      else algSolver->compute(matrix);
  }

  template<typename InputType>
  void compute_svd(const EigenBase<InputType>& matrix, unsigned int computationOptions) {
      if(!algSolver) algSolver = new WrapperSolverType(matrix.derived(), computationOptions);
      else algSolver->compute(matrix, computationOptions);
  }

  template<typename RhsType, typename DstType>
  EIGEN_DEVICE_FUNC
  void solve(const RhsType &rhs, DstType &dst) const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    algSolver->_solve_impl(rhs, dst);
  }

  template<bool Conjugate, typename RhsType, typename DstType>
  EIGEN_DEVICE_FUNC
  void solve_transposed(const RhsType &rhs, DstType &dst) const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    algSolver->template _solve_impl_transposed<Conjugate>(rhs, dst);
  }

  typename WrapperSolverType::MatrixType matrixLU() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixLU();
  }

  template<typename DstType>
  void inverse(DstType& dst) const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    algSolver->inverse(dst);
  }

  typename internal::traits<typename WrapperSolverType::MatrixType>::Scalar determinant() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->determinant();
  }

  typename WrapperSolverType::MatrixType::RealScalar absDeterminant() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->absDeterminant();
  }

  typename WrapperSolverType::MatrixType::RealScalar logAbsDeterminant() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->logAbsDeterminant();
  }

  template<typename DstType>
  void kernel(DstType& dst) const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    dst = algSolver->kernel();
  }

  template<typename DstType>
  void image(DstType& dst, const typename WrapperSolverType::MatrixType& originalMatrix) const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    dst = algSolver->image(originalMatrix);
  }

  auto matrixU() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixU();
  }

  auto matrixL() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixL();
  }

  typename WrapperSolverType::MatrixType matrixLLT() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixLLT();
  }

  typename WrapperSolverType::MatrixType matrixLDLT() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixLDLT();
  }

  template <typename SolverType, typename T, typename U>
  void rankUpdate(SolverType& solver, const T& vec, const U& factor) {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    algSolver->rankUpdate(vec, factor);
    // TODO: Now what?
  }

  auto householderQ() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->householderQ();
  }

  auto matrixQ() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixQ();
  }

  typename WrapperSolverType::MatrixType matrixR() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixR();
  }

  typename WrapperSolverType::MatrixType matrixQR() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixQR();
  }

  typename WrapperSolverType::MatrixType matrixZ() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixZ();
  }

  typename WrapperSolverType::MatrixType matrixQTZ() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixQTZ();
  }

  typename WrapperSolverType::MatrixType matrixT() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixT();
  }

  auto hCoeffs() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->hCoeffs();
  }

  auto zCoeffs() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->zCoeffs();
  }

  auto matrixV() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->matrixV();
  }

  auto singularValues() const {
    EIGEN_AD_SOLVER_FALLBACK_ASSERT
    return algSolver->singularValues();
  }
#else
  template<typename InputType>
  void compute(const EigenBase<InputType>&) {
  }

  template<typename InputType>
  void compute_svd(const EigenBase<InputType>&) {
  }

  template<typename InputType>
  void compute_svd(const EigenBase<InputType>&, unsigned int) {
  }
#endif
};

} // end namespace Eigen_AD

namespace internal {
  // Forward to solver_enable
  template<typename MatrixType>
  struct eigen_enable_custom_solver<MatrixType, typename std::enable_if<(!Eigen_AD::internal::is_Matrix_AD_Wrapper<typename remove_all<MatrixType>::type>::value &&
                                                                         Eigen_AD::solver_enable<MatrixType>::value && !EIGEN_AD_NO_SYMBOLIC_SOLVERS)>::type>
  {
    enum { value = true };
  };

} // end namespace internal
} // end namespace Eigen

#define EIGEN_IF_CUSTOM_SOLVER_AD(MatrixType) typename std::enable_if<internal::eigen_enable_custom_solver<MatrixType >::value>::type

#define EIGEN_GENERIC_PUBLIC_INTERFACE_SYMBOLIC_SOLVER(Derived) \
  template<typename Derived> \
  friend struct internal::solve_assertion; \
  EIGEN_GENERIC_PUBLIC_INTERFACE(Derived) \
  typedef typename Base::ValueSolverType::Scalar ValueScalar; \
  typedef typename Base::ValueSolverType::RealScalar ValueRealScalar; \
  typedef typename Base::ValueSolverType::CoeffReturnType ValueCoeffReturnType; \
  typedef typename Base::ValueSolverType::Nested ValueNested; \
  typedef typename Base::ValueSolverType::StorageKind ValueStorageKind; \
  typedef typename Base::ValueSolverType::StorageIndex ValueStorageIndex; \
  enum { ValueRowsAtCompileTime = Base::ValueSolverType::RowsAtCompileTime, \
         ValueColsAtCompileTime = Base::ValueSolverType::ColsAtCompileTime, \
         ValueFlags = Base::ValueSolverType::Flags, \
         ValueSizeAtCompileTime = Base::ValueSolverType::SizeAtCompileTime, \
         ValueMaxSizeAtCompileTime = Base::ValueSolverType::MaxSizeAtCompileTime, \
         ValueIsVectorAtCompileTime = Base::ValueSolverType::IsVectorAtCompileTime };

#endif // EIGEN_AD_SDSOLVER_H
