// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD_H
#define EIGEN_AD_H

#ifdef EIGEN_AD_NO_AUTO_RETURN_TYPE
  #undef EIGEN_AD_NO_AUTO_RETURN_TYPE
  #define EIGEN_AD_NO_AUTO_RETURN_TYPE 1
#else
  #define EIGEN_AD_NO_AUTO_RETURN_TYPE 0
#endif

namespace Eigen {
namespace Eigen_AD {

/******************************************************************************
 *
 * Eigen-AD General API
 *
 * Specialize these structs for your AD-O tool module.
 *
 *****************************************************************************/
// Define an enum with 'value=true' if T is your AD type.
template<typename T, typename enable_if = void> struct is_ad_type;

// If T is your AD type, define a typedef 'type' pointing to the value type
// of your AD type (=primal type in case of first order).
template<typename T, typename enable_if = void> struct value_type;

// If T is a template expression type of your AD tool, the typedef 'type' should
// point to your AD type. Don't specialize otherwise.
template<typename T, typename enable_if = void> struct active_type;

// If you want to allow a scalar binary operation between T and U, define
// the typedef 'ReturnType' in your specialization. Required if your AD
// tool uses expression templates to change the result to the AD type.
template <typename T, typename U, typename BinaryOp, typename enable_if = void>
struct scalar_binary_return_type;

/******************************************************************************
 *
 * Eigen-AD Auto-Return-Type-Deduction API
 *
 * Specialize this struct for your AD-O tool module.
 *
 *****************************************************************************/
// If T is your AD type and your tool uses expression templates, define an
// enum with 'value=true' to enable auto-return-type-deduction.
template<typename T, typename enable_if = void>
struct enable_auto_return_type;

/******************************************************************************
 *
 * Internals
 *
 *****************************************************************************/
template<typename T, typename enable_if>
struct is_ad_type { enum { value = false }; };

template<typename T, typename enable_if>
struct value_type { typedef T type; };

template<typename T, typename enable_if>
struct active_type { typedef T type; };

template<typename T, typename enable_if>
struct enable_auto_return_type { enum { value = false }; };

template <typename T, typename U, typename BinaryOp, typename enable_if>
struct scalar_binary_return_type { };

namespace internal {
  //
  // Experimental feature to allow fallback to AD evaluation.
  //
  template<typename _MatrixType> class Matrix_AD_Wrapper;

  template<typename _MatrixType>
  class Matrix_AD_Wrapper : public Matrix<typename _MatrixType::Scalar, _MatrixType::RowsAtCompileTime, _MatrixType::ColsAtCompileTime,
                                          _MatrixType::Options, _MatrixType::MaxRowsAtCompileTime, _MatrixType::MaxColsAtCompileTime> {
  public:

      typedef _MatrixType Base;
      using Base::Base;

      typedef const Matrix_AD_Wrapper<Base>   Self;
      //typedef typename _MatrixType::ConjugateReturnType ConjugateReturnType;
      //typedef Matrix_AD_Wrapper<typename Base::ConjugateReturnType> ConjugateReturnType;

      template<bool Cond>
      EIGEN_DEVICE_FUNC
      inline typename Eigen::internal::conditional<Cond,Self&,Self&>::type
      conjugateIf() const {
        //typedef typename Eigen::internal::conditional<Cond,ConjugateReturnType,Self>::type ReturnType;
        return *this;
      }

  };

  template<typename T> struct is_Matrix_AD_Wrapper                    { enum { value = false }; };
  template<typename T> struct is_Matrix_AD_Wrapper<Matrix_AD_Wrapper<T> > { enum { value = true }; };

} // end namespace internal
} // end namespace Eigen-AD

//
// Specializations
//

// NumTraits for the AD type.
template<typename T> struct NumTraits<T, typename std::enable_if<Eigen_AD::is_ad_type<T>::value>::type>
 : GenericNumTraits<typename Eigen_AD::value_type<T>::type> {
  typedef typename Eigen_AD::active_type<T>::type Real;  // Allows intermediates to point to their active type in Fuzzy functions (e.g. isMuchSmallerThan)
  typedef Real NonInteger;
  typedef Real Literal;
  typedef Real Nested;

  // We need to make sure to adjust the floating point return value of the inherited functions to active type
  EIGEN_DEVICE_FUNC static inline Real epsilon()          { return NumTraits<typename Eigen_AD::value_type<T>::type>::epsilon(); }
  EIGEN_DEVICE_FUNC static inline Real dummy_precision()  { return NumTraits<typename Eigen_AD::value_type<T>::type>::dummy_precision(); }

  // TODO: think about the right ReadCost, AddCost and MulCost
  enum {
    IsComplex = NumTraits<typename Eigen_AD::value_type<T>::type>::IsComplex,
    IsInteger = NumTraits<typename Eigen_AD::value_type<T>::type>::IsInteger,
    ReadCost  = NumTraits<typename Eigen_AD::value_type<T>::type>::ReadCost,
    AddCost   = NumTraits<typename Eigen_AD::value_type<T>::type>::AddCost,
    MulCost   = NumTraits<typename Eigen_AD::value_type<T>::type>::MulCost,
    IsSigned  = NumTraits<typename Eigen_AD::value_type<T>::type>::IsSigned,
    RequireInitialization = NumTraits<typename Eigen_AD::value_type<T>::type>::RequireInitialization
  };

  // Remove ambiguosity from lowest() function
  EIGEN_DEVICE_FUNC static inline Real lowest()  {
    if(IsInteger) {
      return (numext::numeric_limits<T>::min)();
    } else {
      return -(numext::numeric_limits<T>::max)();
    }
  }

};

// Forward ScalarBinaryOpTraits to scalar_binary_return_type.
// Hit when
//     T and U are not the same, otherwise the trait in XprHelper handles it
// and T is not complex<U> and U is not complex<T> (this is specialized in XprHelper)
// and scalar_binary_return_type specialization exists.
template <typename T, typename U, typename BinaryOp>
struct ScalarBinaryOpTraits<T, U, BinaryOp,
                            typename std::enable_if<!std::is_same<typename internal::remove_all<T>::type, typename internal::remove_all<U>::type>::value
                                                    && !Eigen_AD::internal::is_complex_of_T_or_U<T, U>::value
                                                    &&  internal::has_ReturnType<Eigen_AD::scalar_binary_return_type<T, U, BinaryOp> >::value>::type> {
  typedef typename Eigen_AD::scalar_binary_return_type<T, U, BinaryOp>::ReturnType ReturnType;
};

namespace internal {
#if !EIGEN_AD_NO_AUTO_RETURN_TYPE
  // Forward to enable_auto_return_type
  template<typename T>
  struct enable_auto_return_type<T, typename std::enable_if<Eigen_AD::enable_auto_return_type<T>::value>::type> {
    typedef void type;
    enum { value = true };
  };
#else
  // If globally disabled, change intermediates to their active type
  template<typename T>
  struct enable_auto_return_type<T, typename std::enable_if<Eigen_AD::enable_auto_return_type<T>::value>::type> {
    typedef typename Eigen_AD::active_type<T>::type type;
    enum { value = false };
  };
#endif

  //
  // Return type traits
  //

  // Unary result_of specialized version, change intermediates to active types
  template<typename UnaryOp, typename T>
  struct result_of<UnaryOp(const T&),
                   typename internal::enable_if<(Eigen_AD::is_ad_type<T>::value &&        // Complex types may want to return a Real (e.g. abs2), so no get_inner_type
                                                 !Eigen_AD::internal::is_bool_op<UnaryOp>::value)>::type> {
    typedef T type;
  };

  // Binary result_of specialized version, forward to ScalarBinaryOpTraits
  template<typename BinaryOp, typename Lhs, typename Rhs>
  struct result_of<BinaryOp(const Lhs&, const Rhs&),
                            typename internal::enable_if<( (Eigen_AD::is_ad_type<typename Eigen_AD::internal::get_inner_type<Lhs>::type>::value ||
                                                            Eigen_AD::is_ad_type<typename Eigen_AD::internal::get_inner_type<Rhs>::type>::value) &&
                                                            !Eigen_AD::internal::is_bool_op<BinaryOp>::value)>::type> {
    typedef typename ScalarBinaryOpTraits<Lhs, Rhs, BinaryOp>::ReturnType type;
  };

  //
  // Other specializations
  //

  // min with different types (ad intermediates)
  template<typename T, typename U>
  struct min_impl<T, U, typename std::enable_if<(!is_same<T, U>::value && (Eigen_AD::is_ad_type<T>::value || Eigen_AD::is_ad_type<U>::value)), void>::type>
  {
    static EIGEN_DEVICE_FUNC inline auto run(T x, U y) {
      EIGEN_USING_STD_MATH(min);
      return deduce_return_type<decltype(min EIGEN_NOT_A_MACRO (x,y))>::run(min EIGEN_NOT_A_MACRO (x,y));
    }
  };

  // max with different types (ad intermediates)
  template<typename T, typename U>
  struct max_impl<T, U, typename std::enable_if<(!is_same<T, U>::value && (Eigen_AD::is_ad_type<T>::value || Eigen_AD::is_ad_type<U>::value)), void>::type>
  {
    static EIGEN_DEVICE_FUNC inline auto run(T x, U y) {
      EIGEN_USING_STD_MATH(max);
      return deduce_return_type<decltype(max EIGEN_NOT_A_MACRO (x,y))>::run(max EIGEN_NOT_A_MACRO (x,y));
    }
  };

  template<typename _MatrixType>
  struct traits<Eigen::Eigen_AD::internal::Matrix_AD_Wrapper<_MatrixType> > : traits<_MatrixType> {
    /*
    typedef traits<_MatrixType> Base;
    typedef _MatrixType MatrixType;
    // Let's remove NestByRefBit (copied from MatrixWrapper) // DirectAccessBit
    enum {
      Flags0 = Base::Flags,
      //LvalueBitFlag = is_lvalue<_MatrixType>::value ? LvalueBit : 0,
      //Flags = (Flags0 & ~(NestByRefBit | LvalueBit)) | LvalueBitFlag,
      Flags = Flags0 | NestByRefBit
      //EvaluatorFlags = (Base::EvaluatorFlags & ~DirectAccessBit)
    };
    */
  };

  template<typename _MatrixType>
  struct evaluator<Eigen::Eigen_AD::internal::Matrix_AD_Wrapper<_MatrixType> > : evaluator<_MatrixType> {
    typedef evaluator<_MatrixType> Base;
    using Base::Base;
  };

} // end namespace internal
} // end namespace Eigen

#endif // EIGEN_AD_H
