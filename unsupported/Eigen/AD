// This file is part of Eigen-AD, a fork of Eigen aimed at Algorithmic
// Differentiation (AD) by overloading tool developers and users.
//
// Copyright (C) 2019 Patrick Peltzer <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Johannes Lotz   <info@stce.rwth-aachen.de>
// Copyright (C) 2019 Uwe Naumann     <info@stce.rwth-aachen.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef EIGEN_AD
#define EIGEN_AD

#include "src/AD/Eigen_AD_Meta.h"
#include "src/AD/Eigen_AD.h"

#include "src/AD/Eigen_AD_Product.h"

#include "src/AD/Eigen_AD_SDSolver.h"
#include "src/AD/Solvers/Eigen_AD_LLT.h"
#include "src/AD/Solvers/Eigen_AD_LDLT.h"
#include "src/AD/Solvers/Eigen_AD_FullPivLU.h"
#include "src/AD/Solvers/Eigen_AD_PartialPivLU.h"
#include "src/AD/Solvers/Eigen_AD_HouseholderQR.h"
#include "src/AD/Solvers/Eigen_AD_FullPivHouseholderQR.h"
#include "src/AD/Solvers/Eigen_AD_ColPivHouseholderQR.h"
#include "src/AD/Solvers/Eigen_AD_CompleteOrthogonalDecomposition.h"
#include "src/AD/Solvers/Eigen_AD_SVDBase.h"
#include "src/AD/Solvers/Eigen_AD_JacobiSVD.h"
#include "src/AD/Solvers/Eigen_AD_BDCSVD.h"

#endif // EIGEN_AD
